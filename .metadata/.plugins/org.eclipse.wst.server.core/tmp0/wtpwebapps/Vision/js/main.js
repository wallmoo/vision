/* main.js */

$(document).ready(function(){
	/*gnb */
		$(".gnb > ul > li").bind('mouseenter focusin', function() { 
			$(this).addClass('on').siblings().removeClass('on'); 
		}); 

		$(".gnb > ul > li").bind("mouseleave focusout", function() {
			$(this).removeClass("on");
		});
	/*모바일 검색*/ 
	var toggle_srch = true;
	$(".msearch").click(function(){
		
		if(toggle_srch){
			$('.search').slideToggle();			
		}
		
	});
	/* 모바일 - 전체메뉴 */

	/* 전체메뉴 열기 */
	$(".header_wrap > img:nth-of-type(1)").click(function(){
		$('.allmenu').animate({"left":"0"},500);
		$('.nav_bg').css('display','block').animate({"opacity":"0.8"});
	});

	/*화살표*/
	$('.allmenu > ul > li > a').click(function(e){
		e.preventDefault();
		$(this).toggleClass('submenu-open').next('.allmenu > ul > li > ul').slideToggle(300)
		.parent().siblings().children().removeClass('submenu-open').next('.allmenu > ul > li > ul').slideUp(300);
	});

	/* 전체메뉴 닫기 */
	$(".allmenu > h3 > img").click(function(){
		$('.allmenu').animate({"left":"-150%"},500);
		$('.nav_bg').animate({"opacity":"0"}).css('display','none');
	});
});