/* qna.js  */
function submitFunction() {
	var username =	$("#username").val();
	var companyname =$("#companyname").val();
	var userphone = $("#userphone").val();
	var useremail = $("#useremail").val();
	var title = $("#title").val();
	var contents =	$("#contents").val();
	
	if(username==null || username=="" || username == undefined || companyname==null || companyname=="" || companyname == undefined || userphone==null || userphone=="" || userphone == undefined ||
		useremail==null || useremail=="" || useremail == undefined || title==null || title=="" || title == undefined || contents==null || contents=="" || contents == undefined ) {
			alert("필수 사항을 모두 입력해주세요.");
			
	} else {
		$("#orderfrm").submit();
	}
}