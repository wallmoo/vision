<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="bean.BBSBean"%>
<%@page import="java.util.List"%>
<%
	request.setCharacterEncoding("UTF-8");
	int type = (Integer)request.getAttribute("type");
	int num = (Integer)request.getAttribute("num");
	BBSBean bbsBean = (BBSBean) request.getAttribute("bbslist");
%>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
	<jsp:include page="header.jsp"/>
		<form action="./rewrite.bs" method="post" id="frm">
			<div class='container'>
			<div class='board_content'>
			<h2> <span>Online QnA</span> </h2>
					<input type="hidden" name="num" value="<%=num%>">
					 <input type="hidden" name="type" value="<%=type%>">
				<div>
					<table>
						<caption class='blind'>목록</caption>
						<thead>
							<tr>
								<th colspan='5'>게시판 글쓰기</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>제목</td>
								<td><input type="text" size="50" id="title" name="title" value="<%=bbsBean.getBbs_title() %>" /></td>
								<td>작성자</td>
								<td><input type="text" id="writer" name="writer" value="<%=bbsBean.getBbs_writer() %>"/></td>

							</tr>
							<tr>
								<td></td>
								<td></td>
								<td>비밀번호</td>
								<td><input type="password" id="password" name="password" value="<%=bbsBean.getBbs_password() %>"/></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<textarea id="contents" name="contents"><%=bbsBean.getBbs_content()%></textarea>
					<script>
						//<![CDATA[
							CKEDITOR.replace('contents',{
								enterMode:'2',
								shiftEnterMode:'3',
								width:'99.8%',
								height:'300',
								filebrowserUploadUrl:'./upload.jsp?'
							        +'realUrl=http://dostory.co.kr:8080/vision/'
							        +'&realDir=/home/vision/textupload/'
							});
							
						//]]
					</script>
				</div>
				<div align="right">
					<p><a onclick="submitFuntion()">등록</a></p>
				</div>
			</div>
			</div>
		</form>
		<script>
			function submitFuntion() {
				var title = $("#title").val();
				var writer = $("#writer").val();
				var password = $("#password").val();
				var content = CKEDITOR.instances.contents.getData();
				if(title==null || title=="" || title == undefined || content==null || content=="" || content == undefined
						|| writer==null || writer=="" || writer == undefined || password == null || password == "" || password == undefined) {
					alert("모두 작성해주세요.");
				} else {
					$("#frm").submit();	
				}
			}
		</script>
		<jsp:include page="footer.jsp"/>
	</div>
</body>
</html>