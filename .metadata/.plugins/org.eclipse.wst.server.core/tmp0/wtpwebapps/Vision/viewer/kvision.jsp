<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> K-VISION series </title>
	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_controller'>
		<div class='kvision_con1'>
			<h2> <span>정전류</span> </h2>
			<section>
				<h3><span>정전류</span>CONSTANT CURRENT</h3>
				<p>1.<span>정전압 제어 형식</span>의 조명 컨트롤러 제품입니다.<br/>조명 연결 채널 수(1~8CH)와 전력량(10W~1000W/12V,24V) 등 고객님께서 원하시는 대로 제품 설계, 생산 가능한 제품이니 담당자와 상담부탁드립니다.<br/>
					DNSR series는 컨트롤러 전면부에 디스플레이 화면을 통해 용이하게 전력 제어가 가능한 제품입니다. 또한 RS-232 통신을 통해 자동 제어 가능한 제품입니다.
				</p>
				<p>2.<span>스트로브 형식</span>의 조명 컨트롤러 제품입니다.<br/>조명 연결 채널 수(1~8CH)와 전력량(10W~1000W/12V,24V) 등 고객님께서 원하시는 대로 제품 설계, 생산 가능한 제품이니 담당자와 상담부탁드립니다.<br/>
					또한 RS-232 통신을 통해 자동 제어 가능한 제품입니다.
				</p>
			</section>
			<section>
				<div><img src='../img/controller/kvision_5.png' alt=''><img src='../img/controller/kvision_6.png' alt=''></div>
                <div><img src='../img/controller/kvision_7.png' alt=''><img src='../img/controller/kvision_8.png' alt=''></div>
                <div><img src='../img/controller/kvision_1.png' alt=''><img src='../img/controller/kvision_2.png' alt=''></div>
				<div><img src='../img/controller/kvision_3.png' alt=''><img src='../img/controller/kvision_4.png' alt=''></div>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV100NSR(V)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV100NSR(V)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../download/KV100NSR(V).dwg' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV200NSR(V)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV200NSR(V)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../download/KV200NSR(V).dwg' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV200NSR</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV200NSR.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../download/KV200NSR.dwg' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV200SM STROBE 2CH</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV200SM STROBE 2CH.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV400NSR </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV400NSR .pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV400NSR(V)</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV400NSR(V)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../download/KV400NSR(V).dwg' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV800NSR </th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV800NSR .pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../downloadKV800DNSR.dwg' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV800DNSR 200w</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV800DNSR 200w.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV800DNSR 600w</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV800DNSR 600w.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV800DNSR 1000w</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV800DNSR 1000w.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV800DNSR 1500w</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV800DNSR 1500w.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../download/KV800DNSR(1500W).dwg' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>KV800DNSR1CH(V)-Model</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/KV800DNSR1CH(V)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td></td>
							</tr>
						</tbody>
					</table>
					
			</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>