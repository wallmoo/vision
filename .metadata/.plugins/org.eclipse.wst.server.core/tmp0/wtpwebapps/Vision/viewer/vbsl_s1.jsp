<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VBSL_S1 series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vbsl_s1'>
		<div class='vbsl_s1_con1'>
			<h2 class='pc'> <span>VBSL series</span> </h2>
			<h2 class='mobile'> <span>백라이트조명</span> </h2>
			<ul class='mobile'>
				<li class='name'><a href='vbsl_s1.jsp'>VHLS</a></li>
				<li><a href='vbml.jsp'>VLSL</a></li>
				<li><a href='vbhl.jsp'>VLBL</a></li>
			</ul>
			<section>
				<h3><span>백라이트 소형 조명</span>LED BACK  SMALL LIGHT</h3>
				<p><span>VBSL series</span>는 타사 백라이트 제품에 비해 높은 휘도율을 자랑하는 제품입니다.
				</p>
				<img src='../img/light/vbsl_s1_img1.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>PCB 외관 검사 , 고속촬영 및 실루엣 검사, 대형 제품 검사용</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VBL serise는 타사에 비해 높은 휘도율을 자랑하는 제품입니다.<br/>
					발열을 억제하기 위해 특수 제작한 방열판을 채용하였고
					펜을 추가적으로 부착할 수 있는 제품입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					LED PCB를 균일하고 고밀도로 배열하여 균일하고 높은 휘도율을 자랑합니다.<br/>
					광확산판을 채용하여 확산각이 넓고 균일합니다.<br/>
					발열을 잡기 위해 자체 설계한 방열판(VBSL,VBML,VBLL)과 
					추가적으로 펜(VBLL)을 부착하였습니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>평판형 링 조명(입사각 0°)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th>조도(Lx)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VBSL32/16J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>1.5W<br/>3W</td>
								<td>1 /0.5<br/>2.5 / 0.125</td>
								<td>32</td>
								<td>16</td>
								<td>10</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 32 10 16J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 32 10 16J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL45/45</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>2W<br/>4W</td>
								<td>2 / 1<br/>3 / 1.5</td>
								<td>45</td>
								<td>45</td>
								<td>20</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 45 45 20.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 45 45 20-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL53/20J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>2.5W<br/>5W</td>
								<td>2 / 1<br/>4 / 2</td>
								<td>53</td>
								<td>20</td>
								<td>20</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 53 20 20J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 53 20 20J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL64/58J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>3W<br/>5.5W</td>
								<td>2.5 / 0.125<br/>4.5 / 2.2</td>
								<td>64</td>
								<td>58</td>
								<td>17</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 64 58 29J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 64 58 29J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL64/58J-2</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>3W<br/>6W</td>
								<td>2.5 / 0.125<br/>5 / 2.5</td>
								<td>64</td>
								<td>58</td>
								<td>29</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 64 58 29J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 64 58 29J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL72/52J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>4W<br/>8W</td>
								<td>3 / 1.5<br/>6.6 / 3.3</td>
								<td>75</td>
								<td>52</td>
								<td>17</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 72 52 17J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 72 52 17J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL75/45</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>3.5W<br/>7W</td>
								<td>2.9 / 1.4<br/>5.8 / 2.9</td>
								<td>75</td>
								<td>45</td>
								<td>20</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 75 45 20.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 75 45 20-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL75/75</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>4W<br/>8W</td>
								<td>3 / 1.5<br/>6.6 / 3.3</td>
								<td>75</td>
								<td>75</td>
								<td>20</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 75 75 20.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 75 75 20-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL78/66J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>4W<br/>8W</td>
								<td>3 / 1.5<br/>6.6 / 3.3</td>
								<td>78</td>
								<td>66</td>
								<td>17</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 78 66 17J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 78 66 17J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL90/37J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>4.5W<br/>9W</td>
								<td>3.7 / 1.8<br/>7.5 / 3.7</td>
								<td>90</td>
								<td>37</td>
								<td>25</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 90 37 25J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 90 37 25J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL90/82</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>5W<br/>10W</td>
								<td>4 / 2<br/>8.3 / 4</td>
								<td>90</td>
								<td>82</td>
								<td>20</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 90 82 20.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 90 82 20-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL105/75</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>5W<br/>10W</td>
								<td>4 / 2<br/>8.3 / 4</td>
								<td>105</td>
								<td>75</td>
								<td>20</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 105 75 20.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 105 75 20-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL105/105</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>6W<br/>12W</td>
								<td>5 / 2.5<br/>10 / 5</td>
								<td>105</td>
								<td>105</td>
								<td>20</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 105 105 25.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 105 105 25-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL127/79J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>6W<br/>12W</td>
								<td>0.5A/0.25A<br/>1A/0.5A</td>
								<td>127</td>
								<td>79</td>
								<td>25</td>
								<td>100,000<br/>200,000</td>
								<td class='download'><a href='../download/VBSL 127 79 25J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 127 79 25J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL135/135</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>7.5W<br/>15W</td>
								<td>0.6A/0.3A<br/>1.2A/0.6A</td>
								<td>135</td>
								<td>135</td>
								<td>25</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 135 135 25.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 135 135 25-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL167/167J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>9W<br/>18W</td>
								<td>0.7A/0.35A<br/>1.4A/0.7A</td>
								<td>167</td>
								<td>167</td>
								<td>32</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 167 167 32J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 167 167 32J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL169/213JL</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>2W<br/>4W</td>
								<td> 0.2A/0.1A<br/>0.4A/0.2A</td>
								<td>169</td>
								<td>213</td>
								<td>12</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 169 213 JL.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 169 213 JL-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL193/128J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>9W<br/>18W</td>
								<td>0.7A/0.35A<br/>1.4A/0.7A</td>
								<td>193</td>
								<td>128</td>
								<td>32</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 193 128 32J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 193 128 32J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL195/195</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>12W<br/>24W</td>
								<td>1A/0.5A<br/>2A/1A</td>
								<td>195</td>
								<td>195</td>
								<td>25</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 195 195 25.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 195 195 25-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL208/25J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>7W<br/>14W</td>
								<td>0.6A/0.3A<br/>1.2A/0.6A</td>
								<td>208</td>
								<td>25</td>
								<td>22</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 208 25 22J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 208 25 22J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL225/225</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>14W<br/>28W</td>
								<td>1.2A/0.6A<br/>2.4A/12A</td>
								<td>225</td>
								<td>225</td>
								<td>30</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 225 225 30.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 225 225 30-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL255/255</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>15W<br/>30W</td>
								<td>1.3A/0.65A<br/>2.6A/1.3A</td>
								<td>255</td>
								<td>255</td>
								<td>30</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 255 255 30.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 255 255 30-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL285/285</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>16W<br/>32W</td>
								<td>1.4A/0.7A<br/>2.8A/1.4A</td>
								<td>285</td>
								<td>285</td>
								<td>30</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 285 285 30.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 285 285 30-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL343/323J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>18W<br/>36W</td>
								<td>1.5A/0.75A<br/>3A/1.5A</td>
								<td>343</td>
								<td>323</td>
								<td>30</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 334 334 30.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 334 334 30-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL334/334J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>20W<br/>40W</td>
								<td>1.6A/0.8A<br/>3.2A/1.6A</td>
								<td>334</td>
								<td>334</td>
								<td>30</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 343 323 30J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 343 323 30J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBSL362/348JL</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>4W<br/>8W</td>
								<td>0.3A/0.15A<br/>0.6A/0.3A</td>
								<td>362</td>
								<td>348</td>
								<td>16</td>
								<td>70<br/>140</td>
								<td class='download'><a href='../download/VBSL 362 348 JL.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBSL 362 348 JL-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vbsl_s1_img2.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 백라이트 조명의 적용 사례</h3>
					<img src='../img/light/vbsl_s1_img3.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 백라이트 조명 예시</h3>
					<img src='../img/light/vbsl_s1_img4.png' alt=''>
			</section>
		</div>
	<jsp:include page="viewerfooter.jsp" />
</body>
</html>