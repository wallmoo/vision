<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VLSL series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vlsl'>
		<div class='vlsl_con1'>
			<h2 class='pc'> <span>VLSL series</span> </h2>
			<h2 class='mobile'> <span>라인스캔조명</span> </h2>
			<ul class='mobile'>
				<li><a href='vhls.jsp'>VHLS</a></li>
				<li class='name'><a href='vlsl.jsp'>VLSL</a></li>
				<li><a href='vlbl.jsp'>VLBL</a></li>
			</ul>
			<section>
				<h3><span>고조도 라인조명</span> LINE SCAN LIGHT</h3>
				<p><span>VLSL series</span>는 자사 주력 제품 중 하나로써 높은 조도를 자랑하는 제품입니다.<br/>빛의 균일성과 확산도가 뛰어나며, 발열을 억제하기 위하여 특수제작한 방열판과 펜을 채용하였습니다.
				</p>
				<img src='../img/light/vlsl_img1.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>라인 스캔용 조명 ,필름, 제품 외관, 바코드 등 다양한 제품 검사, 설치의 자유도가 높지 않은 협소한 공간 등에 쓰입니다.</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VLSL는 라인스캔용 다목적 LED BAR 조명입니다. 빛의 확산도와 균일성을 위해 광확산판을 채용하였습니다.<br/>
					넓은 발광원(18,28㎜) 을 가지고 있으며 발열을 잡기위해 특수제작한 방열판과 방열펜을 채용하였습니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					LED PCB를 고밀도로 배치하여 균일도를 높힌 제품입니다.<br/>
					제품의 폭,길이와 높이 등을 각10㎜,100㎜ 축소 또는 확대 제작 가능합니다.<br/>
					조도 또한 20,40,80,100만LUX에 국한되어 있지않고, 적게는 3000 ~ 6000Lux 많게는 300만Lux까지 고객님께서 원하시는 조도, 휘도로 제작 가능합니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>고조도 라인 조명(18)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th>발광장(㎜)</th>
								<th>조도(㎜)</th>
								<th class='width'>냉각방식</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VLSL100N</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>8W X N<br/>16W X N</td>
								<td>0.3A<br/>0.6A</td>
								<td>100 X N</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL100N(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL100N(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>16W<br/>32W</td>
								<td>0.6A<br/>1.3A</td>
								<td>200</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL200(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL200(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL300</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>24W<br/>48W</td>
								<td>1A<br/>2A</td>
								<td>300</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL300(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL300(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>42W<br/>96W</td>
								<td>1.75A<br/>4A</td>
								<td>600</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL600(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL600(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>64W<br/>128W</td>
								<td>2.6A<br/>5.2A</td>
								<td>800</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL800(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL800(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL1000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>80W<br/>160W</td>
								<td>3.3A<br/>7A</td>
								<td>1000</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL1000(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL1000(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL1200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>96W<br/>192W</td>
								<td>4A<br/>8A</td>
								<td>1200</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL1200(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL1200(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL1500</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>120W<br/>240W</td>
								<td>5A<br/>10A</td>
								<td>1500</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL1500(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL1500(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL1800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>144W<br/>288W</td>
								<td>6A<br/>12A</td>
								<td>1800</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL1800(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL1800(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL2000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>160W<br/>320W</td>
								<td>6.6A<br/>13A</td>
								<td>2000</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL2000(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL2000(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL2400</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>192W<br/>384W</td>
								<td>8A<br/>16A</td>
								<td>2400</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL2400(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL2400(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL2600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>208W<br/>416W</td>
								<td>8.6A<br/>17.3A</td>
								<td>2600</td>
								<td>60</td>
								<td>30</td>
								<td>18</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL2600(18).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL2600(18)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
					<p>고조도 라인 조명(28)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th>발광장(㎜)</th>
								<th>조도(㎜)</th>
								<th class='width'>냉각방식</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VLSL100N</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>7W X N<br/>14W X N</td>
								<td>0.3A<br/>0.6A</td>
								<td>100 X N</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL100N(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL100N(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>14W<br/>28W</td>
								<td>0.6A<br/>1.2A</td>
								<td>200</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL200(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL200(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL300</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>21W<br/>42W</td>
								<td>0.9A<br/>1.8A</td>
								<td>300</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL300(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL300(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>42W<br/>84W</td>
								<td>1.75A<br/>3.5A</td>
								<td>600</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL600(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL600(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>56W<br/>112W</td>
								<td>1A<br/>2A</td>
								<td>800</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL800(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL800(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL1000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>70W<br/>140W</td>
								<td>2.9A<br/>5.8A</td>
								<td>1000</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL1000(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL1000(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL1200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>84W<br/>168W</td>
								<td>3.5A<br/>7A</td>
								<td>1200</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL1200(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL1200(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL1500</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>105W<br/>210W</td>
								<td>4.3A<br/>8.6A</td>
								<td>1500</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL1500(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL1500(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL1800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>126W<br/>252W</td>
								<td>5.2A<br/>10.4A</td>
								<td>1800</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL1800(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL1800(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL2000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>140W<br/>280W</td>
								<td>5.8A<br/>11.6A</td>
								<td>2000</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL2000(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL2000(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL2400</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>168W<br/>336W</td>
								<td>7A><br/>14A</td>
								<td>2400</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL2400(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL2400(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLSL2600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>182W<br/>364W</td>
								<td>7.5A<br/>15A</td>
								<td>2600</td>
								<td>60</td>
								<td>40</td>
								<td>28</td>
								<td>300.000<br/>600,000</td>
								<td>펜냉각</td>
								<td class='download'><a href='../download/VLSL2600(28).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLSL2600(28)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vlsl_img2.png' alt=''>
			</section>
			<section>
				<h3><span>◆</span> 라인 조명 예시</h3>
				<dl>
					<dt><img src='../img/light/vhis_img4.jpg' alt='라인조명 예시1'></dt>
					<dd>라인조명 예시1</dd>
				</dl>
				<dl>
					<dt><img src='../img/light/vhis_img5.jpg' alt='라인조명 예시2'></dt>
					<dd>라인조명 예시2</dd>
				</dl>
			</section>
			
			
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>