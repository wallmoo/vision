<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>History</title>

		<jsp:include page="viewerheader.jsp" />
		<!--container-->
	<div class='container history'>
		<div class='his_con1'>
			<h2><span>History</span></h2>
			<img src='../img/history_img1.jpg' alt=''>
			<div class='his_con2 pc'>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2015</span></dt>
					<dd>방열판 방식 초고휘도 조명장치 개발 판매 -</dd>
					<dd>삼성 SDI 검사용 조명장치 납품</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2013</span></dt>
					<dd>르노 삼성 자동차 납품 -</dd>
					<dd>자동차 차선 이탈 검사용 조명장치 개발</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2011</span></dt>
					<dd>PC GUIDE 를 구비한 돌출간판 특허 취득 -</dd>
					<dd>비전용 LED 고조도 라인스캔 조명장치 개발</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2009</span></dt>
					<dd>광고용 V-cutting 기계 설치 -</dd>
					<dd>아크릴 자동 절단기계 설치</dd>
					<dd>LED 유도등 제작</dd>
					<dd>LED 광고판 특허 출원</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2007</span></dt>
					<dd>LED 등기구 전문업체인 NINGBO SHINESUN Co.Ltd 의 대리점 계약 -</dd>
					<dd>코엑스 조명 전시회 참가</dd>
					<dd>LED 유도등 제작</dd>
					<dd>LED 광고판 특허 출원</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2005</span></dt>
					<dd>비전용 검사장비 업체 (주) 쓰리비시스템 협력업체 등록 -</dd>
					<dd>비전용 CCFL 조명장치 개발</dd>
					<dd>고휘도 유도등 업체 (주) 신화전자, 타이코코리아 협력업체 등록</dd>
					<dd>비전테크 설립</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2016</span></dt>
					<dd>- LG 디스플레이 검사용 조명장치 납품</dd>
					<dd>동우화학 비전 조명장치 납품</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2014</span></dt>
					<dd>- 포스코 고조도 비전 조명장치 납품</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2012</span></dt>
					<dd>- 방열판 방식 고휘도 조명장치 개발 판매</dd>
					<dd>비전용 LED 고휘도 조명장치 개발</dd>
					<dd>LED 조명의 방열장치 특허 취득</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2010</span></dt>
					<dd>- 비전용  LED 평판조명장치 특허 취득 </dd>
					<dd>PC GUIDE를 구비한 돌출간판 특허 출현</dd>
					<dd>LED 조명의 방열장치 특허 출현</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2008</span></dt>
					<dd>- LED 가로등 전문업체인 명창광전 Co.Ltd 와 대리점 계약</dd>
					<dd>코엑스 건축조명 전시회 참가</dd>
					<dd>LED 전용 홈페이지 lightbank.co.kr OPEN</dd>
					<dd>LED 평판등기구 특허 출원</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2006</span></dt>
					<dd>- 대면적 검사용 조명장치 개발 완료</dd>
					<dd>비전용 검사장비 업체 (주) 넥스트아이, (주) 넥스타 협력업체 등록</dd>
					<dd>고휘도 유도등 업체 (주) 올라이트라이프 협력업체 등록</dd>
				</dl>
				<dl>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2019</span></dt>
					<dd>기술우수 기업인증 획득 -</dd>
                    <dd>CE인증 획득(유럽수출)</dd>
				</dl>           
			</div>
            
			<div class='his_con2 mobile'>
				<dl class='his_left'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2019</span>-</dt>
					<dd>기술우수 기업인증 획득 -</dd>
                    <dd>CE인증 획득(유럽수출)</dd>
				</dl>            
				<dl class='his_right'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2016</span></dt>
					<dd>LG 디스플레이 검사용 조명장치 납품 -<br/>
						<span>동우화학 비전 조명장치 납품</span></dd>
				</dl>
				<dl class='his_left'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2015</span>-</dt>
					<dd>방열판 방식 초고휘도 조명장치 개발 판매<br/>
					<span>삼성 SDI 검사용 조명장치 납품</span></dd>
				</dl>
				<dl class='his_right'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2014</span></dt>
					<dd>포스코 고조도 비전 조명장치 납품 -</dd>
				</dl>
				<dl class='his_left'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2013</span>-</dt>
					<dd>르노 삼성 자동차 납품<br/>
					<span>자동차 차선 이탈 검사용 조명장치 개발</span></dd>
				</dl>
				<dl class='his_right'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2012</span></dt>
					<dd>방열판 방식 고휘도 조명장치 개발 판매 -<br/>
					<span>비전용 LED 고휘도 조명장치 개발</span><br/>
					<span>LED 조명의 방열장치 특허 취득</span></dd>
				</dl>
				<dl class='his_left'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2011</span>-</dt>
					<dd>PC GUIDE 를 구비한 돌출간판 특허 취득<br/>
					<span>비전용 LED 고조도 라인스캔 조명장치 개발</span></dd>
				</dl>
				<dl class='his_right'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2010</span></dt>
					<dd>비전용  LED 평판조명장치 특허 취득 -<br/>
					<span>PC GUIDE를 구비한 돌출간판 특허 출현</span><br/>
					<span>LED 조명의 방열장치 특허 출현</span></dd>
				</dl>
				<dl class='his_left'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2009</span>-</dt>
					<dd>광고용 V-cutting 기계 설치<br/>
					<span>아크릴 자동 절단기계 설치</span><br/>
					<span>LED 유도등 제작</span><br/>
					<span>LED 광고판 특허 출원</span></dd>
				</dl>
				<dl class='his_right'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2008</span></dt>
					<dd>LED 가로등 전문업체인 명창광전 Co.Ltd 와 대리점 계약 -<br/>
					<span>코엑스 건축조명 전시회 참가</span><br/>
					<span>LED 전용 홈페이지 lightbank.co.kr OPEN</span><br/>
					<span>LED 평판등기구 특허 출원</span></dd>
				</dl>
				<dl class='his_left'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2007</span>-</dt>
					<dd>LED 등기구 전문업체인 NINGBO SHINESUN Co.Ltd 의 대리점 계약<br/>
					<span>코엑스 조명 전시회 참가</span><br/>
					<span>LED 유도등 제작</span><br/>
					<span>LED 광고판 특허 출원</span></dd>
				</dl>
				<dl class='his_right'> 
					<dt><img src='../img/history_img3.jpg' alt=''><span>2006</span></dt>
					<dd>대면적 검사용 조명장치 개발 완료 -<br/>
					<span>비전용 검사장비 업체 (주) 넥스트아이, (주) 넥스타 협력업체 등록</span><br/>
					<span>고휘도 유도등 업체 (주) 올라이트라이프 협력업체 등록</span></dd>
				</dl>
				<dl class='his_left'>
					<dt><img src='../img/history_img3.jpg' alt=''><span>2005</span>-</dt>
					<dd>비전용 검사장비 업체 (주) 쓰리비시스템 협력업체 등록<br/>
					<span>비전용 CCFL 조명장치 개발</span><br/>
					<span>고휘도 유도등 업체 (주) 신화전자, 타이코코리아 협력업체 등록</span><br/>
					<span>비전테크 설립</span></dd>
				</dl>
			</div>
		</div>
			<jsp:include page="viewerfooter.jsp" />
</body>
</html>