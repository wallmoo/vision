<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VFDL series </title>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0 " />
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<link type="text/css" rel="stylesheet" href="../css/main.css" />
<script type="text/javascript" src="../js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="../js/main.js"></script>
<!-- js Library -->
</head>
<body>
 <div>
	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vfdl'>
		<div class='vfdl_con1'>
				<h2 class='pc'> <span>VFDL series</span> </h2>
				<h2 class='mobile'> <span>돔조명</span> </h2>
				<ul class='mobile'>
					<li ><a href='vdml.jsp'>VDML</a></li>
					<li class='name'><a href='vfdl.jsp'>VFDL</a></li>
				</ul>
			<section>
				<h3><span>플랫 돔 조명</span>FLAT DOME LIGHT</h3>
				<p><span>VFDL series</span>는 고속 검사 촬영용, 빠르게 움직이는 생산 라인에 적합한 고 출력 재생 플랫 타입의 돔 조명입니다.
				</p>
				<img src='../img/light/vfdl_img1.jpg' alt='vdml'/>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>광택이 강한 포장재, 표면형태 입체적인 제품, 인쇄 문자인식, 금속표면검사</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					고속 검사 촬영용, 빠르게 움직이는 생산 라인에 적합한 고 출력 재생 플랫 타입의 돔 조명입니다.<br/>					
					이 제품은 작업라인에 균일한 확산 광을 비출 수 있습니다.<br/>					
					촬영 제품에 맞추어 패턴을 선택 바랍니다.<br/>
					(Dot , Stripes)
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					도광판 의 표면상의 도트,스프라이트 패턴은 조명광의 확산 및 투과를 제어합니다.<br/>
					자사 돔조명(VDML) 검사대상체의 크기가 크면 이미지 밝기가 어두워 질 수 있는 현상을 보안한 제품입니다.<br/>					
					기존에 보안점은 자사 동축조명(VCOL)을 추가 사용하여 보안하였지만, VFDL은 추가사용 없이 밝은 이미지 획득이 가능합니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>플랫 돔 조명(FLAT DOME LIGHT)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>높이(㎜)</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>발광장(㎜x㎜)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VFDL30/30</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>3W</td>
								<td>0.2A/0.12A</td>
								<td>12</td>
								<td>60</td>
								<td>46</td>
								<td>30X30</td>
								<td class='download'><a href='../download/VFDL 60 46 12.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VFDL 60 46 12-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VFDL50/50</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>6W</td>
								<td>0.5A/0.25A</td>
								<td>12</td>
								<td>90</td>
								<td>90</td>
								<td>50X50</td>
								<td class='download'><a href='../download/VFDL 90 90 12.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VFDL 90 90 12-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VFDL75/75</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>8W</td>
								<td>0.6A/0.3A</td>
								<td>12</td>
								<td>115</td>
								<td>115</td>
								<td>75X75</td>
								<td class='download'><a href='../download/VFDL 115 115 12.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VFDL 115 115 12-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VFDL100/100</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>10W</td>
								<td>0.8A/0.4A</td>
								<td>12</td>
								<td>140</td>
								<td>140</td>
								<td>100X100</td>
								<td class='download'><a href='../download/VFDL 140 140 12.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VFDL 140 140 12-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VFDL150/150</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>12W</td>
								<td>1A/0.5A</td>
								<td>12</td>
								<td>190</td>
								<td>190</td>
								<td>150X150</td>
								<td class='download'><a href='../download/VFDL 190 190 12.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VFDL 190 190 12-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VFDL200/100</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>14W</td>
								<td>1.1A/0.55A</td>
								<td>12</td>
								<td>240</td>
								<td>140</td>
								<td>200X100</td>
								<td class='download'><a href='../download/VFDL 240 140 12.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VFDL 240 140 12-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VFDL200/200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>16W</td>
								<td>1.2A/0.6A</td>
								<td>12</td>
								<td>240</td>
								<td>240</td>
								<td>200X200</td>
								<td class='download'><a href='../download/VFDL 240 240 12.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VFDL 240 240 12-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
					<img src='../img/light/vfdl_img3.jpg' alt=''>
			</section>
			<section>
					<h3><span>◆</span>플랫돔 조사구조</h3>
					<img src='../img/light/vfdl_img2.jpg' alt=''>
			</section>
			<section>
					<h3><span>◆</span>완벽한 검사 이미지 도출 방법</h3>
					<p>1.카메라 렌즈 조리개를 평소보다 넓혀야 합니다.</p>
					<p>2.검사대상에 렌즈를 초점을 맞춥니다.</p>
					<p>3.도트 패턴이 보이면 조명 장치의 위치를 조정하십시오.</p>
					<p>4.빛의 강도를 조절합니다. (조도 값이 높다면 카메라의 셔터 속도를 높입니다.)</p>
					<p>주위 광원은 조명 장치 표면 또는 작업 물에서 반사되어 이미징에 영향을 줄 수 있습니다</p>
					<p>ⓐ렌즈에 렌즈 필터를 부착하십시오. </p>
					<p>ⓑ 셔터 속도를 높이고, 조도를 약간 올리십시오.</p>
					<p>ⓒ후드 또는 덮개로 주변 광선이 들어 가지 않도록 하십시오.</p>
					<p>조명과 검사 대상체의 거리를 변경하면 용도에 맞게 검사 이미지를 획득 할 수 있습니다.20mm</p>
			</section>
			
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>