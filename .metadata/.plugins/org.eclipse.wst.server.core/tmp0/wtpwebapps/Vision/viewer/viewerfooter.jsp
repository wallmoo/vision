<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<div>
		<div class='con2'>
			<div>
				<dl class="pc">
					<dt>회사위치</dt>
					<dd>서울시 금천구 범안로 1130<br />디지털 엠파이어 빌딩 1405호</dd>
					<dd><img src='../img/tele_img.jpg' alt='전화'>02-869-3001<br /> <img src='../img/fax_img.jpg' alt='펙스'>02-869-3008</dd>
				</dl>
				<dl>
					<dt>Company</dt>
					<dd><a href='javascript:subMove(1);'>인사말 및 경영이념</a></dd>
					<dd><a href='javascript:subMove(2);'>회사연혁</a></dd>
					<dd><a href='javascript:subMove(3);'>인증현황</a></dd>
					<dd><a href='javascript:subMove(4);'>오시는 길</a></dd>
				</dl>
				<dl class="pc">
					<dt>Light</dt>
					<dd><a href='javascript:subMove(5);'>라인 스캔 조명</a></dd>
					<dd><a href='javascript:subMove(6);'>백라이트 조명</a></dd>
					<dd><a href='javascript:subMove(7);'>링 조명</a></dd>
					<dd><a href='javascript:subMove(8);'>스퀘어 조명</a></dd>
				</dl>
				<dl class="pc">
					<dt>&nbsp;</dt>
					<dd><a href='javascript:subMove(9);'>스폿 조명</a></dd>
					<dd><a href='javascript:subMove(15);'>돔 조명</a></dd>
					<dd><a href='javascript:subMove(16);'>동축 조명</a></dd>
					<dd><a href='javascript:subMove(10);'>특수 주문 조명</a></dd>
				</dl>
				<dl class="mobile">
					<dt>Light</dt>
					<dd><a href='javascript:subMove(5);'>라인 스캔 조명</a></dd>
					<dd><a href='javascript:subMove(6);'>백라이트 조명</a></dd>
					<dd><a href='javascript:subMove(7);'>링 조명</a></dd>
					<dd><a href='javascript:subMove(8);'>스퀘어 조명</a></dd>
				</dl>
				<dl class="mobile">
					<dt>&nbsp;</dt>
					<dd><a href='javascript:subMove(9);'>스폿 조명</a></dd>
					<dd><a href='javascript:subMove(15);'>돔 조명</a></dd>
					<dd><a href='javascript:subMove(16);'>동축 조명</a></dd>
					<dd><a href='javascript:subMove(10);'>특수 주문 조명</a></dd>
				</dl>
				<dl>
					<dt>Controller</dt>
					<dd><a href="javascript:subMove(12);">정전압</a></dd>
					<dd><a href="javascript:subMove(13);">정전류</a></dd>
				</dl>
				<dl>
					<dt>Costomer Support</dt>
					<dd><a href="javascript:subMove(51);">공지사항</a></dd>
					<dd><a href="javascript:subMove(52);">온라인 문의</a></dd>
					<dd><a href="javascript:subMove(53);">주문 의뢰서</a></dd>
				</dl>
				<dl>
					<dt>Data Room</dt>
					<dd><a href="javascript:subMove(21);">카탈로그</a></dd>
					<dd><a href="javascript:subMove(22);">동영상</a></dd>
					<dd><a href="javascript:subMove(54);">관련자료</a></dd>
				</dl>
			</div>
		</div>
	</div>
	</div>
	<div class='footer'>
	<div class='footer_inner pc'>
		<img src='../img/logo_vision-tech2.png' alt='로고'>
		<p>	대표이사 : 이석길  사업자 등록번호 : 113-15-50834  서울시 금천구 범안로 1130 디지털 엠파이어 빌딩 1405호<br/>
			대표번호 : 02-869-3001  팩스 : 02-6008-6405  이메일 : luckylsk@naver.com  Copyright By 비전테크놀로지. All Rights Reserved.<br/>
			본사이트의 컨텐츠는 저작권법의 보호를 받는 바 무단 전재, 복사, 배포를 금합니다.  개인정보취급방침</p>
	</div>
	<div class='footer_inner mobile'>
			<img src='../img/logo_vision-tech2.png' alt='로고'>
			<p>	대표이사 : 이석길  사업자 등록번호 : 113-15-50834  <br/>서울시 금천구 범안로 1130 디지털 엠파이어 빌딩 1405호<br/>
				대표번호 : 02-869-3001 &nbsp; 팩스 : 02-6008-6405 &nbsp; 이메일 : luckylsk@naver.com  Copyright By 비전테크놀로지. All Rights Reserved.<br/>
				본사이트의 컨텐츠는 저작권법의 보호를 받는 바 무단 전재, 복사, 배포를 금합니다. <br/>개인정보취급방침</p>
		</div>
 </div>