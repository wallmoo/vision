<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VSQL-D series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vsql'>
		<div class='vsql_con1'>
			<h2 class='pc'> <span>VSQL-D series</span> </h2>
			<h2 class='mobile'> <span>스퀘어조명</span> </h2>
			<ul class='mobile'>
				<li class='name'><a href='vsql_d.jsp'>VSQL-D</a></li>
				<li><a href='vsql_l.jsp'>VSQL-L</a></li>
			</ul>
			<section>
				<h3><span>스퀘어 조명</span> DIFFUSION SQUARE LIGHT</h3>
				<p><span>VSQL-D series</span>는 자사 고휘도 조명(VSL)을 4방향으로 취부하여 각각 독립적인 각도 조절이 가능하도록 설계된 제품입니다.
				</p>
				<img src='../img/light/vsql_img3.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>제품 크기에 따라 다양한 효과를 나타낼수 있습니다.</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					다양한 조명 환경을 형성시키는 구조화 된 조명입니다.VSQL 조명은 자사 고휘도 조명(VSL)을 4방향으로 취부하여 각각 독립적인 각도 조절이 가능하도록 설계된 제품입니다.<br/>
					다양한 입사각 설정과 설치의 자유도가 높아 설치된 현장에서 다양하고 자유롭게 적용할수있는 조명입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					VSQL-D조명은 폭넓은 사용범위를 갖습니다.광확산판을 채용하여 높은 균일도를 갖습니다.<br/>
					LED PCB를 촘촘희 배치하여 균일도를 높혔습니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>스퀘어 조명(DIFFUSION TYPE)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로</th>
								<th>발광장</th>
								<th>총가로</th>
								<th>세로</th>
								<th>높이</th>
								<th>높이2</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VSQL100XN(D)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>24W X N</td>
								<td>2A/1A</td>
								<td>100N</td>
								<td>100N-10</td>
								<td>100N+67</td>
								<td>31</td>
								<td>55</td>
								<td>41</td>
								<td class='download'><a href='../download/VSQL100N(D).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL100N(D)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSQL100(D)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>24W</td>
								<td>2A/1A</td>
								<td>111</td>
								<td>102</td>
								<td>179</td>
								<td>31</td>
								<td>55</td>
								<td>41</td>
								<td class='download'><a href='../download/VSQL100(D).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL100(D)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSQL200(D)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>48W</td>
								<td>4A/2A</td>
								<td>211</td>
								<td>202</td>
								<td>279</td>
								<td>31</td>
								<td>55</td>
								<td>41</td>
								<td class='download'><a href='../download/VSQL200(D).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL200(D)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSQL400(D)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>96W</td>
								<td>8A/4A</td>
								<td>411</td>
								<td>402</td>
								<td>479</td>
								<td>31</td>
								<td>55</td>
								<td>41</td>
								<td class='download'><a href='../download/VSQL400(D).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL400(D)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSQL500(D)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>120W</td>
								<td>10A/5A</td>
								<td>511</td>
								<td>502</td>
								<td>579</td>
								<td>31</td>
								<td>55</td>
								<td>41</td>
								<td class='download'><a href='../download/VSQL500(D).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL500(D)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
					
				<img src='../img/light/vsql_img2.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 스퀘어 조명 예시</h3>
					<img src='../img/light/vhis_img4.jpg' alt=''>
			</section>
		</div>
	<jsp:include page="viewerfooter.jsp" />
</body>
</html>