<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VRDL series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vrdl'>
		<div class='vrdl_con1'>
			<h2  class='pc'> <span>VRDL series</span> </h2>
			<h2 class='mobile'> <span>링조명</span> </h2>
			<ul class='mobile'>
				<li ><a href='vrll.jsp'>VRLL</a></li>
				<li ><a href='vrml.jsp'>VRML</a></li>
				<li ><a href='vrhl.jsp'>VRHL</a></li>
				<li class='name'><a href='vrdl.jsp'>VRDL</a></li>
				<li ><a href='vrfl.jsp'>VRFL</a></li>
			</ul>
			<section>
				<h3><span>측면조사 링 조명</span> RING DIRECT ANGLE LIGHT</h3>
				<p><span>VRDL series</span>는 수평을 기준으로 90°의 입사각을 갖는 제품입니다.
				</p>
				<img src='../img/light/vrdl_img1.png' alt='vrdl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>이물검사,스크래치 검사</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VRDLseries는 수평을 기준으로90°의 입사각을 갖는 제품입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					90°의 입사각을 가지는 제품으로써 검사대상체 표면의 돌출부에 빛을 간접 조사함으로써 검사대상물체의 음,양각을
					효과적으로 조사함으로써 이물검사,스크래치 등에 검사에 특화된 조명입니다.<br/>검사 대상체에 조명을 근접 시킬수록 검사 검출이 효과 적입니다.주문제작이 가능한 제품으로써 담당자와 상담부탁드립니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>입사각 링 조명(입사각 90°)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>외경(Ø)</th>
								<th>내경(Ø)</th>
								<th>높이(㎜)</th>
								<th>입사각(°)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VRDL90/70</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>6W</td>
								<td>0.6A/0.3A</td>
								<td>90</td>
								<td>70</td>
								<td>12</td>
								<td>90</td>
								<td class='download'><a href='../download/VRDL90 70.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRDL90 70-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRDL140/120</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>12W</td>
								<td>1A/0.5A</td>
								<td>140</td>
								<td>120</td>
								<td>14</td>
								<td>90</td>
								<td class='download'><a href='../download/VRDL140 120.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRDL140 120-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRDL170/150</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>16W</td>
								<td>1.3A/0.7A</td>
								<td>170</td>
								<td>150</td>
								<td>14</td>
								<td>90</td>
								<td class='download'><a href='../download/VRDL170 150.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRDL170 150-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRDL200/170</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>20W</td>
								<td>1.7A/0.8A</td>
								<td>200</td>
								<td>170</td>
								<td>14</td>
								<td>90</td>
								<td class='download'><a href='../download/VRDL200 170.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRDL200 170-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vrdl_img2.png' alt=''>
			</section>
			
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>