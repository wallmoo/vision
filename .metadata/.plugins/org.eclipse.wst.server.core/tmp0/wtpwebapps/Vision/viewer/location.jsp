<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Location</title>

		<jsp:include page="viewerheader.jsp" />
		<!--container-->
	<div class='container location'>
		<div class='loca_con1'>
			<h2> <span>Location</span> </h2>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3166.7937473701686!2d126.88571955079344!3d37.465591437561606!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357b61e988e3bd45%3A0x7317a08870084fa9!2z65SU7KeA7YS47Jeg7YyM7J207Ja0!5e0!3m2!1sko!2skr!4v1502691056760" width="1050" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class='loca_con2'>
			<ul>
				<li>
					<ul>
						<li>ADDRESS</li>
						<li><span class='pc'>서울시 금천구 범안로 1130 디지털 엠파이어 빌딩 1405호</span>
							<span class='mobile'>서울시 금천구 범안로 1130<br/>디지털 엠파이어 빌딩 1405호</span>
						</li>
						<li>E-MAIL</li>
						<li>luckylsk@naver.com</li>
					</ul>
				</li>
				<li>
					<ul>
						<li>TEL</li>
						<li>02) 869-3001</li>
						<li>FAX</li>
						<li>02) 6008-6405</li>
					</ul>
				</li>
			</ul>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>