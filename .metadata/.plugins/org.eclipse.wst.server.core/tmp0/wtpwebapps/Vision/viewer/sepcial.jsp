<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> SPECIAL series </title>
	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light special'>
		<div class='special_con1'>
			<h2> <span>SPECIAL series</span> </h2>
			<section>
				<h3><span>특수 조명</span>SPECIAL LIGHT</h3>
				<p>고객님께서 원하시는<span>특수한 조명을 제작</span>가능합니다.아래 예시 사진들을 봐주시고 문의해 주세요.
				</p>
			</section>
			<section>
				<h3 class='blind'></h3>
				<img src='../img/light/710led-1_1.png'alt=''><img src='../img/light/710led-1_2.png'alt=''>
				<ul>
					<li>LAYOUT</li>
					<li>MODEL</li>
				</ul>
				<ul>
					<li class='download'><a href='../download/710LED-Model.pdf' target='_black'><img src='../img/light/download.png'></a></li>
					<li>710LED-1</li>
					
				</ul>
			</section>
			<section>
				<h3 class='blind'></h3>
				<img src='../img/light/800led_1.png'alt=''><img src='../img/light/800led_2.png'alt=''>
				<ul>
					<li>LAYOUT</li>
					<li>MODEL</li>
				</ul>
				<ul>
					<li class='download'><a href='../download/800LED(cross)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></li>
					<li>800LED(cross)-2</li>
					
				</ul>
			</section>
			<section>
				<h3 class='blind'></h3>
				<img src='../img/light/1450-900_1.png'alt=''><img src='../img/light/1450-900_2.png'alt=''>
				<ul>
					<li>LAYOUT</li>
					<li>MODEL</li>
				</ul>
				<ul>
					<li class='download'><a href='../download/1450 900-Model.pdf' target='_black'><img src='../img/light/download.png'></a></li>
					<li>1450 900-1</li>
					
				</ul>
			</section>
			<section>
				<h3 class='blind'></h3>
				<img src='../img/light/2002_1.png'alt=''><img src='../img/light/2002_2.png'alt=''>
				<ul>
					<li>LAYOUT</li>
					<li>MODEL</li>
				</ul>
				<ul>
					<li class='download'><a href='../download/2002-2(new)-1-Model.pdf' target='_black'><img src='../img/light/download.png'></a></li>
					<li>2002-2</li>
					
				</ul>
			</section>
			<section>
				<h3 class='blind'></h3>
				<img src='../img/light/8625.png'alt=''>
				<ul>
					<li>LAYOUT</li>
					<li>MODEL</li>
				</ul>
				<ul>
					<li class='download'><a href='../download/R625(new-2)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></li>
					<li>R625</li>
					
				</ul>
			</section>
			<section>
				<h3 class='blind'></h3>
				<img src='../img/light/assemble_1.png'alt=''><img src='../img/light/assemble_1.png'alt=''>
				<ul>
					<li>LAYOUT</li>
					<li>MODEL</li>
				</ul>
				<ul>
					<li class='download'><a href='../download/분리조립-Model.pdf' target='_black'><img src='../img/light/download.png'></a></li>
					<li>분리조립</li>
					
				</ul>
			</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>