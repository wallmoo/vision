<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VCOL series </title>

<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vcol'>
		<div class='vcol_con1'>
			<h2> <span>VCOL series</span> </h2>
			<section>
				<h3><span>동축 낙사 조명</span>COAXIAL LIGHT</h3>
				<p><span>VCOL series</span>는 조명과 렌즈와 동일 선상에서 빛을 조사하는 동축낙사 형태의 조명입니다.<br/>자사 제품 중 가장 높은 균일도를 가지는 제품으로써, 검사 대상체의 광택이 높아 빛의 산란 및 반사가 많은 제품 검사에 적합한 제품입니다.
				</p>
				<img src='../img/light/vcol_img1.png' alt='vspl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>경면 물체 검사 , 유리,금속 외관 등 표면조도,반사율이 높은 제품의 외관검사</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VCOL은 조명과 렌즈와 동일 선상에서 빛을 조사하는 동축낙사 형태의 조명입니다.<br/>
					표면 조도가 높은 검사대상체에 사용,검사되어지는 제품입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					검사 대상체 표면조도가 높은 제품의 외관 및 프린팅 ,바코드를 검사하는 조명입니다.<br/>
					빛을 경면의 대상물에 직접 조사하여 정반사된 빛의 반사도를 식별하여 원하는 이미지를 검출하기에 특화된 조명입니다.<br/>
					제품의 효과를 극대화 시키기 위해 투과율 50% 반투과유리가 적용 되었습니다.
					
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>동축 낙사 조명 (COAXIAL LIGHT)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th>촬영구(㎜)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VCOL15/15</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>2W</td>
								<td>0.2A/0.1A</td>
								<td>32</td>
								<td>45</td>
								<td>25</td>
								<td>15X15</td>
								<td class='download'><a href='../download/VCOL 15 15J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VCOL 15 15J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VCOL22/20</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>2W</td>
								<td>0.2A/0.1A</td>
								<td>32</td>
								<td>50.5</td>
								<td>32</td>
								<td>22X20</td>
								<td class='download'><a href='../download/VCOL 22 20J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VCOL 22 20J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VCOL27/27</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>4.5W</td>
								<td>0.4A/0.2A</td>
								<td>48</td>
								<td>69</td>
								<td>40</td>
								<td>27X27</td>
								<td class='download'><a href='../download/VCOL 27 27J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VCOL 27 27J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VCOL28/26</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>4.5W</td>
								<td>0.4A/0.2A</td>
								<td>52</td>
								<td>70</td>
								<td>50</td>
								<td>28X26</td>
								<td class='download'><a href='../download/VCOL 28 26J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VCOL 28 26J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VCOL43X43</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>5W</td>
								<td>0.4A/0.2A</td>
								<td>54</td>
								<td>87</td>
								<td>54</td>
								<td>43X43</td>
								<td class='download'><a href='../download/VCOL 43 43J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VCOL 43 43J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VCOL123/105</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>12W</td>
								<td>1A/0.5A</td>
								<td>134</td>
								<td>151</td>
								<td>118</td>
								<td>123X105</td>
								<td class='download'><a href='../download/VCOL 123 105J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VCOL 123 105J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VCOL130/130</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>20W</td>
								<td>1.6A/0.8A</td>
								<td>190</td>
								<td>240</td>
								<td>188</td>
								<td>130X130</td>
								<td class='download'><a href='../download/VCOL 130 130J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VCOL 130 130J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VCOL205/160</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>22W</td>
								<td>1.8A/0.9A</td>
								<td>226</td>
								<td>242</td>
								<td>200</td>
								<td>205X160</td>
								<td class='download'><a href='../download/VCOL 205 160J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VCOL 205 160J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
					
				<img src='../img/light/vcol_img2.png' alt=''>
			</section>
			<section>
				<h3><span>◆</span> 동축낙사 조명의 적용 사례</h3>
				<img src='../img/light/vcol_img3.png' alt=''>
		</section>
		<section>
				<h3><span>◆</span> 동축낙사 조명 예시</h3>
				<img src='../img/light/vcol_img4.png' alt=''>
		</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>