<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VBHL series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vbhl'>
		<div class='vbhl_con1'>
			<h2 class='pc'> <span>VBLL series</span> </h2>
			<h2 class='mobile'> <span>백라이트조명</span> </h2>
			<ul class='mobile'>
				<li ><a href='vbsl_s1.jsp'>VHLS</a></li>
				<li><a href='vbml.jsp'>VLSL</a></li>
				<li class='name'><a href='vbhl.jsp'>VLBL</a></li>
			</ul>
			<section>
				<h3><span>백라이트 대형 조명</span>LED BACK LARGE LIGHT</h3>
				<p><span>VBLL series</span>는 타사 백라이트 제품에 비해 높은 휘도율을 자랑하는 제품입니다.
				</p>
				<img src='../img/light/vbhl_img1.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>PCB 외관 검사 , 고속촬영 및 실루엣 검사, 대형 제품 검사용</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VBL serise는 타사에 비해 높은 휘도율을 자랑하는 제품입니다.<br/>
					발열을 억제하기 위해 특수 제작한 방열판을 채용하였고
					펜을 추가적으로 부착할 수 있는 제품입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					LED PCB를 균일하고 고밀도로 배열하여 균일하고 높은 휘도율을 자랑합니다.<br/>
					광확산판을 채용하여 확산각이 넓고 균일합니다.<br/>
					발열을 잡기 위해 자체 설계한 방열판(VBSL,VBML,VBLL)과 
					추가적으로 펜(VBLL)을 부착하였습니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>백라이트 조명 (LED BACK LARGE LIGHT)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th>조도(Lx)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VBLL640/600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>300W</td>
								<td>12.5A</td>
								<td>640</td>
								<td>600</td>
								<td>40</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 640 600 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 640 600 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL940/600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>600W</td>
								<td>25A</td>
								<td>940</td>
								<td>600</td>
								<td>40</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 940 600 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 940 600 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL940/900</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>900W</td>
								<td>37.5A</td>
								<td>940</td>
								<td>900</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 940 900 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 940 900 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL1240/600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>900W</td>
								<td>37.5A</td>
								<td>1240</td>
								<td>600</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 1240 600 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 1240 600 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL1240/900</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>1200W</td>
								<td>50A</td>
								<td>1240</td>
								<td>900</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 1240 900 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 1240 900 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL1240/1200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>1500W</td>
								<td>62.5A</td>
								<td>1240</td>
								<td>1200</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 1240 1200 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 1240 1200 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL1540/600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>1200W</td>
								<td>50A</td>
								<td>1540</td>
								<td>600</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 1540 600 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 1540 600 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL1540/900</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>1500W</td>
								<td>62.5A</td>
								<td>1540</td>
								<td>900</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 1540 900 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 1540 900 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL1540/1200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>1800W</td>
								<td>75A</td>
								<td>1840</td>
								<td>600</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 1540 1200 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 1540 1200 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL1840/600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>1500W</td>
								<td>75A</td>
								<td>1840</td>
								<td>900</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 1840 900 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 1840 900 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL1840/900</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>1800W</td>
								<td>75A</td>
								<td>1840</td>
								<td>900</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 1840 900 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 1840 900 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL1840/1200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>2100W</td>
								<td>87.5A</td>
								<td>1840</td>
								<td>1200</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 1840 1200 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 1840 1200 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL2440/600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>1800W</td>
								<td>75A</td>
								<td>2440</td>
								<td>600</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 2440 600 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 2440 600 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL2440/900</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>2100W</td>
								<td>87.6A</td>
								<td>2440</td>
								<td>900</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 2440 900 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 2440 900 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBLL2440/1200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>24V</td>
								<td>2400W</td>
								<td>100A</td>
								<td>2440</td>
								<td>1200</td>
								<td>60</td>
								<td>70,000Lx</td>
								<td class='download'><a href='../download/VBLL 2440 1200 60.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBLL 2440 1200 60-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vbhl_img2.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 백라이트 조명의 적용 사례</h3>
					<img src='../img/light/vbsl_s1_img3.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 백라이트 조명 예시</h3>
					<img src='../img/light/vbsl_s1_img4.png' alt=''>
			</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>