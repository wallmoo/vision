<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Certificate</title>

		<jsp:include page="viewerheader.jsp" />
		<!--container-->
		<div class='container certificate'>
			<div class='certi_con1'>
				<h2>
					<span>Certificate</span>
				</h2>
                <section>
				<h3 class='blind'>기술우수기업인증서</h3>
				<img src='../img/certi_img7.jpg' alt='기술우수기업인증서'/>
					<p>기술우수기업인증서</p></section>
                <section>
				<h3 class='blind'>CE인증 VHLS2600(4KK) 조명</h3>
				<img src='../img/certi_img8.jpg' alt='CE인증 VHLS2600(4KK) 조명'/>
					<p>CE인증 VHLS2600(4KK) 조명</p></section>
                <section>
				<h3 class='blind'>CE인증 VT-600 컨트롤러</h3>
				<img src='../img/certi_img9.jpg' alt='CE인증 VT-600 컨트롤러'/>
					<p>CE인증 VT-600 컨트롤러</p></section>    
                        
				<section>
				<h3 class='blind'>공장 등록 증명서</h3>
				<img src='../img/certi_img1.jpg' alt='공장 등록 증명서'/>
					<p>공장 등록 증명서</p></section>
				<section>
				<h3 class='blind'>사업자 등록증</h3>
				<img src='../img/certi_img2.jpg' alt='사업자 등록증'/>
					<p>사업자 등록증</p></section>
				<section>
				<h3 class='blind'>조명사 인정증</h3>
				<img src='../img/certi_img3.jpg' alt='조명사 인정증'/>
					<p>조명사 인정증</p></section>
				<section>
				<h3 class='blind'>(특허) LED조명의 방열장치</h3>
				<img src='../img/certi_img4.jpg' alt='(특허) LED조명의 방열장치'/>
					<p>(특허) LED조명의 방열장치</p></section>
				<section>
				<h3 class='blind'>(특허) 평판 조명 장치</h3>
				<img src='../img/certi_img5.jpg' alt='(특허) 평판 조명 장치'/>
					<p>(특허) 평판 조명 장치</p></section>
				<section>
				<h3 class='blind'>(특허) 피씨가이드를 구비한 돌출간판</h3>
				<img src='../img/certi_img6.jpg' alt='(특허) 피씨가이드를 구비한 돌출간판'/>
					<p>(특허) 피씨가이드를 구비한 돌출간판</p></section>
			</div>
			</div>
			<jsp:include page="viewerfooter.jsp" />
</body>
</html>