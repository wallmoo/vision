<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VBML series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vbml'>
		<div class='vbml_con1'>
			<h2 class='pc'> <span>VBML series</span> </h2>
			<h2 class='mobile'> <span>백라이트조명</span> </h2>
			<ul class='mobile'>
				<li ><a href='vbsl_s1.jsp'>VHLS</a></li>
				<li class='name'><a href='vbml.jsp'>VLSL</a></li>
				<li><a href='vbhl.jsp'>VLBL</a></li>
			</ul>
			<section>
				<h3><span>백라이트 중형 조명</span>LED BACK MEDIUM LIGHT</h3>
				<p><span>VBSL series</span>는 타사 백라이트 제품에 비해 높은 휘도율을 자랑하는 제품입니다.
				</p>
				<img src='../img/light/vbml_img1.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>PCB 외관 검사 , 고속촬영 및 실루엣 검사, 대형 제품 검사용</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VBL serise는 타사에 비해 높은 휘도율을 자랑하는 제품입니다.<br/>
					발열을 억제하기 위해 특수 제작한 방열판을 채용하였고
					펜을 추가적으로 부착할 수 있는 제품입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					LED PCB를 균일하고 고밀도로 배열하여 균일하고 높은 휘도율을 자랑합니다.<br/>
					광확산판을 채용하여 확산각이 넓고 균일합니다.<br/>
					발열을 잡기 위해 자체 설계한 방열판(VBSL,VBML,VBLL)과 
					추가적으로 펜(VBLL)을 부착하였습니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>백라이트 중형 조명(LED BACK MEDIUM LIGH)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VBML345/350</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>50W</td>
								<td>4A/2A</td>
								<td>345</td>
								<td>350</td>
								<td>35</td>
								<td class='download'><a href='../download/VBML 345 350 35.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBML 345 350 35-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBML405/410</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>70W</td>
								<td>5.8A/2.9A</td>
								<td>405</td>
								<td>410</td>
								<td>35</td>
								<td class='download'><a href='../download/VBML 405 410 35.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBML 405 410 35-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBML460/470</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>90W</td>
								<td>7.5A/3.7A</td>
								<td>460</td>
								<td>470</td>
								<td>38</td>
								<td class='download'><a href='../download/VBML 460 470 35.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBML 460 470 35-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBML520/530</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>110W</td>
								<td>9A/4.5A</td>
								<td>520</td>
								<td>530</td>
								<td>40</td>
								<td class='download'><a href='../download/VBML 520 530 40.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBML 520 530 40-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VBML585/600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>150W</td>
								<td>12.5A/6.2A</td>
								<td>585</td>
								<td>600</td>
								<td>40</td>
								<td class='download'><a href='../download/VBML 585 600 40.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VBML 585 600 40-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vbml_img2.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 백라이트 조명의 적용 사례</h3>
					<img src='../img/light/vbsl_s1_img3.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 백라이트 조명 예시</h3>
					<img src='../img/light/vbsl_s1_img4.png' alt=''>
			</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>