<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<jsp:include page="viewerheader.jsp" />
		<!--container-->
	<div class='container light'>
		<div class='light_con1'>
			<h2> <span>Light</span> </h2>
			<section>
				<h3>라인 스캔 조명</h3>
				<dl>
					<a href='vhls.jsp'>
					<dt><img src='../img/light/light_img1.jpg' alt=''/></dt>
					<dd>고휘도 라인 스캔 조명</dd>
					<dd><span>VHLS</span>series</dd>
					</a>
				</dl>
				<dl>
					<a href='vlsl.jsp'>
					<dt><img src='../img/light/light_img2.jpg' alt=''/></dt>
					<dd>고조도 라인 스캔 조명</dd>
					<dd><span>VLSL</span>series</dd>
					</a>
				</dl>
				<dl>
					<a href='vlbl.jsp'>
					<dt><img src='../img/light/light_img3.jpg' alt=''/></dt>
					<dd>바 라인 스캔 조명</dd>
					<dd><span>VLBL</span>series</dd>
					</a>
				</dl>
			</section>
			<section>
				<h3>백라이트 조명</h3>
				<dl>
					<a href='vbsl_s1.jsp'>
					<dt><img src='../img/light/light_img4.jpg' alt=''/></dt>
					<dd>소형 백라이트 조명</dd>
					<dd><span>VBSL</span>series</dd>
					</a>
				</dl>
				<dl>
					<a href='vbml.jsp'>
					<dt><img src='../img/light/light_img6.jpg' alt=''/></dt>
					<dd>중형 백라이트 조명</dd>
					<dd><span>VBML</span>series</dd>
					</a>
				</dl>
				<dl>
					<a href='vbhl.jsp'>
					<dt><img src='../img/light/light_img7.jpg' alt=''/></dt>
					<dd>대형 백라이트 조명</dd>
					<dd><span>VBLL</span>series</dd>
					</a>
				</dl>
			</section>
			<section>
				<h3>링 조명</h3>
				<dl>
					<a href='vrll.jsp'>
					<dt><img src='../img/light/light_img18.jpg' alt=''></dt>
					<dd>저입사각 링 조명</dd>
					<dd><span>VRLL</span>series</dd>
					</a>
				</dl>
				<dl>
					<a href='vrml.jsp'>
					<dt><img src='../img/light/light_img8.jpg' alt=''></dt>
					<dd>중입사각 링 조명</dd>
					<dd><span>VRML</span>series</dd>
					</a>
				</dl>
				<dl>
					<a href='vrhl.jsp'>
					<dt><img src='../img/light/light_img8.jpg' alt=''></dt>
					<dd>고입사각 링 조명</dd>
					<dd><span>VRHL</span>series</dd>
					</a>
				</dl>
				<dl>
					<a href='vrdl.jsp'>
					<dt><img src='../img/light/light_img17.jpg' alt=''></dt>
					<dd>측면조사 링 조명</dd>
					<dd><span>VRDL</span>series</dd>
					</a>
				</dl>
				<dl>
					<a href='vrfl.jsp'>
					<dt><img src='../img/light/light_img9.jpg' alt=''></dt>
					<dd>평판형 링 조명</dd>
					<dd><span>VRFL</span>series</dd>
					</a>
				</dl>
			</section>
			<section>
				<h3>스퀘어 조명</h3>
				<dl>
					<a href='vsql_d.jsp'>
					<dt><img src='../img/light/light_img12.jpg' alt=''></dt>
					<dd>스퀘어 조명 -D</dd>
					<dd><span>VSQL-D</span>series</dd>
					</a>
				</dl>
				<dl>
					<a href='vsql_l.jsp'>
					<dt><img src='../img/light/light_img12.jpg' alt=''></dt>
					<dd>스퀘어 조명 -L</dd>
					<dd><span>VSQL-L</span>series</dd>
					</a>
				</dl>
			</section>
			<section>
				<h3>스폿 조명</h3>
				<dl>
					<a href='vspl.jsp'>
					<dt><img src='../img/light/light_img10.jpg' alt=''></dt>
					<dd>스폿 조명</dd>
					<dd><span>VSPL</span>series</dd>
					<a/>
				</dl>
				<dl>
					<a href='vpsl.jsp'>
					<dt><img src='../img/light/light_img11.jpg' alt=''></dt>
					<dd>파워스폿</dd>
					<dd><span>VPSL</span>series</dd>
					</a>
				</dl>
			</section>
			<section>
				<h3>돔조명</h3>
				<dl>
					<a href='vdml.jsp'>
					<dt><img src='../img/light/light_img13.jpg' alt=''></dt>
					<dd>돔 조명</dd>
					<dd><span>VDML</span>series</dd>
					</a>
				</dl>
				<dl>
					<a href='vfdl.jsp'>
					<dt><img src='../img/light/light_img16.jpg' alt=''></dt>
					<dd>플랫 돔 조명</dd>
					<dd><span>VFDL</span>series</dd>
					</a>
				</dl>
			</section>
			<section>
				<h3>동축 조명</h3>
				<dl>
					<a href='vcol.jsp'>
					<dt><img src='../img/light/light_img14.jpg' alt=''></dt>
					<dd>동축 낙사 조명</dd>
					<dd><span>VCOL</span>series</dd>
					</a>
				</dl>
			</section>
			<section>
				<h3>특수 주문 조명</h3>
				<dl>
					<a href='sepcial.jsp'>
					<dt><img src='../img/light/light_img15.jpg' alt=''></dt>
					<dd>특수 조명</dd>
					<dd><span>SPECIAL</span>series</dd>
					</a>
				</dl>
			</section>
		</div>		
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>