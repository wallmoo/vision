<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VSQL-L series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vsql'>
		<div class='vsql_con1'>
			<h2 class='pc'> <span>VSQL-L series</span> </h2>
			<h2 class='mobile'> <span>스퀘어조명</span> </h2>
			<ul class='mobile'>
				<li ><a href='vsql_d.jsp'>VSQL-D</a></li>
				<li class='name'><a href='vsql_l.jsp'>VSQL-L</a></li>
			</ul>
			<section>
				<h3><span>스퀘어 조명</span> LENS SQUARE LIGHT</h3>
				<p><span>VSQL-L series</span>는 자사 LED BAR 조명을 4방향으로 취부하여 각각 독립적인 각도 조절이 가능하도록 설계된 제품입니다.
				</p>
				<img src='../img/light/vsql_img1.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>이물질 검사,스트립 자재검사, PCB 패턴검사,QFP리드검사,LASER마킹검사,라벨검사</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					다양한 조명 환경을 형성시키는 구조화 된 조명입니다.VSQL 조명은 자사 LED BAR 조명을 4방향으로 취부하여 각각 독립적인 각도 조절이 가능하도록 설계된 제품입니다.<br/>
					다양한 입사각 설정과 설치의 자유도가 높아 설치된 현장에서 다양하고 자유롭게 적용할수있는 조명입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					VSQL조명은 폭넓은 사용범위를 갖습니다.대상체와의 거리를 떨어뜨려 놓은 상태에서 동축조명과 같은 효과를 가질 수 있습니다.<br/>
					대상체와의 거리를 가까이 하고 수평 각도로 조명를 조사하면 링조명과 비슷한 효과를 얻을 수 있습니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>스퀘어 조명(LENS TYPE)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로</th>
								<th>발광장</th>
								<th>총가로</th>
								<th>세로</th>
								<th>높이</th>
								<th>높이2</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VSQL100XN(L)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>14W X N</td>
								<td>1.1A/0.6A</td>
								<td>100N</td>
								<td>100XN-10</td>
								<td>100XN+67</td>
								<td>31</td>
								<td>35</td>
								<td>21</td>
								<td class='download'><a href='../download/VSQL100N(L).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL100N(L)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSQL65(L)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>9W</td>
								<td>0.8A/0.4A</td>
								<td>82</td>
								<td>65</td>
								<td>90</td>
								<td>90</td>
								<td>30</td>
								<td>20</td>
								<td class='download'><a href='../download/VSQL65(L).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL65(L)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSQL80(L)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>14W</td>
								<td>1.1A/0.6A</td>
								<td>95</td>
								<td>80</td>
								<td>122</td>
								<td>16</td>
								<td>35</td>
								<td>21</td>
								<td class='download'><a href='../download/VSQL80(L).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL80(L)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSQL200(L)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>28W</td>
								<td>2.3A/1.1A</td>
								<td>212</td>
								<td>202</td>
								<td>279</td>
								<td>31</td>
								<td>35</td>
								<td>21</td>
								<td class='download'><a href='../download/VSQL200(L).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL200(L)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSQL400(L)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>56W</td>
								<td>4.6A/2.3A</td>
								<td>421</td>
								<td>402</td>
								<td>479</td>
								<td>31</td>
								<td>35</td>
								<td>21</td>
								<td class='download'><a href='../download/VSQL400(L).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL400(L)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSQL500(L)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>70W</td>
								<td>5.8A/2.9A</td>
								<td>512</td>
								<td>502</td>
								<td>579</td>
								<td>31</td>
								<td>35</td>
								<td>21</td>
								<td class='download'><a href='../download/VSQL500(L).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSQL500(L)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
					
				<img src='../img/light/vsql_img2.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 스퀘어 조명 예시</h3>
					<img src='../img/light/vhis_img4.jpg' alt=''>
			</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>