<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VRFL series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vrfl'>
		<div class='vrfl_con1'>
			<h2  class='pc'> <span>VRFL series</span> </h2>
			<h2 class='mobile'> <span>링조명</span> </h2>
			<ul class='mobile'>
				<li ><a href='vrll.jsp'>VRLL</a></li>
				<li><a href='vrml.jsp'>VRML</a></li>
				<li><a href='vrhl.jsp'>VRHL</a></li>
				<li><a href='vrdl.jsp'>VRDL</a></li>
				<li class='name'><a href='vrfl.jsp'>VRFL</a></li>
			</ul>
			<section>
				<h3><span>평판형 링 조명</span> FLAT RING LIGHT</h3>
				<p><span>VRFL series</span>는 수평을 기준으로0°의 입사각을 갖는 제품입니다.<br/>가장 일반적인 링조명 형태로써 산업 전반에 거쳐 가장 많이 사용 되며, 일반적인 조명입니다.
				</p>
				<img src='../img/light/vrfl_img1.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>크랙 검사, PCB 검사, 검사대상체 외형 검사 등</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VRFLseries는 수평을 기준으로 0°의 입사각을 갖는 제품입니다.가장 일반적인 링조명 형태로써 산업 전반에 거쳐 가장 많이 사용 되며, 일반적인 조명입니다.<br/>
					입사각이 0°로써 자사 동축조명 VCOL과 비슷한 효과를 가지고 있는 조명입니다.제품 악세사리인 탈부착형 광확산판을 채용하여 다방면의 검사용도로 활용 하실 수 있습니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					0°의 입사각을 가지는 제품으로써 검사대상체에 다양한 각도로 빛을 조사함으로써 음,양각이 없는 균일한 제품검사 영상을 확보할수 있는 제품입니다.<br/>
					검사 대상체에 조명을 멀리 할수록 동축조명과 근접한 검사 촬영값을 얻을 수 있습니다.반대로 근접 시킬수록 검사 검출이 효과 적입니다.<br/>
					탈 부착형 광확산판을 부착하면, 표면조도가 높은 검사 대상체 검사에 더 적합합니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>평판형 링 조명(입사각 0°)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>외경(Ø)</th>
								<th>내경(Ø)</th>
								<th>높이(㎜)</th>
								<th>LED(Ø)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VRFL46/15(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>4W</td>
								<td>0.3A/0.1A</td>
								<td>46</td>
								<td>15</td>
								<td>20</td>
								<td>3</td>
								<td class='download'><a href='../download/VRFL46 15J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL46 15J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRFL50/20(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>4.8W</td>
								<td>0.4A/0.2A</td>
								<td>50</td>
								<td>20</td>
								<td>17</td>
								<td>3</td>
								<td class='download'><a href='../download/VRFL50 20J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL50 20J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRFL66/26(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>6.5W</td>
								<td>0.5A/0.3A</td>
								<td>66</td>
								<td>26</td>
								<td>20</td>
								<td>3</td>
								<td class='download'><a href='../download/VRFL66 26.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL66 26-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRFL74/35(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>7W</td>
								<td>0.6A/0.3A</td>
								<td>74</td>
								<td>35</td>
								<td>20</td>
								<td>3</td>
								<td class='download'><a href='../download/VRFL74 35J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL74 35J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRFL81/38</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>7.6W</td>
								<td>0.6A/0.3A</td>
								<td>81</td>
								<td>38</td>
								<td>20</td>
								<td>3</td>
								<td class='download'><a href='../download/VRFL81 38.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL81 38-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRFL90/28</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>8W</td>
								<td>0.7A/0.3A</td>
								<td>90</td>
								<td>28</td>
								<td>20</td>
								<td>3</td>
								<td class='download'><a href='../download/VRFL90 30J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL90 30J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRFL95/42(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>10W</td>
								<td>0.8A/0.4A</td>
								<td>95</td>
								<td>42</td>
								<td>20</td>
								<td>3</td>
								<td class='download'><a href='../download/VRFL95 42J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL95 42J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRFL120/40(K)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>15W</td>
								<td>1.3A/6.5A</td>
								<td>120</td>
								<td>40</td>
								<td>20</td>
								<td>5</td>
								<td class='download'><a href='../download/VRFL120 40.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL120 40-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRFL150/47(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>20W</td>
								<td>1.6A/0.8A</td>
								<td>150</td>
								<td>47</td>
								<td>20</td>
								<td>5</td>
								<td class='download'><a href='../download/VRFL150 47J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL150 47J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
					<p>평판형 링조명 (입사각 0°,서포터 브라켓 부착형)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th rowspan='2'>MODEL</th>
								<th rowspan='2'>COLOR</th>
								<th rowspan='2'>VOT</th>
								<th rowspan='2'>WAT</th>
								<th rowspan='2'>CURRET</th>
								<th colspan='2'>외경(Ø)</th>
								<th colspan='2'>내경(Ø)</th>
								<th colspan='3'>높이(㎜)</th>
								<th rowspan='2' colspan='2'>download</th>
							</tr>
							<tr>
								<th>제품</th>
								<th>브라켓</th>
								<th>제품</th>
								<th>브라켓</th>
								<th>조명</th>
								<th>브라켓</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VRFL66/26(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>6.5W</td>
								<td>0.5A/0.3A</td>
								<td>66</td>
								<td>38</td>
								<td>26</td>
								<td>30</td>
								<td>20</td>
								<td>9</td>
								<td>29</td>
								<td class='download'><a href='../download/VRFL66 26J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL66 26J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRFL150/47(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>8W</td>
								<td>0.7A/0.4A</td>
								<td>150</td>
								<td>42</td>
								<td>47</td>
								<td>30</td>
								<td>20</td>
								<td>10</td>
								<td>30</td>
								<td class='download'><a href='../download/VRFL150 47J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRFL150 47J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vrfl_img2.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 링조명의 적용 사례</h3>
					<img src='../img/light/vrfl_img3.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 평판형 링 조명 예시</h3>
					<img src='../img/light/vrfl_img4.png' alt=''>
			</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>