<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VDML series </title>

<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vdml'>
		<div class='vdml_con1'>
			<h2 class='pc'> <span>VDML series</span> </h2>
			<h2 class='mobile'> <span>돔조명</span> </h2>
			<ul class='mobile'>
				<li class='name'><a href='vdml.jsp'>VDML</a></li>
				<li><a href='vfdl.jsp'>VFDL</a></li>
			</ul>
			<section>
				<h3><span>돔 조명</span>LED DOME LIGHT</h3>
				<p><span>VDML series</span>는 돔 형태의 조명으로써 간접반사의 대표적인 모델입니다.<br/>
					자사 제품 중 가장 높은 균일도를 가지는 제품으로써, 검사 대상체의 광택이 높아 빛의 산란 및 반사가 많은 제품 검사에 적합한 제품입니다.
				</p>
				<img src='../img/light/vdml_img1.png' alt='vdml'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>광택이 강한 포장재, 표면형태 입체적인 제품, 바코드 인쇄검사, 알약 마킹검사, 인쇄 문자인식, 금속표면검사, 기판 부품 검사 등에 쓰입니다.</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					검사대상체에 전 방향으로 균일도 높은 빛이 입사되는 제품으로써, 검사 대상체 표면조도가 높아 반사,산란이 많은 제품과 굴절, 표면광택이 높은 대상체 검사에<br/>
					적합한 제품입니다. 돔 형상의 사출에 LED 광을 조사하여, 사출로부터 반사된 빛을 간접적으로 검사대상체에 빛을 조사합니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
						자사 제품중 가장 일정한 균일도를 자랑하는 조명입니다. 돔형태의 조명으로써 거의 모든 입사각으로 빛이 고르게 제품에 조사되어 그림자가 거의 생기지 않고<br/> 
						대상물 전체를 검사 할 수 있습니다. 제품설계 상, 검사대상체 크기에 따라 이미지 밝기가 어두울 수 있습니다.<br/> 
						이 점을 보안하기 위해 카메라 개구 상기 및 자사제품 동축 조명(VCOL) 추가 사용을 권장합니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>돔 조명(LED DOME TYPE)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>높이(㎜)</th>
								<th>외경(Ø)</th>
								<th>내경(Ø)</th>
								<th>내경(Ø)2</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VDML70/35J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>2W</td>
								<td>0.2A/0.1A</td>
								<td>46.5</td>
								<td>70</td>
								<td>35</td>
								<td>50</td>
								<td class='download'><a href='../download/VDML 70 35J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VDML 70 35J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VDML90/25</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>5W</td>
								<td>0.4A/0.2A</td>
								<td>57</td>
								<td>60</td>
								<td>25</td>
								<td>70</td>
								<td class='download'><a href='../download/VDML 90 25.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VDML 90 25-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VDML116/50J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>7W</td>
								<td>0.6A/0.3A</td>
								<td>65</td>
								<td>116</td>
								<td>50</td>
								<td>95</td>
								<td class='download'><a href='../download/VDML 116 50J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VDML 116 50J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VDML120/30</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>7W</td>
								<td>0.6A/0.3A</td>
								<td>72</td>
								<td>120</td>
								<td>30</td>
								<td>100</td>
								<td class='download'><a href='../download/VDML 120 30.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VDML 120 30-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VDML150/35</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>9W</td>
								<td>0.8A/0.4A</td>
								<td>92</td>
								<td>150</td>
								<td>35</td>
								<td>130</td>
								<td class='download'><a href='../download/VDML 150 35.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VDML 150 35-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VDML192/40J</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>10W</td>
								<td>0.9/0.45A</td>
								<td>112.5</td>
								<td>192</td>
								<td>40</td>
								<td>150</td>
								<td class='download'><a href='../download/VDML 192 40J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VDML 192 40J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VDML220/40</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>12W</td>
								<td>1A/0.5A</td>
								<td>112</td>
								<td>220</td>
								<td>40</td>
								<td>190</td>
								<td class='download'><a href='../download/VDML 220 40.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VDML 220 40-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VDML250/50</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>14W</td>
								<td>1.2A/0.6A</td>
								<td>132</td>
								<td>250</td>
								<td>50</td>
								<td>210</td>
								<td class='download'><a href='../download/VDML 250 50.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VDML 250 50-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VDML430/80</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>24W</td>
								<td>2A/1A</td>
								<td>212</td>
								<td>430</td>
								<td>80</td>
								<td>390</td>
								<td class='download'><a href='../download/VDML 430 80.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VDML 430 80-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
			</section>
			<section>
					<h3><span>◆</span> 돔 조명의 적용 사례</h3>
					<img src='../img/light/vdml_img2.png' alt=''>
					<img src='../img/light/vdml_img3.png' alt=''>
					<img src='../img/light/vdml_img4.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 돔 조명 샘플</h3>
					<img src='../img/light/vdml_img5.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 돔 조명 예시</h3>
					<img src='../img/light/vdml_img6.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 돔 조명 조도분표</h3>
					<p>돔 조명 조도분포 (대표 예)</p>
					<p>	임이의 범위 내에서 100x100 (=10000)포인트의 농도 계측을 하여 각각의 밝기 분포를 표시합니다.<br/>
						분포 표시는 모델별로 LWD를 변경해서 취득한 데이터 중에서 가장 밝은 포인트를 100으로 했을때의 상대 표시입니다.</p>
					<img src='../img/light/vdml_img7.png' alt=''>
					<img src='../img/light/vdml_img8.png' alt=''>
					<p>	※ 왼쪽에 기재된 데이터는 대표적인 예로 상품의 품질을 보증하는 것은 아닙니다.<br/>
						※ LWD =  조명부터 계측 대상 물체까지의 거리</p>
			</section>
			<section>
					<h3><span>◆</span> 돔 조명 동축나사</h3>
					<img src='../img/light/vdml_img9.png' alt=''>
			</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>