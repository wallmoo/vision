<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VSPL series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vspl'>
		<div class='vspl_con1'>
				<h2 class='pc'> <span>VSPL series</span> </h2>
				<h2 class='mobile'> <span>스폿조명</span> </h2>
				<ul class='mobile'>
					<li class='name'><a href='vspl.jsp'>VSPL</a></li>
					<li ><a href='vpsl.jsp'>VPSL</a></li>
				</ul>
			<section>
				<h3><span>스폿 조명</span> SPOT LIGHT</h3>
				<p><span>VSPL series</span>는 크기가 작은 조명으로써 좁고 협소한 공간에서 효과 적인 조명입니다.
				</p>
				<img src='../img/light/vspl_img1.png' alt='vspl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>반도체,PCB 마킹검사 , 금속면 인쇄 ,라벨 검사</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					조명,취급이 간단한 헤드사이즈입니다.
					좁고 협소한 공간에서도 확산광을 이용한 균일 조사를 실현 할 수 있습니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					협소한 공간에서 효과를 내는 조명입니다.설치가 자유로워 여러가지 형태로 응용이 가능한 제품입니다.<br/>
					공간 절약 설치, 원포인트 스팟 조명
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>스폿 조명(SPOT TYPE)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>길이(㎜)</th>
								<th>외경(Ø)</th>
								<th>내경(Ø)</th>
								<th>발광장(Ø)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VSPL30/20</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>1W</td>
								<td>1㎃/0.5㎃</td>
								<td>30</td>
								<td>20</td>
								<td>8</td>
								<td>6</td>
								<td class='download'><a href='../download/VSPL 6 50.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSPL 6 30-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSPL50/27</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>1W</td>
								<td>1㎃/0.5㎃</td>
								<td>50</td>
								<td>27</td>
								<td>8</td>
								<td>6</td>
								<td class='download'><a href='../download/VSPL 6 52.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSPL 6 50-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSPL53/30</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>3W</td>
								<td>3㎃/1.5㎃</td>
								<td>53</td>
								<td>30</td>
								<td>8</td>
								<td>6</td>
								<td class='download'><a href='../download/VSPL 6 52.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSPL 6 52-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VSPL67/30</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>6W</td>
								<td>5㎃/2.5㎃</td>
								<td>67</td>
								<td>30</td>
								<td>8</td>
								<td>6</td>
								<td class='download'><a href='../download/VSPL 6 67.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSPL 6 67-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
					
				<img src='../img/light/vspl_img2.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 스폿 조명의 적용 사례</h3>
					<img src='../img/light/vspl_img3.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 스폿 조명 예시</h3>
					<img src='../img/light/vspl_img4.png' alt=''>
			</section>
		</div>
	<jsp:include page="viewerfooter.jsp" />
</body>
</html>