<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0 " />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>게시판 글쓰기</title>
<link type="text/css" rel="stylesheet" href="coustom/css/bootstrap.css" />
<link type="text/css" rel="stylesheet" href="css/main.css" />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script src="coustom/js/bootstrap.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
</head>
<body>
	<div>
	<jsp:include page="header.jsp"/>
		<form action="./write.bs?type=52" method="post"
			enctype="multipart/form-data" id="frm">
			<div class='container'>
			<div class='board_content'>
			<h2> <span>Online QnA</span> </h2>
				<div>
					<table>
						<caption class='blind'>목록</caption>
						<thead>
							<tr>
								<th colspan='5'>게시판 글쓰기</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>제목</td>
								<td><input type="text" size="50" id="title" name="title" /></td>
								<td>작성자</td>
								<td><input type="text" id="writer" name="writer" /></td>

							</tr>
							<tr>
								<td>첨부파일</td>
								<td><input type="file" id="file" name="file"></td>
								<td>비밀번호</td>
								<td><input type="password" id="password" name="password" /></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<textarea id="contents" name="contents"></textarea>
					<script>
						//<![CDATA[
							CKEDITOR.replace('contents',{
								filebrowserUploadUrl: '',
								enterMode:'2',
								shiftEnterMode:'3',
								width:'99.8%',
								height:'300',
								toolbar : [
									 ['Source','-','Save','NewPage','Preview','-','Templates'],
									 ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
									 ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
									 ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
									 '/',
									 ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
									 ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
									 ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
									 ['Link','Unlink','Anchor'],
									 ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
									 '/',
									 ['Styles','Format','Font','FontSize'],
									 ['TextColor','BGColor'],
									 ['Maximize', 'ShowBlocks','-','About']
								],
								filebrowserUploadUrl:'./upload.jsp?'
							        +'realUrl=http://dostory.co.kr:8080/vision/'
							        +'&realDir=/home/vision/textupload/'
							});
							
						//]]
					</script>
				</div>
				<div align="right">
					<p><a onclick="submitFuntion()">등록</a></p>
				</div>
			</div>
			</div>
		</form>
		<script>
			function submitFuntion() {
				var title = $("#title").val();
				var writer = $("#writer").val();
				var pw = $("#password").val();
				var content = CKEDITOR.instances.contents.getData();
				if(title==null || title=="" || title == undefined || writer==null || writer=="" || writer == undefined || pw==null || pw=="" || pw == undefined ||
						content==null || content=="" || content == undefined) {
					alert("모두 작성해주세요.");
				} else {
					$("#frm").submit();	
				}
			}
		</script>
		</div>
		<jsp:include page="footer.jsp"/>
</body>
</html>