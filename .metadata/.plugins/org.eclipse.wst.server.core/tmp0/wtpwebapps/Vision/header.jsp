<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0"/>
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<link type="text/css" rel="stylesheet" href="css/main.css" />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/main_m2.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/serch.js"></script>
<!-- js Library -->
</head>
<body>
 <div>
	<div class='header'>
			<!-- 본문바로가기 -->
			<div id="skipNavi">
				<a href="#container">본문바로가기</a>
			</div>
			<div class='header_wrap'>
				<!-- 로고 -->
				<h1>
					<a href="index.jsp"><img src="img/logo_vision-tech.png" alt="vision tech" /></a>
				</h1>
				<img src="img/m.button.png" alt="menu" class='mbutton' />
				<h2>
					<a href="index.jsp"><img src="img/m.logo_vision-tech.png" alt="vision tech" /></a>
				</h2>
				<img src="img/m.search.png" alt="search" class='msearch' />
				<!--네비-->
				<nav class='gnb'>
				<ul>
					<li><a href="javascript:subMove(1);" >Company</a>
						<ul>
							<li><a href="javascript:subMove(1);">인사말 및 경영이념</a></li>
							<li><a href="javascript:subMove(2);">회사연혁</a></li>
							<li><a href="javascript:subMove(3);">인증현황</a></li>
							<li><a href="javascript:subMove(4);">오시는 길</a></li>
						</ul></li>
					<li><a href="javascript:subMove(14);">Light</a>
						<ul>
							<li><a href="javascript:subMove(5);">라인 스캔 조명</a></li>
							<li><a href="javascript:subMove(6);">백라이트 조명</a></li>
							<li><a href="javascript:subMove(7);">링 조명</a></li>
							<li><a href="javascript:subMove(8);">스퀘어 조명</a></li>
							<li><a href="javascript:subMove(9);">스폿 조명</a></li>
							<li><a href='javascript:subMove(15);'>돔 조명</a></li>
							<li><a href='javascript:subMove(16);'>동축 조명</a></li>
							<li><a href="javascript:subMove(10);">특수 주문 조명</a></li>
						</ul></li>
					<li><a href="javascript:subMove(13);">Controller</a>
						<ul>
								<li><a href="javascript:subMove(12);">정전압</a></li>
								<li><a href="javascript:subMove(13);">정전류</a></li>
							</ul>
							</li>
					<li><a href="javascript:subMove(51);">Costomer Support</a>
						<ul>
							<li><a href="javascript:subMove(51);">공지사항</a></li>
							<li><a href="javascript:subMove(52);">온라인 문의</a></li>
							<li><a href="javascript:subMove(53);">주문 의뢰서</a></li>
						</ul></li>
					<li><a href="javascript:subMove(21);">Data Room</a>
						<ul>
							<li><a href="javascript:subMove(21);">카탈로그</a></li>
							<li><a href="javascript:subMove(22);">동영상</a></li>
							<li><a href="javascript:subMove(54);">관련자료</a></li>
						</ul></li>
				</ul>
				</nav>
				<!-- 모바일 전체메뉴-->
				<nav class='allmenu'>
				<h3>
					전체 메뉴 보기<img src="img/m.close.png" alt="전체 메뉴 닫기" />
				</h3>
				<ul>
					<li><a href="javascript:subMove(1);" class="submenu">Company</a>
						<ul>
							<li><a href="javascript:subMove(1);">인사말 및 경영이념</a></li>
							<li><a href="javascript:subMove(2);">회사연혁</a></li>
							<li><a href="javascript:subMove(3);">인증현황</a></li>
							<li><a href="javascript:subMove(4);">오시는 길</a></li>
						</ul></li>
					<li><a href="javascript:subMove(14);" class="submenu">Light</a>
						<ul>
							<li><a href="javascript:subMove(5);">라인 스캔 조명</a></li>
							<li><a href="javascript:subMove(6);">백라이트 조명</a></li>
							<li><a href="javascript:subMove(7);">링 조명</a></li>
							<li><a href="javascript:subMove(8);">스퀘어 조명</a></li>
							<li><a href="javascript:subMove(9);">스폿 조명</a></li>
							<li><a href='javascript:subMove(15);'>돔 조명</a></li>
							<li><a href='javascript:subMove(16);'>동축 조명</a></li>
							<li><a href="javascript:subMove(10);">특수 주문 조명</a></li>
							
						</ul>
					<li><a href="javascript:subMove(13);" class="submenu">Controller</a>
						<ul>
								<li><a href="javascript:subMove(12);">정전압</a></li>
								<li><a href="javascript:subMove(13);">정전류</a></li>
							</ul>
							</li>
					<li><a href="javascript:subMove(51);" class="submenu">Costomer Support</a>
						<ul>
							<li><a href="javascript:subMove(51);">공지사항</a></li>
							<li><a href="javascript:subMove(52);">온라인 문의</a></li>
							<li><a href="javascript:subMove(53);">주문 의뢰서</a></li>
						</ul></li>
					<li><a href="javascript:subMove(21);" class="submenu">Data Room</a>
						<ul>
							<li><a href="javascript:subMove(21);">카탈로그</a></li>
							<li><a href="javascript:subMove(22);">동영상</a></li>
							<li><a href="javascript:subMove(54);">관련자료</a></li>
						</ul></li>
				</ul>
				</nav>
				<!--검색창-->
				<form action="./serch.bs?type=52" method="post" class="search">
					<p>검색할 키워드를 입력하세요.</p>
					<fieldset>
						<legend class="blind">검색하기</legend>
						<label for="srch" class="blind">검색어 입력</label> <input
							type="search" placeholder="Search" class='search_type' name="srch" /><input
							type="submit" value="검색" class='search_btn'/>
					</fieldset>
				</form>
			</div>
		</div>