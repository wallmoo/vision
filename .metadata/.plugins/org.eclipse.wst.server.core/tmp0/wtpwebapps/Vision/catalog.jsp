<%@page import="bean.MediaBean"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	// 전달된 객체 가져오기 
	List medialist = (List) request.getAttribute("medialist");
	MediaBean mediaBean = null;
	int maxPage = (Integer) request.getAttribute("maxPage");
	int startPage = (Integer) request.getAttribute("startPage");
	int endPage = (Integer) request.getAttribute("endPage");
	int listCount = (Integer) request.getAttribute("listCount");
	int nowPage = (Integer) request.getAttribute("page");
	int type = (Integer) request.getAttribute("type");
	String msg = (String) request.getAttribute("fail");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Video</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0 " />
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<link type="text/css" rel="stylesheet" href="css/main.css" />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<!-- js Library -->
</head>
<body>
	<div>
		<jsp:include page="header.jsp" />
		<!--container-->
		<div class='container video'>
			<div class='video_con1'>
				<%
			if(type==21) {
		%>
				<h2>
					<span>Catalog</span>
				</h2>
				<%		
			} else if (type==22) {
		%>
				<h2>
					<span>Video</span>
				</h2>
				<%		
			}
		%>
				<%
				for(int i=0; i<medialist.size();i++) {
					mediaBean = (MediaBean) medialist.get(i);
			%>
				<div onclick="moveDetail('<%=mediaBean.getMedia_type()%>','<%=mediaBean.getMedia_num()%>','<%=nowPage%>')">
					<p>
						등록일: <span><%=mediaBean.getMedia_date().substring(0,11) %></span>
					</p>
					<p>
						조회수: <span><%=mediaBean.getMedia_count() %></span>
					</p>
					<p>
						상세보기
					</p>
					<dl>
						<dt>
						<!--<iframe width="560" height="315" src="https://www.youtube.com/embed/oULcV0n6F8c" frameborder="0" allowfullscreen></iframe>  -->
						
						<%	
							if(type==22) {
						%>
							<iframe width="330" height="210"  src="https://www.youtube.com/embed/<%=mediaBean.getMedia_filename()%>" autohide=1></iframe>
						<%		
							} else {
						%>
							<img width="330" height="210"
								src="./upload/type0/<%=mediaBean.getMedia_filename()%>" />	
						<%		
							}
						%>
						</dt>
						<script>
							function moveDetail(type, num, page) {
								location.href = "./detail.md?type="+type+"&num="+num+"&page="+page;
							}
						</script>
						<dd><%=mediaBean.getMedia_title() %></dd>
					</dl>
				</div>
				<%
				}
			%>
				<div  class='pager'>
					<%
					if (nowPage == 1) {
				%>

					<img src='img/arrow_left.png' alt='이전페이지 이동'>
					<%
					} else {
				%>

					<img src='img/arrow_left.png' alt='이전페이지 이동'
						onclick="location.href='./list.bs?type=<%=type%>&page=<%=nowPage - 1%>'">
					<%
					}
					for (int j = 1; j <= maxPage; j++) {
				%>
					<a href='./list.bs?type=<%=type%>&page=<%=j%>'><%=j%></a>
					<%
					}
					if (nowPage == maxPage) {
				%>
					<img src='img/arrow_right.png' alt='다음페이지 이동'>

					<%
					} else {
				%>
					<img src='img/arrow_right.png' alt='다음페이지 이동'
						onclick="location.href='./list.bs?type=<%=type%>&page=<%=nowPage + 1%>'">

					<%
					}
				%>
				</div>
			</div>
			<jsp:include page="footer.jsp" />
</body>
</html>