<%@page import="bean.BBSBean"%>
<%@page import="bean.ComentBean"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	// 전달된 객체 가져오기 
	BBSBean bbsBean = (BBSBean) request.getAttribute("bbsList");

	List comentList = (List) request.getAttribute("comentlist");
	ComentBean comentbean = null;
	// 기본 자료형을 setAttribute() 로 전달했을 경우
	// getAttribute() 리턴형이 Object 이므로 기본자료형으로 직접 형변환 불가
	// 따라서 Wrapper 클래스 타입으로 형변환 후 기본자료형에 저장 필수!
	int maxPage = (Integer) request.getAttribute("maxPage");
	int startPage = (Integer) request.getAttribute("startPage");
	int endPage = (Integer) request.getAttribute("endPage");
	int listcount = (Integer) request.getAttribute("listCount");
	int comentCount = 0;
	if((Integer) request.getAttribute("comentCount")!=null) {
	comentCount = (Integer) request.getAttribute("comentCount");	
	}
	
	int nowPage = (Integer) request.getAttribute("page");
	int type = (Integer) request.getAttribute("type");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<title>VISIONTECH</title>
<link rel="stylesheet" href="adminD/css/do_layout.css">
<!-- radio,check -->
<link href="adminD/css/_all.css" rel="stylesheet">
<script src="adminD/js/jquery.js"></script>
<script src="adminD/js/icheck.js"></script>
<!--달력-->
<link rel="stylesheet" type="text/css"
	href="adminD/css/jquery.datetimepicker.css" />
<!-- 메뉴 -->
<link rel="stylesheet" href="adminD/dist/css/superfish.css"
	media="screen">
<!-- <script src="dist/js/jquery.js"></script> -->
<script src="adminD/dist/js/superclick.js"></script>
<script src="js/content.js"></script>

<script>
	(function($) { //create closure so we can safely use $ as alias for jQuery

		$(document).ready(function() {

			// initialise plugin
			var example = $('#example').superclick({
			//add options here if required
			});

			// buttons to demonstrate Superclick's public methods
			$('.destroy').on('click', function() {
				example.superclick('destroy');
				return false;
			});

			$('.init').on('click', function() {
				example.superclick();
				return false;
			});

			$('.open').on('click', function() {
				example.children('li:first').superclick('show');
				return false;
			});

			$('.close').on('click', function() {
				example.children('li:first').superclick('hide');
				return false;
			});

			$('.closeall').on('click', function() {
				example.superclick('reset');
				return false;
			});
		});

	})(jQuery);
</script>

</head>
<body>
	<div class="do_wrap">
		<jsp:include page="adminheader.jsp"></jsp:include>
			<div class="right_box">
				<div class="manage_box h1000">
					<h2>게시물 상세보기</h2>
					<div class="table_box">
	            		<table cellpadding="0" cellspacing="0">
			                <tr>
			                    <th class="title" colspan="2" width="90%"><%=bbsBean.getBbs_title()%><span class="title_line"></span></th>
			                    <th class="view_date"><%=bbsBean.getBbs_date().substring(0,11)%></th>
			                </tr>
			                <tr>
			                    <td class="view_content" colspan="3"><%=bbsBean.getBbs_content()%>
			                    
			                    <div class="file" align="left">
			                    <span class="up_span_front">첨부화일&nbsp;&nbsp;|&nbsp;&nbsp;</span>
			                    <%
			                    	if(bbsBean.getBbs_filename()!=null) {
			                    		
			                    %>
								<span class="up_span_front">
									<a href="./BBSFileDownload.bs?filename=<%=bbsBean.getBbs_filename() %>&type=<%=type%>&id=<%=bbsBean.getBbs_id()%>"><%=bbsBean.getBbs_filename() %> &nbsp;&nbsp;</a>
								</span>
										
								<%
									}else{
								%>
									<span class="up_span_front">첨부파일 없음</span>
								<%
								}
								%>
			                    </div>
			                    </td>
			                </tr>
	            		</table>
           		 </div>
				<% 
				if(comentList !=null)  {
				%>
					<div id="sub_area2">
					<form action="#" method="post" class="">
						<p>덧글(<span id="count"><%=comentCount %></span>)</p>
						<fieldset>
							<legend class="blind">검색하기</legend>
							<label for="srch" class="blind">검색어 입력</label>
							<textarea id="coment"></textarea>
							<input type="button" value="등록" onclick="comentSubmit('<%=session.getAttribute("admin")%>','null','<%=bbsBean.getBbs_num()%>','<%=bbsBean.getBbs_type()%>')" />
						</fieldset>
					</form>
				</div>
			
				
				<div id="sub_area">
				
				<%for (int k = 0; k < comentList.size(); k++) {
					comentbean = (ComentBean) comentList.get(k);
					if(comentbean.getDeleteCheck()!=1) {
				%>
					<div class='comment'>
						<dl>
							<dt>
								<span><%=comentbean.getUserid() %></span> | <span><%=comentbean.getRegdate().substring(0,16) %></span>
							</dt>
							<dd id="coment<%=k%>"><%=comentbean.getContent() %></dd>
						</dl>
						<ul>
							<li><a href="javascript:reWriteFunction('coment<%=k%>','<%=comentbean.getType() %>','<%=comentbean.getBoardid() %>','<%=comentbean.getId() %>','<%=k %>');" id="modeSave<%=k%>">수정</a></li>
							<li><a href="javascript:submitDelFunction('<%=comentbean.getType()%>','<%=comentbean.getBoardid() %>','<%=comentbean.getContent() %>','d','<%=comentbean.getId()%>');">삭제</a></li>
						</ul>
					</div>
				<%
					}
				}
				%>
				</div>	
				<%
				}
				%>
					
							<div class="f_right">
					<%
						if(bbsBean.getBbs_deleteStatus()!=1) {
					%>
							<button>
								<a href="./detail.ad?type=<%=bbsBean.getBbs_type() %>&num=<%=bbsBean.getBbs_num() %>&page=<%=nowPage %>&mode=rewrite" class="b_g" >수정</a>
							</button>
							<button>
								<a href="./statuscg.ad?type=<%=bbsBean.getBbs_type() %>&num=<%=bbsBean.getBbs_num() %>&mode=delete" class="b_g">삭제</a>
							</button>
							
					<%		
						} else if (bbsBean.getBbs_deleteStatus()!=1 && bbsBean.getBbs_type() == 52) {
					%>
							<button>
								<a href="./detail.bs?type=<%=bbsBean.getBbs_type() %>&num=<%=bbsBean.getBbs_num() %>&page=<%=nowPage %>&mode=rewrite" class="b_g" >댓글</a>
							</button>
							<button>
								<a href="./statuscg.ad?type=<%=bbsBean.getBbs_type() %>&num=<%=bbsBean.getBbs_num() %>&mode=delete" class="b_g">삭제</a>
							</button>
					<%		
						}
					%>		
						<button>	
							<a href="javascript:history.back();" class="b_g">목록</a>
						</button>
					</div>				
				</div>
				<jsp:include page="adminfooter.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			$('.login input').iCheck({
				checkboxClass : 'icheckbox_square-top',
				// radioClass: 'iradio_square-green',
				increaseArea : '20%'
			});
		});
	</script>
	<!-- 달력 -->
	<!-- <script src="js/jqueryC.js"></script> -->
	<script src="adminD/build/jquery.datetimepicker.full.js"></script>
	<script>
		/*
		window.onerror = function(errorMsg) {
		$('#console').html($('#console').html()+'<br>'+errorMsg)
		}*/

		$.datetimepicker.setLocale('en');

		$('#datetimepicker_format').datetimepicker({
			value : '2015/04/15 05:03',
			format : $("#datetimepicker_format_value").val()
		});
		$("#datetimepicker_format_change").on(
				"click",
				function(e) {
					$("#datetimepicker_format").data('xdsoft_datetimepicker')
							.setOptions(
									{
										format : $(
												"#datetimepicker_format_value")
												.val()
									});
				});
		$("#datetimepicker_format_locale").on("change", function(e) {
			$.datetimepicker.setLocale($(e.currentTarget).val());
		});

		$('#datetimepicker').datetimepicker({
			dayOfWeekStart : 1,
			lang : 'en',
			disabledDates : [ '1986/01/08', '1986/01/09', '1986/01/10' ],
			startDate : '1986/01/05'
		});
		$('#datetimepicker').datetimepicker({
			value : '2015/04/15 05:03',
			step : 10
		});

		$('.some_class').datetimepicker();

		$('#default_datetimepicker').datetimepicker({
			formatTime : 'H:i',
			formatDate : 'd.m.Y',
			//defaultDate:'8.12.1986', // it's my birthday
			defaultDate : '+03.01.1970', // it's my birthday
			defaultTime : '10:00',
			timepickerScrollbar : false
		});

		$('#datetimepicker10').datetimepicker({
			step : 5,
			inline : true
		});
		// 여기
		$('#datetimepicker_mask').datetimepicker({
			mask : '9999/19/39 29:59'
		});
		$('#datetimepicker_mask02').datetimepicker({
			mask : '9999/19/39 29:59'
		});

		$('#datetimepicker1').datetimepicker({
			datepicker : false,
			format : 'H:i',
			step : 5
		});
		$('#datetimepicker2').datetimepicker({
			yearOffset : 222,
			lang : 'ch',
			timepicker : false,
			format : 'd/m/Y',
			formatDate : 'Y/m/d',
			minDate : '-1970/01/02', // yesterday is minimum date
			maxDate : '+1970/01/02' // and tommorow is maximum date calendar
		});
		$('#datetimepicker3').datetimepicker({
			inline : true
		});
		$('#datetimepicker4').datetimepicker();
		$('#open').click(function() {
			$('#datetimepicker4').datetimepicker('show');
		});
		$('#close').click(function() {
			$('#datetimepicker4').datetimepicker('hide');
		});
		$('#reset').click(function() {
			$('#datetimepicker4').datetimepicker('reset');
		});
		$('#datetimepicker5').datetimepicker(
				{
					datepicker : false,
					allowTimes : [ '12:00', '13:00', '15:00', '17:00', '17:05',
							'17:20', '19:00', '20:00' ],
					step : 5
				});
		$('#datetimepicker6').datetimepicker();
		$('#destroy').click(function() {
			if ($('#datetimepicker6').data('xdsoft_datetimepicker')) {
				$('#datetimepicker6').datetimepicker('destroy');
				this.value = 'create';
			} else {
				$('#datetimepicker6').datetimepicker();
				this.value = 'destroy';
			}
		});
		var logic = function(currentDateTime) {
			if (currentDateTime && currentDateTime.getDay() == 6) {
				this.setOptions({
					minTime : '11:00'
				});
			} else
				this.setOptions({
					minTime : '8:00'
				});
		};
		$('#datetimepicker7').datetimepicker({
			onChangeDateTime : logic,
			onShow : logic
		});
		$('#datetimepicker8').datetimepicker({
			onGenerate : function(ct) {
				$(this).find('.xdsoft_date').toggleClass('xdsoft_disabled');
			},
			minDate : '-1970/01/2',
			maxDate : '+1970/01/2',
			timepicker : false
		});
		$('#datetimepicker9').datetimepicker(
				{
					onGenerate : function(ct) {
						$(this).find('.xdsoft_date.xdsoft_weekend').addClass(
								'xdsoft_disabled');
					},
					weekends : [ '01.01.2014', '02.01.2014', '03.01.2014',
							'04.01.2014', '05.01.2014', '06.01.2014' ],
					timepicker : false
				});
		var dateToDisable = new Date();
		dateToDisable.setDate(dateToDisable.getDate() + 2);
		$('#datetimepicker11').datetimepicker(
				{
					beforeShowDay : function(date) {
						if (date.getMonth() == dateToDisable.getMonth()
								&& date.getDate() == dateToDisable.getDate()) {
							return [ false, "" ]
						}

						return [ true, "" ];
					}
				});
		$('#datetimepicker12').datetimepicker(
				{
					beforeShowDay : function(date) {
						if (date.getMonth() == dateToDisable.getMonth()
								&& date.getDate() == dateToDisable.getDate()) {
							return [ true, "custom-date-style" ];
						}

						return [ true, "" ];
					}
				});
		$('#datetimepicker_dark').datetimepicker({
			theme : 'dark'
		})
	</script>
</body>