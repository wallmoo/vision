<%@page import="bean.BBSBean"%>
<%@page import="java.util.List"%>
<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	// 전달된 객체 가져오기 
	List bbsList = (List) request.getAttribute("bbsList");
	BBSBean bbsBean = null;
	// 기본 자료형을 setAttribute() 로 전달했을 경우
	// getAttribute() 리턴형이 Object 이므로 기본자료형으로 직접 형변환 불가
	// 따라서 Wrapper 클래스 타입으로 형변환 후 기본자료형에 저장 필수!
	int maxPage = (Integer) request.getAttribute("maxPage");
	int startPage = (Integer) request.getAttribute("startPage");
	int endPage = (Integer) request.getAttribute("endPage");
	int listCount = (Integer) request.getAttribute("listCount");
	int nowPage = (Integer) request.getAttribute("page");
	int type = (Integer) request.getAttribute("type");
	String msg = (String) request.getAttribute("fail");
	if (msg != null) {
		PrintWriter script = response.getWriter();
		script.println("<script>");
		script.println("alert(" + msg + ")");
		script.println("</script>");
		script.close();
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Online Enquiries</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0 " />
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<link type="text/css" rel="stylesheet" href="css/main.css" />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript"
	src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="js/serch.js"></script>
<!-- js Library -->
</head>
<body>
	<script>
	function checkNotice(type,page,num) {
		var inputString = prompt("비밀글입니다. 비밀번호를 입력해주세요", "");
			location.href = "./detail.bs?type="+type+"&page="+page+"&num="+num+"&su=1&uw="+inputString;

		}
	</script>
	<div>
		<jsp:include page="header.jsp" />
		<!--container-->
		<div class='container Enquiries'>
			<div class='notice_con1'>
				<h2> <span>Online QnA</span> </h2>
				
				<form action="" method="post">
					<fieldset>
						<legend class="blind">검색</legend>
						<label for="search" class="blind">검색</label><input type="text"
							id="srch" onkeyup="serchSubmit('<%=type %>','<%=nowPage %>')" onkeydown="serchSubmit('<%=type %>','<%=nowPage %>')" />
						<p>
							<a href="javascript:serchSubmit('<%=type %>','<%=nowPage %>');">search</a>
						</p>
					</fieldset>
				</form>
				<table summary="공지사항 목록으로 번호, 제목, 작성자, 등록일, 조회로 구성">
					<caption class="blind">공지사항</caption>
					<thead>
						<tr>
							<th scope="col">번호</th>
							<th scope="col">제목</th>
							<th scope="col">작성자</th>
							<th scope="col">등록일</th>
							<th scope="col">조회</th>
						</tr>
					</thead>
					<tbody id="srchide">
						<%
							for (int i = 0; i < bbsList.size(); i++) {
								bbsBean = (BBSBean) bbsList.get(i);
								if (bbsBean.getBbs_deleteStatus() == 0) {
									if (bbsBean.getBbs_secretStatus() == 1) {
						%>
						<tr>
							<td><%=bbsBean.getBbs_num()%></td>
							<td class="2"><a
								href="javascript:checkNotice('<%=type%>','<%=nowPage%>','<%=bbsBean.getBbs_num()%>');"><%=bbsBean.getBbs_title()%><img src="img/lock.png"></img></a></td>
							<td><%=bbsBean.getBbs_writer()%></td>
							<td><%=bbsBean.getBbs_date().substring(0, 11)%></td>
							<td><%=bbsBean.getBbs_count()%></td>
						</tr>
						<%
							}
								}
								
							}
						%>
					</tbody>
				</table>
				<%
					if (nowPage == 1) {
				%>
				<div>
					<img src='img/arrow_left.png' alt='이전페이지 이동' />
					<%
						} else {
					%>
					<div>
						<img src='img/arrow_left.png' alt='이전페이지 이동'
							onclick="location.href='./list.bs?type=<%=type%>&page=<%=nowPage - 1%>'" />
						<%
							}
							for (int j = 1; j <= maxPage; j++) {
						%>
						<a href='./list.bs?type=<%=type%>&page=<%=j%>'><%=j%></a>
						<%
							}
							if (nowPage == maxPage) {
						%>
						<img src='img/arrow_right.png' alt='다음페이지 이동' />
						<p>
							<a href='noticeWrite.jsp'>글쓰기</a>
						</p>
					</div>
					<%
						} else {
					%>
					<img src='img/arrow_right.png' alt='다음페이지 이동'
						onclick="location.href='./list.bs?type=<%=type%>&page=<%=nowPage + 1%>'" />
					<p>
						<a href='noticeWrite.jsp'>글쓰기</a>
					</p>
				</div>
				<%
					}
				%>
			</div>
			</div>
		<jsp:include page="footer.jsp" />
</body>
</html>