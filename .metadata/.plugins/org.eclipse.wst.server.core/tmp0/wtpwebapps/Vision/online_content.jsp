<%@page import="bean.BBSBean"%>
<%@page import="bean.ComentBean"%>
<%@page import="java.util.List"%>
<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	// 전달된 객체 가져오기 
	BBSBean bbsList = (BBSBean) request.getAttribute("bbsList");
	List comentList = (List) request.getAttribute("comentlist");
	ComentBean comentbean = null;
	// 기본 자료형을 setAttribute() 로 전달했을 경우
	// getAttribute() 리턴형이 Object 이므로 기본자료형으로 직접 형변환 불가
	// 따라서 Wrapper 클래스 타입으로 형변환 후 기본자료형에 저장 필수!
	int maxPage = (Integer) request.getAttribute("maxPage");
	int startPage = (Integer) request.getAttribute("startPage");
	int endPage = (Integer) request.getAttribute("endPage");
	int listcount = (Integer) request.getAttribute("listcount");
	int nowPage = (Integer) request.getAttribute("page");
	int type = (Integer) request.getAttribute("type");
	String msg = (String) request.getAttribute("fail");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Notice</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0 " />
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<link type="text/css" rel="stylesheet" href="css/main.css" />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="js/content.js"></script>
<!-- js Library -->
</head>
<body onload="deleteCheck('<%=bbsList.getBbs_deleteStatus()%>','<%=type%>');">
	<div>
	<script>
			function deleteCheck(num, type) {
				if(num==1) {
					alert("삭제된 게시물입니다.");
					location.href="./list.bs?type="+type+"&page=1";
				}
			}
	</script>
		<jsp:include page="header.jsp" />
		<!--container-->
		<div class='container'>
		<div class='online_content'>
		<h2> <span>Online QnA</span> </h2>
			<div>
				<table>
					<caption class='blind'>목록</caption>
					<thead>
						<tr>
							<th colspan='1'><%=bbsList.getBbs_num()%></th>
							<th colspan='5'><%=bbsList.getBbs_title()%></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>작성자</td>
							<td><%=bbsList.getBbs_writer()%></td>
							<td>등록일</td>
							<td><%=bbsList.getBbs_date().substring(0,11)%></td>
							<td>조회수</td>
							<td><%=bbsList.getBbs_count()%></td>
						</tr>
						<tr>
							<td>첨부파일</td>
							<%if (bbsList.getBbs_filename() == null) {%>
							<td colspan='5'>첨부된 파일이 없습니다.</td>
							<%} else {%>
								<td colspan='5'><img src='img/file_download.png' alt='첨부파일 다운로드'/>
								<a href="./Download.bs?type=<%=bbsList.getBbs_type()%>&filename=<%=bbsList.getBbs_filename()%>&id=<%=bbsList.getBbs_id()%>&page=<%=nowPage%>"><%=bbsList.getBbs_filename()%></a></td>
							<%}%>
						</tr>
					</tbody>
				</table>
			</div>
				<div>
					<p><%=bbsList.getBbs_content()%></p>
				</div>
				<div id="sub_area2">
					<form action="#" method="post" class="">
						<p>덧글(<span><%=listcount %></span>)</p>
						<fieldset>
							<legend class="blind">검색하기</legend>
							<label for="srch" class="blind">검색어 입력</label>
							<textarea id="coment"></textarea>
							<input type="button" value="등록" onclick="comentSubmit('<%=bbsList.getBbs_writer()%>','null','<%=bbsList.getBbs_num()%>','<%=bbsList.getBbs_type()%>')" />
						</fieldset>
					</form>
				</div>
				<div id="sub_area">
				
				<%for (int k = 0; k < comentList.size(); k++) {
					comentbean = (ComentBean) comentList.get(k);
					if(comentbean.getDeleteCheck()!=1) {
				%>
					<div class='comment'>
						<dl>
							<dt>
								<span><%=comentbean.getUserid() %></span> | <span><%=comentbean.getRegdate().substring(0,16) %></span>
							</dt>
							<dd id="coment<%=k%>"><%=comentbean.getContent() %></dd>
						</dl>
						<ul>
						<%
							if(comentbean.getUserid().equals(bbsList.getBbs_writer())) {
						%>
							<li><a href="javascript:reWriteFunction('coment<%=k%>','<%=comentbean.getType() %>','<%=comentbean.getBoardid() %>','<%=comentbean.getId() %>','<%=k %>');" id="modeSave<%=k%>">수정</a></li>
							<li><a href="javascript:submitDelFunction('<%=comentbean.getType()%>','<%=comentbean.getBoardid() %>','<%=comentbean.getContent() %>','d','<%=comentbean.getId()%>');">삭제</a></li>
						<%		
							}
						%>
						</ul>
					</div>
				<%
					}
				}
				%>
				</div>
					<div>
						<p><a href="./modify.bs?num=<%=bbsList.getBbs_num()%>&page=<%=nowPage%>&type=<%=bbsList.getBbs_type()%>&title=<%=bbsList.getBbs_title()%>">수정</a></p>
						<p><a href="./change.bs?num=<%=bbsList.getBbs_num()%>&page=<%=nowPage%>&type=<%=bbsList.getBbs_type()%>&status=delete">삭제</a></p>
						<p><a href="./list.bs?type=<%=type%>&page=<%=nowPage%>">목록</a></p>
					</div>
			</div>
			</div>
			<jsp:include page="footer.jsp" />
</body>
</html>