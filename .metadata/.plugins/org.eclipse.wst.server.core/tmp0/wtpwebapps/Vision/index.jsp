<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VISION TECHNOLOGY</title>

		<jsp:include page="header.jsp" />
		<!--메인 베너-->
		<section class='index_section'>
		<h2 class='blind'>메인 베너</h2>
		<ul>
			<li>
				<!--<img src='img/main_img1.jpg' alt=''>-->
			</li>
			<!--<li><img src='img/main_img2.jpg' alt=''></li>-->
		</ul>
		</section>	
		<!--container-->
		<div class='container'>
			<div class='con1'>
				<ul class="pc">
					<li><a href='javascript:subMove(14);'><img src='img/product.jpg' alt='제품소개'/></a></li>
					<li><a href='javascript:subMove(52);'><img src='img/inquiry.jpg' alt='궁금한점 안내'/></a></li>
					<li><a href='javascript:subMove(51);'><img src='img/news,notice.jpg' alt='새소식'/></a></li>
					<li><a href='javascript:subMove(4);'><img src='img/information.jpg' alt='회사 정보'/></a></li>
				</ul>
				<ul class="mobile">
					<li><img src='img/mobile_img1.jpg' alt='메인 이미지'/></li>
					<li><a href='javascript:subMove(14);'><img src='img/mobile_img2.jpg' alt='제품소개'/></a></li>
					<li><a href='javascript:subMove(52);'><img src='img/mobile_img3.jpg' alt='궁금한점 안내'/></a></li>
					<li><a href='javascript:subMove(51);'><img src='img/mobile_img4.jpg' alt='새소식'/></a></li>
					<li><a href='javascript:subMove(4);'><img src='img/mobile_img5.jpg' alt='회사 정보'/></a></li>
				</ul>
			</div>
		</div>
			<jsp:include page="footer.jsp" />
</body>
</html>