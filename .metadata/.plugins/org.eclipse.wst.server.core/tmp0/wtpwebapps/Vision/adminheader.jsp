<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<div class="do_top">
			<div class="top_text">
				<span>VISIONTECH 관리자 시스템</span>|&nbsp;&nbsp;<%=session.getAttribute("admin")%>님,
				안녕하세요? 오늘도 좋은 하루 되세요.
			</div>
			<div class="time">
				<img src="adminD/images/top_time.png"><%=session.getAttribute("date")%>
			</div>
			<div class="logout" onclick="mvSession()">
				<img src="adminD/images/img_top_03.png">logout
			</div>
		</div>
		<script>
		function mvSession() {
			location.href = 'admin_logout.jsp';
		}
		</script>
		<div class="do_content">
			<div class="do_nav h1071">
				<ul class="sf-menu" id="example">
					<li><a href="adminList.ad?type=51">공지사항관리</a></li>
					<li><a href="adminList.ad?type=52">온라인문의</a></li>
					<li><a href="adminList.ad?type=54">관련자료</a></li>
					<li><a href="adminList.ad?type=53">주문의뢰서</a></li>
					<li><a href="adminList.md?type=21">카탈로그관리</a></li>
					<li><a href="adminList.md?type=22">동영상관리</a></li>
					<li><a href="management.jsp">관리자관리</a></li>
				</ul>
			</div>
</body>
</html>