/**
 * 
 */

function submitSerchFunction(type, nowPage) {
	var select = $("#select").val();
	var serch = $("#serch").val();
	var date1 = $("#datetimepicker_mask").val();
	var date2 = $("#datetimepicker_mask02").val();
	if(date1 == '____/__/__ __:__' || date2 == '____/__/__ __:__') {
		alert("날짜를 선택해주세요.");
	} else {
		if(type==54) {
			$.ajax({
				"url":"./orderSerch?type="+type+"&page="+nowPage+"&date1="+date1+"&date2="+date2+"&select="+select+"&serch="+serch,
				"method":"post"
			}).done(function(rst){
				var htmlText;
				for(var j=0; j<rst.length;j++) {
					var jsonInfo = JSON.parse(rst);
					if(jsonInfo.serchorder[0]==null) {
						htmlText = "<tr>" +
								"<td></td>" +
								"<td></td>" +
								"<td colspan='2'></td>" +
								"<td class='2' colspan='2'>검색결과가 없습니다.</td>" +
								"<td></td>" +
								"<td></td>" +
								"<td></td>" +
								"</tr>";
						$("#adminSerch").html(htmlText);
					} else {
						var status;
						switch(jsonInfo.serchorder[j].orderstatus) {
						case 0 :
							status = "신청";
							break;
						case 1 :
							status = "대기중";
							break;
						case 2 :
							status = "처리중";
							break;
						case 3 : 
							status = "의뢰처리";
							break;
						}
						htmlText = "<tr><td>"+jsonInfo.serchorder[j].orderid+"</td>" +
								"<td>"+jsonInfo.serchorder[j].username+"</td>" +
										"<td colspan='2'>"+jsonInfo.serchorder[j].usercompanyname+"</td>" +
												"<td class='2' colspan='2'>"+jsonInfo.serchorder[j].ordertitle+"</td>" +
														"<td>"+jsonInfo.serchorder[j].orderdate+"</td>" +
																"<td>"+status+"</td>" +
																		"<td><button>" +
																		"<a href=\"./detail.ad?num="+jsonInfo.serchorder[j].orderid+"&page="+nowPage+"&type=54\">" +
																				"<img src=\"adminD/images/btn_table.png\">" +
																					"</button></td></tr>";
						$("#adminSerch").html(htmlText);
					}
				}
			})
		} else {
			$.ajax({
				"url":"./adminSerch?type="+type+"&page="+nowPage+"&date1="+date1+"&date2="+date2+"&select="+select+"&serch="+serch,
				"method":"post"
			}).done(function(rst) {
				for(var i =0; i<rst.length;i++) {
					var htmlText;
					var jsonInfo = JSON.parse(rst);
					if(jsonInfo.serchnotice==null) {
						htmlText = "<tr>" +
						"<td></td>" +
						"<td></td>" +
						"<td colspan='2'></td>" +
						"<td class='2' colspan='2'>검색결과가 없습니다.</td>" +
						"<td></td>" +
						"<td></td>" +
						"<td></td>" +
						"</tr>";
						$("#adminSerch").html(htmlText);
					} else {
						var str;
						var str2;
						var str3;
						
						var strlength = jsonInfo.serchnotice[i].title.length;
						var strlength2 = jsonInfo.serchnotice[i].content.length;
						var strmatch = jsonInfo.serchnotice[i].content.indexOf('img');
						if(strlength > 10) {
							str = jsonInfo.serchnotice[i].title.substring(0,10);
						}else {
							str = jsonInfo.serchnotice[i].title;
						}
						if(strmatch!=-1) {
							str2 = "....";
						} else {
							if(strlength2 > 10) {
								str2 = jsonInfo.serchnotice[i].content.substring(0, 10); 
							}else {
								str2 = jsonInfo.serchnotice[i].content;
							}
						}
						if(jsonInfo.serchnotice[i].deleteStatus==1) {
							str3 = "삭제";
						}else {
							str3 = "-";
						}
						htmlText = "<tr><td>"+jsonInfo.serchnotice[i].num+"</td>" +
								"<td>"+jsonInfo.serchnotice[i].writer+"</td>" +
										"<td class='2' colspan='2' >"+str+"</td>" +
											"<td colspan='2'>"+str2+"</td>" +
													"<td>"+jsonInfo.serchnotice[i].date+"</td>" +
															"<td>"+jsonInfo.serchnotice[i].count+"</td>" +
																	"<td>"+str3+"</td>" +
																			"<td><button>" +
																			"<a href=\"./detail.ad?num="+jsonInfo.serchnotice[i].num+"&page="+nowPage+"&type="+jsonInfo.serchnotice[i].type+"\">" +
																					"<img src=\"adminD/images/btn_table.png\"></a></button></td></tr>";
																	
						$("#adminSerch").html(htmlText);
					}
				}
			})
			
		}
	}
}

