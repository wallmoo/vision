<%@page import="bean.BBSBean"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	// 전달된 객체 가져오기 
	List bbsList = (List) request.getAttribute("bbsList");
	BBSBean bbsBean = null;
	// 기본 자료형을 setAttribute() 로 전달했을 경우
	// getAttribute() 리턴형이 Object 이므로 기본자료형으로 직접 형변환 불가
	// 따라서 Wrapper 클래스 타입으로 형변환 후 기본자료형에 저장 필수!
	int maxPage = (Integer) request.getAttribute("maxPage");
	int startPage = (Integer) request.getAttribute("startPage");
	int endPage = (Integer) request.getAttribute("endPage");
	int listCount = (Integer) request.getAttribute("listCount");
	int nowPage = (Integer) request.getAttribute("page");
	int type = (Integer) request.getAttribute("type");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<title>VISIONTECH</title>
<link rel="stylesheet" href="adminD/css/do_layout.css">
<!-- radio,check -->
<link href="adminD/css/_all.css" rel="stylesheet">
<script src="adminD/js/jquery.js"></script>
<script src="adminD/js/icheck.js"></script>
<!--달력-->
<link rel="stylesheet" type="text/css"
	href="adminD/css/jquery.datetimepicker.css" />
<!-- 메뉴 -->
<link rel="stylesheet" href="adminD/dist/css/superfish.css"
	media="screen">
<!-- <script src="dist/js/jquery.js"></script> -->
<script src="adminD/dist/js/superclick.js"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="adminD/js/adminserch.js"></script>
<script>

		(function($){ //create closure so we can safely use $ as alias for jQuery

			$(document).ready(function(){

				// initialise plugin
				var example = $('#example').superclick({
					//add options here if required
				});

				// buttons to demonstrate Superclick's public methods
				$('.destroy').on('click', function(){
					example.superclick('destroy');
					return false;
				});

				$('.init').on('click', function(){
					example.superclick();
					return false;
				});

				$('.open').on('click', function(){
					example.children('li:first').superclick('show');
					return false;
				});

				$('.close').on('click', function(){
					example.children('li:first').superclick('hide');
					return false;
				});

				$('.closeall').on('click', function(){
					example.superclick('reset');
					return false;
				});
			});

		})(jQuery);


	</script>

</head>
<body>
	<div class="do_wrap">
		<jsp:include page="adminheader.jsp"></jsp:include>
			<div class="right_box">
				<div class="manage_box h1000">
					<h2>온라인문의 관리</h2>
					<div class="search">
						<input class="w120" id="datetimepicker_mask"> <img src="adminD/images/bar.png">
						<input class="w120" id="datetimepicker_mask02"> 
						<img src="adminD/images/divide.png" class="m20">
						<select class="s110" id="select">
							<option value="all">전체</option>
							<option value="title">제목</option>
							<option value="content">내용</option>
						</select> 
						<input class="w398" id="serch">
						<button class="b_consult" onclick="submitSerchFunction('<%=type%>','<%=nowPage%>')">검색</button>
						
					</div>
					<table>
						<thead>
							<tr>
								<th>번호</th>
								<th>작성자</th>
								<th colspan="2">제목</th>
								<th class="w210" colspan="2">내용</th>
								<th>작성일</th>
								<th>조회수</th>
								<th>상태</th>
								<th>상세보기</th>
							</tr>
						</thead>
						<tbody id="adminSerch">
						<%
							for (int i = 0; i < bbsList.size(); i++) {
								bbsBean = (BBSBean) bbsList.get(i);
								int contentsize = bbsBean.getBbs_content().length();
							%>
							<tr>
								<td><%=bbsBean.getBbs_num()%></td>
								<td><%=bbsBean.getBbs_writer()%></td>
								<%
									if(bbsBean.getBbs_title().length()>10) {
								%>
									<td class="2" colspan="2"><%=bbsBean.getBbs_title().substring(0, 10)%>...</td>
								<%		
									} else {
								%>
									<td class="2" colspan="2"><%=bbsBean.getBbs_title()%></td>
								<%		
									}
									if(contentsize > 10) {
										if(bbsBean.getBbs_content().matches(".*img.*")) {
								%>
											<td colspan="2">....</td>
								<%
										} else {
								%>		
										<td colspan="2"><%=bbsBean.getBbs_content().substring(0, 10) %></td>
								<%
										}
									}else {
								%>
									<td colspan="2"><%=bbsBean.getBbs_content() %>...</td>
								<%		
									}
								%>
								<td><%=bbsBean.getBbs_date()%></td>
								<td><%=bbsBean.getBbs_count()%></td>
								<%
							if(bbsBean.getBbs_deleteStatus()==1) {
						%>
								<td>삭제</td>
								<%
							} else {
						%>
								<td>-</td>
								<%		
							}
						 %>
								<td><button>
									<a
											href="./detail.ad?num=<%=bbsBean.getBbs_num()%>&page=<%=nowPage%>&type=<%=bbsBean.getBbs_type()%>">
											<img src="adminD/images/btn_table.png"></a>
									</button></td>
							</tr>
							<%
							}
						%>
						</tbody>
					</table>
					<div class="number">
							<%
							if (nowPage == 1) {
						%>
		
							<span class="num_arrow"><img src="adminD/images/arrow_pre.png"></span>
							<%
							} else {
						%>
		
							<span class="num_arrow"><img src="adminD/images/arrow_pre.png" onclick="location.href='./adminList.ad?type=<%=type%>&page=<%=nowPage - 1%>'"></span>
							<%
							}
							for (int j = 1; j <= maxPage; j++) {
								if(j==nowPage) {
						%>
								<span class="num num_active"><%=j %></span>
						<%			
								} else {
						%>
								<span class="num"><a href='./adminList.ad?type=<%=type%>&page=<%=j%>'><%=j%></a></span>
						<%			
								}
						%>
					
						<%
							}
							if (nowPage == maxPage) {
						%>
							<span class="num_arrow"><img src="adminD/images/arrow_next.png"></span>
		
							<%
							} else {
						%>
							<span class="num_arrow"><img src="adminD/images/arrow_next.png" onclick="location.href='./adminList.ad?type=<%=type%>&page=<%=nowPage + 1%>'"></span>

							<%
							}
						%>
					</div>
				</div>
				<jsp:include page="adminfooter.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script>
			$(document).ready(function(){
              $('.login input').iCheck({
                checkboxClass: 'icheckbox_square-top',
                // radioClass: 'iradio_square-green',
                increaseArea: '20%'
              });
            });          
    </script>
	<!-- 달력 -->
	<!-- <script src="js/jqueryC.js"></script> -->
	<script src="adminD/build/jquery.datetimepicker.full.js"></script>
	<script>/*
window.onerror = function(errorMsg) {
	$('#console').html($('#console').html()+'<br>'+errorMsg)
}*/

$.datetimepicker.setLocale('en');

$('#datetimepicker_format').datetimepicker({value:'2015/04/15 05:03', format: $("#datetimepicker_format_value").val()});
$("#datetimepicker_format_change").on("click", function(e){
	$("#datetimepicker_format").data('xdsoft_datetimepicker').setOptions({format: $("#datetimepicker_format_value").val()});
});
$("#datetimepicker_format_locale").on("change", function(e){
	$.datetimepicker.setLocale($(e.currentTarget).val());
});

$('#datetimepicker').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:	'1986/01/05'
});
$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});

$('.some_class').datetimepicker();

$('#default_datetimepicker').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:false
});

$('#datetimepicker10').datetimepicker({
	step:5,
	inline:true
});
// 여기
$('#datetimepicker_mask').datetimepicker({
	mask:'9999/19/39 29:59'
});
$('#datetimepicker_mask02').datetimepicker({
	mask:'9999/19/39 29:59'
});

$('#datetimepicker1').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:5
});
$('#datetimepicker2').datetimepicker({
	yearOffset:222,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	minDate:'-1970/01/02', // yesterday is minimum date
	maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#datetimepicker3').datetimepicker({
	inline:true
});
$('#datetimepicker4').datetimepicker();
$('#open').click(function(){
	$('#datetimepicker4').datetimepicker('show');
});
$('#close').click(function(){
	$('#datetimepicker4').datetimepicker('hide');
});
$('#reset').click(function(){
	$('#datetimepicker4').datetimepicker('reset');
});
$('#datetimepicker5').datetimepicker({
	datepicker:false,
	allowTimes:['12:00','13:00','15:00','17:00','17:05','17:20','19:00','20:00'],
	step:5
});
$('#datetimepicker6').datetimepicker();
$('#destroy').click(function(){
	if( $('#datetimepicker6').data('xdsoft_datetimepicker') ){
		$('#datetimepicker6').datetimepicker('destroy');
		this.value = 'create';
	}else{
		$('#datetimepicker6').datetimepicker();
		this.value = 'destroy';
	}
});
var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('#datetimepicker7').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});
$('#datetimepicker8').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date')
			.toggleClass('xdsoft_disabled');
	},
	minDate:'-1970/01/2',
	maxDate:'+1970/01/2',
	timepicker:false
});
$('#datetimepicker9').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date.xdsoft_weekend')
			.addClass('xdsoft_disabled');
	},
	weekends:['01.01.2014','02.01.2014','03.01.2014','04.01.2014','05.01.2014','06.01.2014'],
	timepicker:false
});
var dateToDisable = new Date();
	dateToDisable.setDate(dateToDisable.getDate() + 2);
$('#datetimepicker11').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [false, ""]
		}

		return [true, ""];
	}
});
$('#datetimepicker12').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [true, "custom-date-style"];
		}

		return [true, ""];
	}
});
$('#datetimepicker_dark').datetimepicker({theme:'dark'})


</script>
</body>