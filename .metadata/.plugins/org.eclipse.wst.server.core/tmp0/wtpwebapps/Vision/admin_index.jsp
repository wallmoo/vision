
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.io.PrintWriter"%>
<%
	request.setCharacterEncoding("UTF-8");
	String msg = (String)request.getAttribute("loginMsg");
	if(msg!=null) {
		PrintWriter script = response.getWriter();
		script.println("<script>");
		script.println("alert('"+msg+"')");
		script.println("</script>");
	}
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<!-- 페이지 개요,설명:알맞게 작성하세요 -->
<title>비전테크</title>
    <link rel="stylesheet" href="css/do_layout.css">
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<!-- 팝업 -->
<link rel="stylesheet" href="adminD/css/jquery.popup.css" type="text/css">
<!-- <script type="text/javascript" src="js/jquery-2.0.3.min.js"></script> -->
<script type="text/javascript" src="adminD/js/jquery.popup.js"></script>
<!--  
	   <script type="text/javascript">
	   var popups;
	    	$(function() {
	      		popups = $(".js__p_01_start").simplePopup();
	    	});
	   </script>
	    -->
<script>

function submitFuntionLogin() {
	$("#fr").submit();
}
</script>

</head>
<body>
	<!-- DIM처리 -->
	<div class="p_body js__p_body js__fadeout"></div>
	<div class="do_wrap">
		<div class="login_wrap">
			<img src="img/login_logo.jpg">
			<div class="login_box">
			<form action="./adminLogin.ad" id="fr" method="post">
				<p>비전테크 관리자 페이지 입니다</p>
				<div class="input_wrap">
					<div class="input_area">
						<input type="text" class="login_input" placeholder='User ID' name="id">
						<input type="password" class="login_input"placeholder='Password' name="pass">
					</div>
					<div class="btn_wrap">
						<button class="login_btn"><a href="javascript:void(0);" onclick="submitFuntionLogin()" class="lal js__p_01_start">LOGIN</a></button>
					</div>
				</div>
			</form>
			</div>
			<p>
				<span>(주)디오스토리</span> 서울시 서초구 강남대로 221 동성빌딩 409호 | 02-333-0520<br/>Copyright⒞2009 dostory Inc.All Rights Reserved.
			</p>
		</div>
		
	</div>
	<!-- 
	<div class="popup w500 js__01_popup js__slide_top">
	    <a href="#" class="p_close js__p_close" title="">
	      <span></span><span></span>
	    </a>
	    <div class="p_content">
	    		<div class="top">
	    		<h1>SMS 본인확인</h1>
	    		<p>본 시스템에 등록된 휴대전화로 인증번호를 발송하였습니다.<br> 
				아래에 수신된 인증번호를 입력하여 주시기 바랍니다.
				</p>
				</div>
				<ul>
				<li>인증번호 
					<input type="text" name="" class="login_input_popup">
					<button><a href="" class="b_o_02">확인</a></button>
					<button><a href="" class="b_g_02">취소</a></button>
				</li>
				<li class="red">입력 대기시간을 초과하였습니다.<br>
				id, pass 입력부터 다시 진행하여 주시기 바랍니다.</li>
	    		</a>
	    		</ul>
		</div>
	</div>
	 -->
</body>
</html>