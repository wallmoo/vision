<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>VISIONTECH</title>
    <link rel="stylesheet" href="adminD/css/do_layout.css">
     <!-- radio,check -->
    <link href="adminD/css/_all.css" rel="stylesheet">
  	<script src="adminD/js/jquery.js"></script>
  	<script src="adminD/js/icheck.js"></script>

  	<!-- 메뉴 -->
    <link rel="stylesheet" href="adminD/dist/css/superfish.css" media="screen">
    <!-- <script src="dist/js/jquery.js"></script> -->
	<script src="adminD/dist/js/superclick.js"></script>
	<script>

		(function($){ //create closure so we can safely use $ as alias for jQuery

			$(document).ready(function(){

				// initialise plugin
				var example = $('#example').superclick({
					//add options here if required
				});

				// buttons to demonstrate Superclick's public methods
				$('.destroy').on('click', function(){
					example.superclick('destroy');
					return false;
				});

				$('.init').on('click', function(){
					example.superclick();
					return false;
				});

				$('.open').on('click', function(){
					example.children('li:first').superclick('show');
					return false;
				});

				$('.close').on('click', function(){
					example.children('li:first').superclick('hide');
					return false;
				});

				$('.closeall').on('click', function(){
					example.superclick('reset');
					return false;
				});
			});

		})(jQuery);


	</script>
</head>
<body>
	<div class="do_wrap">
	<jsp:include page="adminheader.jsp"></jsp:include>
			<div class="right_box">
				<div class="manage_box">
				 <h2>관리자관리</h2>
				   <div class="manage_box_inner">
				   <div class="skin-square">
				   	 <div class="col_1">
				   	 	<div class="left">현재 비빌번호</div>
				   	 	<div class="right"><input class="w308" id="nowPw"></div>
				   	 	
				   	 </div>
				   	 <div class="col_1">
				   	 	<div class="left">새 비빌번호</div>
				   	 	<div class="right" ><input class="w308" id="pw2"></div>
				   	 	
				   	 </div>
				   	 <div class="col_1">
				   	 	<div class="left">비빌번호 재확인</div>
				   	 	<div class="right"><input class="w308" id="pw1"></div>
				   	 	
				   	 </div>
				   </div>
				   </div>				   
				   <div class="f_right">
				   	<button><a href="javascript:checkChange()" class="b_o">확인</a></button>
				   	<button><a href="location.href='history.back()';" class="b_g">취소</a></button>
				   </div>	
				</div>
				<jsp:include page="adminfooter.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script>
		
		
		function checkChange() {
			var real = "";
			var pass = $("#nowPw").val();
			var pw1 = $("#pw1").val();
			var pw2 = $("#pw2").val();
			
			if(pw1==pw2) {
				
				real = pw1;
				
				$.ajax({
					"url":"./adminCheckPass?pass="+pass,
					"method":"post"
				}).done(function(rst) {
					if(rst=="true") {
						location.href = "/change.ad?mode=changePw&pass="+real;
					}else {
						alert("패스워드가 일치하지 않습니다.");
					}
				})	
			}else {
				alert("비밀번호가 일치하지 않습니다.");
			}
	
		}
	</script>
	<script>
			$(document).ready(function(){
              $('.login input').iCheck({
                checkboxClass: 'icheckbox_square-top',
                // radioClass: 'iradio_square-green',
                increaseArea: '20%'
              });
            });
            $(document).ready(function(){
              $('.skin-square input').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
                increaseArea: '20%'
              });
            });           
    </script>
</body>