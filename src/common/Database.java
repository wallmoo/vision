package common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class Database {
	
	private Connection conn;
	private PreparedStatement pstmt;
	private Statement stmt;
	private ResultSet rs;
	
	public Database() {
		try {
			Context initialContext = new InitialContext();
			Context envContext = (Context)initialContext.lookup("java:/comp/env");
			DataSource dataSource = (DataSource)envContext.lookup("jdbc/sqlDB");
			conn = dataSource.getConnection();
		}catch (NamingException e) {
			e.printStackTrace();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public PreparedStatement preparedQry(String query) {
		try {
			pstmt = conn.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pstmt;
	}
	
	public boolean executePrepared() {
		boolean bool = false;
		try {
			bool = pstmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return bool;
	}
	
	public ResultSet executePreaparedQry() {
		try {
			rs = pstmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rs;
	}
	
	public int executePreaparedUpdateQry() {
		int row = 0;
		try {
			row = pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return row;
	}
	
	public ResultSet executeQry(String query) {
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rs;
	}
	
	public int executeUpdateQry(String query) {
		int row = 0;
		
		try {
			stmt = conn.createStatement();
			row = stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return row;
	}
	
	public boolean excute(String query) {
		boolean bool = false;
		try {
			stmt = conn.createStatement();
			bool = stmt.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bool;
	}
	
	public int getGeneratKey() {
		int key = 0;
		try (ResultSet generatedKeys = pstmt.getGeneratedKeys()){
			if(generatedKeys.next()) {
				key = generatedKeys.getInt(1);
			}else {
				throw new SQLException("Creating user failed, no ID obtained");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return key;
	}
	
	public int[] getGenrateKeys() {
		int[] keys = null;
		try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
			generatedKeys.last();
			int rowCnt = generatedKeys.getRow();
			keys = new int[rowCnt];
			generatedKeys.beforeFirst();
			
			if(rowCnt==0) {
				throw new SQLException("Creating user failed, no ID obtained.");
			}
			
			int i =0;
			while(generatedKeys.next()) {
				keys[i] = generatedKeys.getInt(1);
				i++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return keys;
	}
	
	public PreparedStatement getPstmt() {
		return pstmt;
	}
	
	public void close() {
		try {
			if (rs != null) {
				rs.close();
			}

			if (stmt != null) {
				stmt.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

			if (conn != null) {
				conn.close();
			}
		} catch (SQLException sqlE) {
			sqlE.printStackTrace();
		}
	}
	
	public Connection getConnection() {
		return conn;
	}
}
