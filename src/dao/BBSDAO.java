package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bean.BBSBean;
import bean.BBSFileBean;
import bean.OrderBean;
import common.Database;

public class BBSDAO {
	private Database database;

	public int getNext(int type) {
		database = new Database();
		int num = 0;
		String sql = "";
		sql = "SELECT MAX(bbs_num) FROM bbs where bbs_type=?";
		try {
			PreparedStatement	pstmt = database.preparedQry(sql);
			pstmt.setInt(1, type);
			ResultSet rs = database.executePreaparedQry();
			if (rs.next()) {
				num = rs.getInt(1) + 1;
			} else {
				num = 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return num;
	}

	public boolean bbsInsert(BBSBean bbsBean, List<BBSFileBean> list) {
		System.out.println("Insert !");
		database = new Database();
		int result = 0; 
		int type = bbsBean.getBbs_type();
		int num = getNext(type);
		String date = getDate();
		String sql = "";
		try {
			PreparedStatement	pstmt = database.preparedQry(sql);
			sql = "INSERT INTO bbs(bbs_num ,bbs_type,bbs_title ,bbs_writer ,bbs_content"
					+ ", bbs_filename, bbs_comentStatus,bbs_secretStatus ,bbs_deleteStatus, bbs_password, bbs_count, bbs_date) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
			pstmt = database.preparedQry(sql);

			pstmt.setInt(1, num);
			pstmt.setInt(2, type);
			pstmt.setString(3, bbsBean.getBbs_title());
			pstmt.setString(4, bbsBean.getBbs_writer());
			pstmt.setString(5, bbsBean.getBbs_content());
			pstmt.setString(6, bbsBean.getBbs_filename());
			pstmt.setInt(7, 0); 
			pstmt.setInt(8, bbsBean.getBbs_secretStatus());
			pstmt.setInt(9, 0); 
			pstmt.setString(10, bbsBean.getBbs_password());
			pstmt.setInt(11, 0); 
			pstmt.setString(12, date);
			result = database.executePreaparedUpdateQry();

			if (list.size() > 0) {
				String query = "INSERT INTO bbs_file (type,bbsid, filepath, filename) VALUES ";

				for (BBSFileBean bean : list) {
					query += "(" + bean.getType() + "," + num + ",'" + bean.getFilepath() + "', '"
							+ bean.getFilename() + "')";
					if (!list.get(list.size() - 1).equals(bean)) {
						query += ", ";
					}
				}
				pstmt = database.preparedQry(query);
				result = database.executePreaparedUpdateQry();
			}

			if (result == 0) {
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return false;
	} // boardInsert()
	
	public String getDate() {
		database = new Database();
		String sql = "SELECT NOW()";
		String result ="";
		try {
			PreparedStatement	pstmt = database.preparedQry(sql);
			ResultSet rs = database.executePreaparedQry();
			if(rs.next()) {
				result =  rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int getListCount(int type) {
		database = new Database();
		int count = 0;

		try {
			PreparedStatement pstmt = database.preparedQry("SELECT COUNT(*) bbs_count FROM bbs where bbs_type=?;");
			pstmt.setInt(1, type);
			ResultSet rs = database.executePreaparedQry();

			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return count;
	} // getListCount()

	public int getSearchListCount(int type, String search) {
		database = new Database();

		int count = 0;

		try {
			String sql = "SELECT COUNT(*) bbs_count FROM bbs where bbs_type=? and bbs_title like '%" + search + "%'";
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, type);
			ResultSet rs = database.executePreaparedQry();

			if (rs.next()) {
				count = rs.getInt(1);
			}

			return count;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return 0;
	} // getListCount()

	public int getAllCount() {
		database = new Database();

		int count = 0;

		try {
			PreparedStatement pstmt = database.preparedQry("select id from bbs order by regdate desc limit 1;");
			ResultSet rs = database.executePreaparedQry();

			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return count;
	} // getListCount()

	public String getSecretPassword(int num, int type) {
		database = new Database();

		String result = "";

		try {
			PreparedStatement pstmt = database
					.preparedQry("select bbs_password from bbs where bbs_num = ? and bbs_type = ? limit 1;");
			pstmt.setInt(1, num);
			pstmt.setInt(2, type);
			ResultSet rs = database.executePreaparedQry();

			if (rs.next()) {
				result = rs.getString(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return result;
	} // getListCount()

	public List getBBSList(int page, int limit, int type) {
		database = new Database();
		int startRow = (page - 1) * limit + 1;
		List bbsList = new ArrayList<>();
		String sql = "SELECT * FROM bbs where bbs_type=? ORDER BY bbs_num DESC LIMIT ?,?;";

		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, type);
			pstmt.setInt(2, startRow - 1);
			pstmt.setInt(3, limit);
			ResultSet rs = database.executePreaparedQry();

			while (rs.next()) {
				BBSBean bbsBean = new BBSBean();
				bbsBean.setBbs_writer(rs.getString("bbs_writer"));
				bbsBean.setBbs_type(rs.getInt("bbs_type"));
				bbsBean.setBbs_num(rs.getInt("bbs_num"));
				bbsBean.setBbs_id(rs.getInt("bbs_id"));
				bbsBean.setBbs_title(rs.getString("bbs_title"));
				bbsBean.setBbs_content(rs.getString("bbs_content"));
				bbsBean.setBbs_count(rs.getInt("bbs_count"));
				bbsBean.setBbs_date(rs.getString("bbs_date"));
				bbsBean.setBbs_deleteStatus(rs.getInt("bbs_deleteStatus"));
				bbsBean.setBbs_commentStatus(rs.getInt("bbs_comentStatus"));
				bbsBean.setBbs_secretStatus(rs.getInt("bbs_secretStatus"));
				bbsBean.setBbs_filename(rs.getString("bbs_filename"));
				bbsBean.setBbs_password(rs.getString("bbs_password"));
				bbsList.add(bbsBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return bbsList;
	} // getBoardList

	public BBSBean getBBSDetail(int num, int type) {
		database = new Database();
		ComentDAO comentdao = new ComentDAO();
		BBSBean bbsBean = null;
		List bbsList = new ArrayList<>();
		String sql = "select * from bbs where bbs_num = ? and bbs_type = ?";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, num);
			pstmt.setInt(2, type);

			ResultSet rs = database.executePreaparedQry();

			if (rs.next()) {
				bbsBean = new BBSBean();
				bbsBean.setBbs_writer(rs.getString("bbs_writer"));
				bbsBean.setBbs_type(rs.getInt("bbs_type"));
				bbsBean.setBbs_num(rs.getInt("bbs_num"));
				bbsBean.setBbs_id(rs.getInt("bbs_id"));
				bbsBean.setBbs_title(rs.getString("bbs_title"));
				bbsBean.setBbs_content(rs.getString("bbs_content"));
				bbsBean.setBbs_count(rs.getInt("bbs_count"));
				bbsBean.setBbs_date(rs.getString("bbs_date"));
				bbsBean.setBbs_deleteStatus(rs.getInt("bbs_deleteStatus"));
				bbsBean.setBbs_commentStatus(rs.getInt("bbs_comentStatus"));
				bbsBean.setBbs_secretStatus(rs.getInt("bbs_secretStatus"));
				bbsBean.setBbs_filename(rs.getString("bbs_filename"));
				bbsBean.setBbs_password(rs.getString("bbs_password"));
				
				bbsList.add(bbsBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return bbsBean;
	}

	public void updateBBScount(int num, int type) {
		database = new Database();
		String sql = "";
		
		sql = "update bbs set bbs_count=bbs_count+1 where bbs_type=? and bbs_num=?";
		
		try {

			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, type);
			pstmt.setInt(2, num);
			int result = database.executePreaparedUpdateQry();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
	}

	public int reWriteBBS(int type, int num, String title, String content, String writer, String password) {
		database = new Database();
		System.out.println("rewrite!:--------------");
		System.out.println(type);
		System.out.println(num);
		System.out.println(title);
		System.out.println(content);
		System.out.println("rewrite!:--------------");
		
		String sql = "update bbs set bbs_title = ? , bbs_content = ? , bbs_writer = ?, bbs_password = ?where bbs_type = ? and bbs_num = ?";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setString(1, title);
			pstmt.setString(2, content);
			pstmt.setString(3, writer);
			pstmt.setString(4, password);
			pstmt.setInt(5, type);
			pstmt.setInt(6, num);

			int result = database.executePreaparedUpdateQry();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return -1;
	}

	public int statusBBS(int num, int type, String status, String content) {
		database = new Database();
		PreparedStatement pstmt;
		int result = 0;
		String sql = "";
		
		try {
			switch (status) {
			case "delete":
				sql = "update bbs set bbs_deleteStatus=? where bbs_type = ? and bbs_num = ?";
				pstmt = database.preparedQry(sql);
				pstmt.setInt(1, 1);
				pstmt.setInt(2, type);
				pstmt.setInt(3, num);
				break;
			case "comentwrite":
				sql = "update comment set content = ? where type = ? and id = ?";
				pstmt = database.preparedQry(sql);
				pstmt.setString(1, content);
				pstmt.setInt(2, type);
				pstmt.setInt(3, num);
				break;
			case "coment":
				sql = "update bbs set bbs_comentStatus=?+1 where bbs_type = ? and bbs_num =?";
				pstmt = database.preparedQry(sql);
				pstmt.setInt(1, 1);
				pstmt.setInt(2, type);
				pstmt.setInt(3, num);
				break;
			case "comentdelete" :
				sql = "update comment A join bbs B on A.type = B.bbs_type set A.deleteCheck = 1 ,"
						+ " B.bbs_comentStatus = bbs_comentStatus-1 where A.boardid = ? and B.bbs_num = ? and A.type= ? and B.bbs_type = ? and A.content = ?";
				pstmt = database.preparedQry(sql);
				pstmt.setInt(1, num);
				pstmt.setInt(2, num);
				pstmt.setInt(3, type);
				pstmt.setInt(4, type);
				pstmt.setString(5, content);
				break;
			case "mediadelete" :
				sql = "update media set media_status = 1 where media_type = ? and media_id =?";
				pstmt = database.preparedQry(sql);
				pstmt.setInt(1, type);
				pstmt.setInt(2, num);
				break;
			}
			result = database.executePreaparedUpdateQry();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return result;
	}

	public List getBBSSearchList(String search, int type, int page, int limit) {
		database = new Database();
		int startRow = (page - 1) * limit + 1; 
		List bbsList = new ArrayList<>();
		String sql = "select * from bbs where bbs_title like '%" + search
				+ "%' and bbs_type=? ORDER BY bbs_num DESC LIMIT ?,?";

		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, type);
			pstmt.setInt(2, startRow - 1);
			pstmt.setInt(3, limit);
			ResultSet rs = database.executePreaparedQry();

			while (rs.next()) {
				BBSBean bbsBean = new BBSBean();
			
				bbsBean.setBbs_writer(rs.getString("bbs_writer"));
				bbsBean.setBbs_type(rs.getInt("bbs_type"));
				bbsBean.setBbs_num(rs.getInt("bbs_num"));
				bbsBean.setBbs_id(rs.getInt("bbs_id"));
				bbsBean.setBbs_title(rs.getString("bbs_title"));
				bbsBean.setBbs_content(rs.getString("bbs_content"));
				bbsBean.setBbs_count(rs.getInt("bbs_count"));
				bbsBean.setBbs_date(rs.getString("bbs_date"));
				bbsBean.setBbs_deleteStatus(rs.getInt("bbs_deleteStatus"));
				bbsBean.setBbs_commentStatus(rs.getInt("bbs_comentStatus"));
				bbsBean.setBbs_secretStatus(rs.getInt("bbs_secretStatus"));
				bbsBean.setBbs_filename(rs.getString("bbs_filename"));
				bbsBean.setBbs_password(rs.getString("bbs_password"));
				
				bbsList.add(bbsBean);
			}
			return bbsList;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return bbsList;
	}
	
	public List getAdminSearchList(String search, int type, int page, int limit, String date1, String date2, String select) {
		database = new Database();
		int startRow = (page - 1) * limit + 1; 
		List bbsList = new ArrayList<>();
		String sql = "";
		switch(select) {
		case "all" :
			sql = "select * from bbs where bbs_date >= ? and bbs_date < ? and (bbs_title like '%" + search + "%' or bbs_content like '%" + search + "%') and bbs_type=? ORDER BY bbs_num DESC LIMIT ?,?";
			break;
		case "title" :
			sql = "select * from bbs where bbs_date >= ? and bbs_date < ? and bbs_title like '%" + search + "%' and bbs_type=? ORDER BY bbs_num DESC LIMIT ?,?";
			break;
		case "content" :
			sql = "select * from bbs where bbs_date >= ? and bbs_date < ? and bbs_content like '%" + search + "%' and bbs_type=? ORDER BY bbs_num DESC LIMIT ?,?";
			break;
		}
		
		
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setString(1, date1);
			pstmt.setString(2, date2);
			pstmt.setInt(3, type);
			pstmt.setInt(4, startRow - 1);
			pstmt.setInt(5, limit);
			ResultSet rs = database.executePreaparedQry();

			while (rs.next()) {
				BBSBean bbsBean = new BBSBean();
			
				bbsBean.setBbs_writer(rs.getString("bbs_writer"));
				bbsBean.setBbs_type(rs.getInt("bbs_type"));
				bbsBean.setBbs_num(rs.getInt("bbs_num"));
				bbsBean.setBbs_id(rs.getInt("bbs_id"));
				bbsBean.setBbs_title(rs.getString("bbs_title"));
				bbsBean.setBbs_content(rs.getString("bbs_content"));
				bbsBean.setBbs_count(rs.getInt("bbs_count"));
				bbsBean.setBbs_date(rs.getString("bbs_date"));
				bbsBean.setBbs_deleteStatus(rs.getInt("bbs_deleteStatus"));
				bbsBean.setBbs_commentStatus(rs.getInt("bbs_comentStatus"));
				bbsBean.setBbs_secretStatus(rs.getInt("bbs_secretStatus"));
				bbsBean.setBbs_filename(rs.getString("bbs_filename"));
				bbsBean.setBbs_password(rs.getString("bbs_password"));
				
				bbsList.add(bbsBean);
			}
			return bbsList;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return bbsList;
	}
	
	public List getAdminOrderSearchList(String search, int type, int page, int limit, String date1, String date2, String select) {
		database = new Database();
		int startRow = (page - 1) * limit + 1; 
		List bbsList = new ArrayList<>();
		String sql = "";
		switch(select) {
		case "all" :
			sql = "select * from orderrequest where order_date >= ? and order_date < ? and (order_title like '%" + search + "%' or order_contents like '%" + search + "%' or usercompanyname like '%" +search+ "%') ORDER BY order_id DESC LIMIT ?,?";
			break;
		case "title" :
			sql = "select * from orderrequest where order_date >= ? and order_date < ? and order_title like '%" + search + "%' ORDER BY order_id DESC LIMIT ?,?";
			break;
		case "content" :
			sql = "select * from orderrequest where order_date >= ? and order_date < ? and order_contents like '%" + search + "%' ORDER BY order_id DESC LIMIT ?,?";
			break;
		case "company" :
			sql = "select * from orderrequest where order_date >= ? and order_date < ? and usercompanyname like '%" + search + "%' ORDER BY order_id DESC LIMIT ?,?";
			break;
		}
		
		
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setString(1, date1);
			pstmt.setString(2, date2);
			pstmt.setInt(3, startRow - 1);
			pstmt.setInt(4, limit);
			ResultSet rs = database.executePreaparedQry();

			while (rs.next()) {
				OrderBean orderbean = new OrderBean();
				orderbean.setOrder_id(rs.getInt("order_id"));
				orderbean.setUsername(rs.getString("username"));
				orderbean.setUsercompanyname(rs.getString("usercompanyname"));
				orderbean.setUserphone(rs.getString("userphone"));
				orderbean.setUseremail(rs.getString("useremail"));
				orderbean.setTitle(rs.getString("order_title"));
				orderbean.setContents(rs.getString("order_contents"));
				orderbean.setOrder_status(rs.getInt("order_status"));
				orderbean.setOrder_memo(rs.getString("order_memo"));
				orderbean.setOrder_date(rs.getString("order_date"));
				
				
				bbsList.add(orderbean);
			}
			return bbsList;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return bbsList;
	}
	

}
