package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bean.BBSBean;
import common.Database;

public class AdminDAO {
	private Database database;

	public boolean adminCheck(String id, String pass) {
		database = new Database();
		boolean bool = false;
		System.out.println(id+"id");
		System.out.println(pass+"pass");
		String sql = "SELECT userid FROM adminuser where userid = ? and userpass = ?";
		try {
			PreparedStatement ptsmt = database.preparedQry(sql);
			ptsmt = database.preparedQry(sql);
			ptsmt.setString(1,id);
			ptsmt.setString(2, pass);

			ResultSet rs = database.executePreaparedQry();
			while (rs.next()) {
				String result = rs.getString(1);
				System.out.println(result);
				bool = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		System.out.println(bool);
		return bool;
	}
	
	public boolean adminPassCheck(String pass) {
		database = new Database();
		boolean bool = false;
		
		System.out.println(pass+"pass");
		String sql = "SELECT userid FROM adminuser where userpass = ?";
		try {
			PreparedStatement ptsmt = database.preparedQry(sql);
			ptsmt = database.preparedQry(sql);
			ptsmt.setString(1, pass);

			ResultSet rs = database.executePreaparedQry();
			while (rs.next()) {
				String result = rs.getString(1);
				System.out.println(result);
				bool = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		System.out.println(bool);
		return bool;
	}
	
	public boolean adminPassChange(String pass) {
		database = new Database();
		boolean bool = false;
		
		String sql = "UPDATE adminuser set userpass = ?";
		try {
			PreparedStatement ptsmt = database.preparedQry(sql);
			ptsmt = database.preparedQry(sql);
			ptsmt.setString(1, pass);
			int result = database.executePreaparedUpdateQry();
			System.out.println(pass+result);
			if (result>0) {
				bool = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		System.out.println(bool);
		return bool;
	}
	
	public int reWriteBBS(int type, int num, String title, String content) {
		database = new Database();
		System.out.println("rewrite!:--------------");
		System.out.println(type);
		System.out.println(num);
		System.out.println(title);
		System.out.println(content);
		System.out.println("rewrite!:--------------");
		
		String sql = "update bbs set bbs_title = ? , bbs_content = ? where bbs_type = ? and bbs_num = ?";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setString(1, title);
			pstmt.setString(2, content);
			pstmt.setInt(3, type);
			pstmt.setInt(4, num);

			int result = database.executePreaparedUpdateQry();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return -1;
	}
}
