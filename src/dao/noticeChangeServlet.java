package dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import bean.ComentBean;

/**
 * Servlet implementation class noticeChangeServlet
 */
@WebServlet("/noticeChange")
public class noticeChangeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request,HttpServletResponse response) 
			throws IOException,ServletException{
			    this.doPost(request,response);
			}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		
		int type = Integer.parseInt(request.getParameter("type"));
		int num = Integer.parseInt(request.getParameter("num"));
		
		String status = "";
		String content = "";
		String title = request.getParameter("title");	
		if(request.getParameter("status")!=null) {
			status = request.getParameter("status");
		}
		
		if(request.getParameter("content")!=null) {
			content = request.getParameter("content");
		}
		response.getWriter().write(getMsg(content, type, num, title, status));
	}
	
	public String getMsg(String content, int type, int num, String title, String status) {
		BBSDAO bbsdao = new BBSDAO();
		
		String msg = "";
		int result = bbsdao.statusBBS(num, type, status, content);
		System.out.println("result :::: "+result);
		if(result ==-1) {
			msg = "false";
		} else if (result > 0) {
			msg = "true";
		} else {
			msg = "false";
		}
	
		return msg;
	}

}
