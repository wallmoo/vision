package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.OrderBean;
import common.Database;

public class OrderDAO {
	Database database;

	public boolean orderInsert(OrderBean bean) {
		database = new Database();
		boolean bool = false;
		int result = 0;
		BBSDAO bbsdao = new BBSDAO();
		String date = bbsdao.getDate();
		String sql = "INSERT INTO orderrequest(username, usercompanyname, userphone, useremail,"
				+ " order_title, order_contents, order_status, order_date) VALUES (?,?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setString(1, bean.getUsername());
			pstmt.setString(2, bean.getUsercompanyname());
			pstmt.setString(3, bean.getUserphone());
			pstmt.setString(4, bean.getUseremail());
			pstmt.setString(5, bean.getTitle());
			pstmt.setString(6, bean.getContents());
			pstmt.setInt(7, 0);
			pstmt.setString(8, date);
			
			result = database.executePreaparedUpdateQry();
			if(result == 1) {
				bool = true;
			}
		}catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return bool;
	}
	
	public List orderGetList(int page, int limit) {
		database = new Database();
		int startRow = (page - 1) * limit + 1;
		List orderlist = new ArrayList<>();
		
		String sql = "SELECT * FROM orderrequest order by order_id DESC LIMIT ?,?";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, startRow -1);
			pstmt.setInt(2, limit);
			ResultSet rs = database.executePreaparedQry();
			
			while(rs.next()) {
				OrderBean orderbean = new OrderBean();
				orderbean.setOrder_id(rs.getInt("order_id"));
				orderbean.setTitle(rs.getString("order_title"));
				orderbean.setContents(rs.getString("order_contents"));
				orderbean.setOrder_status(rs.getInt("order_status"));
				orderbean.setUsername(rs.getString("username"));
				orderbean.setUseremail(rs.getString("useremail"));
				orderbean.setUsercompanyname(rs.getString("usercompanyname"));
				orderbean.setUserphone(rs.getString("userphone"));
				orderbean.setOrder_date(rs.getString("order_date"));
				orderbean.setOrder_memo(rs.getString("order_memo"));
				
				orderlist.add(orderbean);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			database.close();
		}
		return orderlist;
	}
	
	public List orderGetDetail(int id) {
		database = new Database();
		List orderlist = new ArrayList<>();
		
		String sql = "SELECT * FROM orderrequest where order_id = ?";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, id);
			
			ResultSet rs = database.executePreaparedQry();
			
			while(rs.next()) {
				OrderBean orderbean = new OrderBean();
				orderbean.setOrder_id(rs.getInt("order_id"));
				orderbean.setTitle(rs.getString("order_title"));
				orderbean.setContents(rs.getString("order_contents"));
				orderbean.setOrder_status(rs.getInt("order_status"));
				orderbean.setUsername(rs.getString("username"));
				orderbean.setUseremail(rs.getString("useremail"));
				orderbean.setUsercompanyname(rs.getString("usercompanyname"));
				orderbean.setUserphone(rs.getString("userphone"));
				orderbean.setOrder_date(rs.getString("order_date"));
				orderbean.setOrder_memo(rs.getString("order_memo"));
				
				orderlist.add(orderbean);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			database.close();
		}
		return orderlist;
	}
	
	public boolean orderStatusChange(int order_id, int status) {
		database = new Database();
		boolean bool = false;
		String sql = "UPDATE orderrequest SET order_status = ?  where order_id = ?";
		
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, status);
			pstmt.setInt(2, order_id);
			
			int rs = database.executePreaparedUpdateQry();
			if (rs > 0) {
				bool = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return bool;
	}
	
	public boolean orderMemoChange(int order_id, String memo) {
		database = new Database();
		boolean bool = false;
		System.out.println("DAO MEMO :::"+memo);
		String sql = "UPDATE orderrequest SET order_memo = ? where order_id = ?";
		
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setString(1, memo);
			pstmt.setInt(2, order_id);
			
			int rs = database.executePreaparedUpdateQry();
			if (rs > 0) {
				bool = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return bool;
	}
}
