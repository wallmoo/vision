package dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.BBSBean;
import bean.OrderBean;

/**
 * Servlet implementation class noticeSerchServlet
 */
@WebServlet("/orderSerch")
public class orderSerchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request,HttpServletResponse response) 
			throws IOException,ServletException{
			    this.doPost(request,response);
			}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		int page = 1;
		int limit = 10;
		int type = Integer.parseInt(request.getParameter("type"));
		String date1 = request.getParameter("date1");
		String date2 = request.getParameter("date2");
		String select = request.getParameter("select");
		String serch = request.getParameter("serch");
		if (request.getParameter("page") != null) {
			page = Integer.parseInt(request.getParameter("page"));
		}
		response.getWriter().write(getJSON(serch, type, page, limit, date1, date2, select));
	}

	public String getJSON(String serch, int type, int page, int limit, String date1, String date2, String select) {
		BBSDAO bbsdao = new BBSDAO();
		OrderBean orderbean = new OrderBean();
		
		List bbsList = new ArrayList();
		JSONObject jsonObject = new JSONObject();
		JSONArray personArray = new JSONArray();
	    JSONObject noticeInfo = new JSONObject();
	    
	    bbsList = bbsdao.getAdminOrderSearchList(serch, type, page, limit, date1, date2, select);
	    
		for(int i=0;i<bbsList.size();i++) {
			orderbean = (OrderBean) bbsList.get(i);
			String date = orderbean.getOrder_date();
			
			noticeInfo.put("orderid", orderbean.getOrder_id());
			noticeInfo.put("username", orderbean.getUsername());
			noticeInfo.put("usercompanyname", orderbean.getUsercompanyname());
			noticeInfo.put("userphone", orderbean.getUserphone());
			noticeInfo.put("useremail", orderbean.getUseremail());	
			noticeInfo.put("ordertitle", orderbean.getTitle());
			noticeInfo.put("ordercontents", orderbean.getContents());
			noticeInfo.put("orderstatus", orderbean.getOrder_status());
			noticeInfo.put("ordermemo", orderbean.getOrder_memo());
			noticeInfo.put("orderdate", date.substring(0, 11));
			personArray.add(noticeInfo);
			
		}
		jsonObject.put("serchorder",personArray);
		String jsonInfo = jsonObject.toJSONString();
		 
	    System.out.print(jsonInfo);
		return jsonInfo.toString();
	}

}
