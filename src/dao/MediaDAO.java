package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bean.BBSBean;
import bean.BBSFileBean;
import bean.MediaBean;
import common.Database;

public class MediaDAO {
	Database database;
	
	public int getNext(int type) {
		database = new Database();
		int num = 0;
		String sql = "SELECT MAX(media_num) FROM media where media_type = ?";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, type);
			ResultSet rs = database.executePreaparedQry();
			if(rs.next()) {
				num = rs.getInt(1) + 1;
			} else {
				num = 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return num;
	}
	
	public boolean mediaInsert(MediaBean mediabean, List<BBSFileBean> fileList) {
		database = new Database(); 
		BBSDAO bbsdao = new BBSDAO();
		String date = bbsdao.getDate();
		boolean bool = false;
		int rs = 0;
		int num = getNext(mediabean.getMedia_type());
		String sql = "INSERT INTO media(media_type, media_num,media_writer, media_title, media_filename, media_count, media_status,media_date) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, mediabean.getMedia_type());
			pstmt.setInt(2, num);
			pstmt.setString(3, mediabean.getMedia_writer());
			pstmt.setString(4, mediabean.getMedia_title());
			pstmt.setString(5, mediabean.getMedia_filename());
			pstmt.setInt(6, 0);
			pstmt.setInt(7, 0);
			pstmt.setString(8, date);
			rs = database.executePreaparedUpdateQry();
			if(rs == 1) {
				if (fileList.size() > 0) {
					String query = "INSERT INTO bbs_file (type,bbsid, filepath, filename) VALUES ";

					for (BBSFileBean bean : fileList) {
						query += "(" + bean.getType() + "," + bean.getBbsid() + ",'" + bean.getFilepath() + "', '" + bean.getFilename()
								+ "')";
						if (!fileList.get(fileList.size() - 1).equals(bean)) {
							query += ", ";
						}
					}
					pstmt = database.preparedQry(query);
					rs = database.executePreaparedUpdateQry();
					if(rs == 1) {
						bool = true;
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return bool;
	}
	
	public boolean mediaTMPInsert(MediaBean mediabean) {
		database = new Database(); 
		boolean bool = false;
		int rs = 0;
		BBSDAO bbsdao = new BBSDAO();
		String date = bbsdao.getDate();
		int num = getNext(mediabean.getMedia_type());
		String sql = "INSERT INTO media(media_type, media_num,media_writer, media_title, media_filename, media_count, media_status,media_date) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, mediabean.getMedia_type());
			pstmt.setInt(2, num);
			pstmt.setString(3, mediabean.getMedia_writer());
			pstmt.setString(4, mediabean.getMedia_title());
			pstmt.setString(5, mediabean.getMedia_filename());
			pstmt.setInt(6, 0);
			pstmt.setInt(7, 0);
			pstmt.setString(8, date);
			rs = database.executePreaparedUpdateQry();
			if(rs == 1) {	
				bool = true;	
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return bool;
	}
	
	public List mediaGetList(int type) {
		database = new Database();
		
		List media = new ArrayList();
		String sql = "SELECT * FROM media where media_type = ?";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, type);
			
			ResultSet rs = database.executePreaparedQry();
			while(rs.next()) {
				MediaBean mediabean = new MediaBean();
				mediabean.setMedia_id(rs.getInt("media_id"));
				mediabean.setMedia_num(rs.getInt("media_num"));
				mediabean.setMedia_status(rs.getInt("media_status"));
				mediabean.setMedia_title(rs.getString("media_title"));
				mediabean.setMedia_type(rs.getInt("media_type"));
				mediabean.setMedia_writer(rs.getString("media_writer"));
				mediabean.setMedia_filename(rs.getString("media_filename"));
				mediabean.setMedia_date(rs.getString("media_date"));
				mediabean.setMedia_count(rs.getInt("media_count"));
				mediabean.setMedia_content(rs.getString("media_content"));
				
				media.add(mediabean);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return media;
				
	}
	
	public int getListCount(int type) {
		database = new Database();
		int count = 0;

		try {
			// COUNT() �Լ��� ��ü �� ���� ��ȸ
			PreparedStatement pstmt = database.preparedQry("SELECT COUNT(*) FROM media where media_type=?");
			pstmt.setInt(1, type);
			ResultSet rs = database.executePreaparedQry();

			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return count;
	} // getListCount()
	
	public List getBBSDetail(int num, int type) {
		database = new Database();
		MediaBean mediabean = null;
		List bbsList = new ArrayList<>();
		String sql = "select * from media where media_num = ? and media_type = ?";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, num);
			pstmt.setInt(2, type);

			ResultSet rs = database.executePreaparedQry();

			if (rs.next()) {
				mediabean = new MediaBean();
				mediabean.setMedia_writer(rs.getString("media_writer"));
				mediabean.setMedia_type(rs.getInt("media_type"));
				mediabean.setMedia_num(rs.getInt("media_num"));
				mediabean.setMedia_id(rs.getInt("media_id"));
				mediabean.setMedia_title(rs.getString("media_title"));
				mediabean.setMedia_count(rs.getInt("media_count"));
				mediabean.setMedia_date(rs.getString("media_date"));
				mediabean.setMedia_status(rs.getInt("media_status"));
				mediabean.setMedia_filename(rs.getString("media_filename"));
				
				bbsList.add(mediabean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return bbsList;
	}
	
	public void updateMediacount(int num, int type) {
		database = new Database();
		String sql = "";
		
		sql = "update media set media_count=media_count+1 where media_type=? and media_num=?";
		
		try {

			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, type);
			pstmt.setInt(2, num);
			int result = database.executePreaparedUpdateQry();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
	}

}
