package dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import bean.BBSBean;

/**
 * Servlet implementation class noticeSerchServlet
 */
@WebServlet("/noticeSerch")
public class noticeSerchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		int page = 1;
		int limit = 10;
		int type = Integer.parseInt(request.getParameter("type"));
		String srch = request.getParameter("srch");

		if (request.getParameter("page") != null) {
			page = Integer.parseInt(request.getParameter("page"));
		}
		response.getWriter().write(getJSON(srch, type, page, limit));
	}

	public String getJSON(String srch, int type, int page, int limit) {
		BBSDAO bbsdao = new BBSDAO();
		BBSBean bbsBean = new BBSBean();
		HashMap map = new HashMap();
		List bbsList = new ArrayList();
		JSONObject jsonObject = new JSONObject();
		JSONArray personArray = new JSONArray();
	    JSONObject noticeInfo = new JSONObject();
	    
	    bbsList = bbsdao.getBBSSearchList(srch, type, page, limit);
	    
		for(int i=0;i<bbsList.size();i++) {
			bbsBean = (BBSBean) bbsList.get(i);
			String date = bbsBean.getBbs_date();
			
			noticeInfo.put("num", bbsBean.getBbs_num());
			noticeInfo.put("type", bbsBean.getBbs_type());
			noticeInfo.put("writer", bbsBean.getBbs_writer());
			noticeInfo.put("title", bbsBean.getBbs_title());
			noticeInfo.put("content", bbsBean.getBbs_content());	
			noticeInfo.put("date", date.substring(0, 11));
			noticeInfo.put("count", bbsBean.getBbs_count());
			noticeInfo.put("deleteStatus", bbsBean.getBbs_deleteStatus());
			noticeInfo.put("secretStatus", bbsBean.getBbs_secretStatus());
			personArray.add(noticeInfo);
		}
		jsonObject.put("notice",personArray);
		 String jsonInfo = jsonObject.toJSONString();
		 
	        System.out.print(jsonInfo);
		return jsonInfo.toString();
	}

}
