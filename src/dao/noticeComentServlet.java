package dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import bean.BBSBean;
import bean.ComentBean;

/**
 * Servlet implementation class noticeComentServlet
 */
@WebServlet("/noticeComent")
public class noticeComentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request,HttpServletResponse response) 
			throws IOException,ServletException{
			    this.doPost(request,response);
			}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		String re = null;
		int type = Integer.parseInt(request.getParameter("type"));
		int num = Integer.parseInt(request.getParameter("num"));
		if(request.getParameter("reWrite")!=null) {
			re = request.getParameter("reWrite");
		}
		String coment = request.getParameter("coment");
		String userid = request.getParameter("user");

		response.getWriter().write(getJSON(coment, type, num, userid, re));

	}
	
	public String getJSON(String coment, int type, int num, String userid, String re) {
		ComentDAO comentdao = new ComentDAO();
		ComentBean comentbean = new ComentBean();
		List comentList = new ArrayList();
		BBSDAO bbsdao = new BBSDAO();
		boolean bool = false;
		JSONObject jsonObject = new JSONObject();
		JSONArray personArray = new JSONArray();
	    JSONObject comentInfo = new JSONObject();
	   
	    comentbean.setBoardid(num);
	    comentbean.setContent(coment);
	    comentbean.setType(type);
	    comentbean.setUserid(userid);
	    System.out.println(re+":::::re CHECK !!");
	    if(!re.equals("null")) {
	    	int rs =comentdao.reWriteComent(comentbean);
	    	if(rs==1) {
	    		System.out.println("::::if !!");
	    		bool = true;
	    	}
		} else if (re.equals("null")) {
			 System.out.println("::::else!!");
			 bool = comentdao.insert(comentbean);
			 
		}
	   
	    /**/
		comentList = comentdao.getComentList(type, num);
		for(int i =0;i<comentList.size();i++) {
			comentbean = (ComentBean) comentList.get(i);
			comentInfo.put("id", comentbean.getId());
		}
		comentInfo.put("num", num);
		comentInfo.put("type", type);
		comentInfo.put("writer", userid);
		comentInfo.put("content", coment);
		comentInfo.put("date", bbsdao.getDate().substring(0,16));
		
		personArray.add(comentInfo);
		
		jsonObject.put("coment",personArray);
		String jsonInfo = jsonObject.toJSONString();
		 
	    System.out.print(jsonInfo);
		return jsonInfo;
	}

}
