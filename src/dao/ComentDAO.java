package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bean.ComentBean;
import common.Database;
import dao.BBSDAO;
public class ComentDAO {
	Database database;
	
	private String content = "";
	
	
	public boolean insert(ComentBean comentbean) {
		database = new Database();
		BBSDAO bbsdao = new BBSDAO();
		String content ="";
		int result = 0;
		String date = bbsdao.getDate();
		String sql = "INSERT INTO comment(userid, type, boardid, content, regdate, deleteCheck) values (?,?,?,?,?,?)";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setString(1, comentbean.getUserid());
			pstmt.setInt(2, comentbean.getType());
			pstmt.setInt(3, comentbean.getBoardid());
			pstmt.setString(4, comentbean.getContent());
			pstmt.setString(5, date);
			pstmt.setInt(6, 0);
			
			result = database.executePreaparedUpdateQry();
			if (result==1) {
				String status = "coment";
				bbsdao.statusBBS(comentbean.getBoardid(), comentbean.getType(),status, content);
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return false; 
	}
	
	public List getComentList(int type, int num) {
		database = new Database();
		
		List comentList = new ArrayList<>();
		System.out.println("DAO NUM ::::"+num);
		String sql = "SELECT * FROM comment where type = ? and boardid = ? order by id DESC";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setInt(1, type);
			pstmt.setInt(2, num);
			ResultSet rs = database.executePreaparedQry();
			while(rs.next()) {
				ComentBean comentbean = new ComentBean();
				comentbean.setId(rs.getInt("id"));
				comentbean.setType(rs.getInt("type"));
				comentbean.setUserid(rs.getString("userid"));
				comentbean.setBoardid(rs.getInt("boardid"));
				comentbean.setContent(rs.getString("content"));
				comentbean.setRegdate(rs.getString("regdate"));
				comentbean.setDeleteCheck(rs.getInt("deleteCheck"));		
				comentList.add(comentbean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return comentList;
	}
	
	public int reWriteComent(ComentBean comentbean) {
		database = new Database();
		BBSDAO bbsdao = new BBSDAO();
		String date = bbsdao.getDate();
		String sql = "UPDATE comment SET content = ? and regdate = ? where type = ? and boardid = ?";
		try {
			PreparedStatement pstmt = database.preparedQry(sql);
			pstmt.setString(1, comentbean.getContent());
			pstmt.setString(2, date);
			pstmt.setInt(3, comentbean.getType());
			pstmt.setInt(4, comentbean.getBoardid());
			
			return database.executePreaparedUpdateQry();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}
		return 0;
	}
	
	public int getListCount(int type, int num) {
		database = new Database();
		int count = 0;

		try {
			PreparedStatement pstmt = database.preparedQry("SELECT COUNT(*) FROM comment where type=? and boardid = ? and deleteCheck = 0");
			pstmt.setInt(1, type);
			pstmt.setInt(2, num);
			ResultSet rs = database.executePreaparedQry();

			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			database.close();
		}

		return count;
	} // getListCount()
}
