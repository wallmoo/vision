package dao;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * Servlet implementation class adminCheckPass
 */
@WebServlet("/adminCheckPass")
public class adminCheckPass extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,HttpServletResponse response) 
			throws IOException,ServletException{
			    this.doPost(request,response);
			}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		
		String pass = request.getParameter("pass");
		
	
		response.getWriter().write(getMsg(pass));
	}
	
	public String getMsg(String pass) {
		BBSDAO bbsdao = new BBSDAO();
		AdminDAO admindao = new AdminDAO();
		String msg = "";
		boolean result = admindao.adminPassCheck(pass);
		System.out.println("result :::: "+result);
		if(!result) {
			msg = "false";
		} else if (result) {
			msg = "true";
		} else {
			msg = "false";
		}
	
		return msg;
	}

}
