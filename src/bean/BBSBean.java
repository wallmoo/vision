package bean;


public class BBSBean {
	private int bbs_type;
	private int bbs_num;
	private String bbs_writer;
	private String bbs_content;
	private String bbs_filename;
	private int bbs_commentStatus;
	private int bbs_secretStatus;
	private int bbs_deleteStatus;
	private String bbs_password;
	private int bbs_count;
	private int bbs_id;
	private String bbs_date;
	private String bbs_title;
	
	
	public int getBbs_type() {
		return bbs_type;
	}
	public void setBbs_type(int bbs_type) {
		this.bbs_type = bbs_type;
	}
	public int getBbs_num() {
		return bbs_num;
	}
	public void setBbs_num(int bbs_num) {
		this.bbs_num = bbs_num;
	}
	public String getBbs_writer() {
		return bbs_writer;
	}
	public void setBbs_writer(String bbs_writer) {
		this.bbs_writer = bbs_writer;
	}
	public String getBbs_content() {
		return bbs_content;
	}
	public void setBbs_content(String bbs_content) {
		this.bbs_content = bbs_content;
	}
	public String getBbs_filename() {
		return bbs_filename;
	}
	public void setBbs_filename(String bbs_filename) {
		this.bbs_filename = bbs_filename;
	}
	public int getBbs_commentStatus() {
		return bbs_commentStatus;
	}
	public void setBbs_commentStatus(int bbs_commentStatus) {
		this.bbs_commentStatus = bbs_commentStatus;
	}
	public int getBbs_secretStatus() {
		return bbs_secretStatus;
	}
	public void setBbs_secretStatus(int bbs_secretStatus) {
		this.bbs_secretStatus = bbs_secretStatus;
	}
	public int getBbs_deleteStatus() {
		return bbs_deleteStatus;
	}
	public void setBbs_deleteStatus(int bbs_deleteStatus) {
		this.bbs_deleteStatus = bbs_deleteStatus;
	}
	public String getBbs_password() {
		return bbs_password;
	}
	public void setBbs_password(String bbs_password) {
		this.bbs_password = bbs_password;
	}
	public int getBbs_count() {
		return bbs_count;
	}
	public void setBbs_count(int bbs_count) {
		this.bbs_count = bbs_count;
	}
	public int getBbs_id() {
		return bbs_id;
	}
	public void setBbs_id(int bbs_id) {
		this.bbs_id = bbs_id;
	}
	public String getBbs_title() {
		return bbs_title;
	}
	public void setBbs_title(String bbs_title) {
		this.bbs_title = bbs_title;
	}
	public String getBbs_date() {
		return bbs_date;
	}
	public void setBbs_date(String bbs_date) {
		this.bbs_date = bbs_date;
	}


}
