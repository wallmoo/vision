package bean;

public class MediaBean {
	private int media_id;
	private int media_type;
	private int media_num;
	private int media_status;
	private String media_title;
	private String media_writer;
	private String media_date;
	private String media_content;
	private String media_filename;
	private int media_count;

	public int getMedia_id() {
		return media_id;
	}
	public void setMedia_id(int media_id) {
		this.media_id = media_id;
	}
	public int getMedia_type() {
		return media_type;
	}
	public void setMedia_type(int media_type) {
		this.media_type = media_type;
	}
	public int getMedia_num() {
		return media_num;
	}
	public void setMedia_num(int media_num) {
		this.media_num = media_num;
	}
	public String getMedia_title() {
		return media_title;
	}
	public void setMedia_title(String media_title) {
		this.media_title = media_title;
	}
	public String getMedia_content() {
		return media_content;
	}
	public void setMedia_content(String media_content) {
		this.media_content = media_content;
	}
	public String getMedia_filename() {
		return media_filename;
	}
	public void setMedia_filename(String media_filename) {
		this.media_filename = media_filename;
	}
	public int getMedia_count() {
		return media_count;
	}
	public void setMedia_count(int media_count) {
		this.media_count = media_count;
	}
	public String getMedia_writer() {
		return media_writer;
	}
	public void setMedia_writer(String media_writer) {
		this.media_writer = media_writer;
	}
	public int getMedia_status() {
		return media_status;
	}
	public void setMedia_status(int media_status) {
		this.media_status = media_status;
	}
	public String getMedia_date() {
		return media_date;
	}
	public void setMedia_date(String media_date) {
		this.media_date = media_date;
	}
	
}
