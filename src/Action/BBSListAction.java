package Action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BBSDAO;
import dao.MediaDAO;


public class BBSListAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
	        System.out.println("listAction");
	        BBSDAO bbsDAO = new BBSDAO();
	        MediaDAO mediadao = new MediaDAO();
	        List bbsList = new ArrayList();
	        ActionForward forward = new ActionForward();
	        int listCount = 0;
	        int page = 1; // 현재페이지의 기본값 
	        int limit = 10; // 페이징 10개
	        
	        int type = Integer.parseInt(request.getParameter("type"));
	        
	        if(type==51 || type==52 || type==53 || type==54) {
	        	listCount = bbsDAO.getListCount(type); 
	        	bbsList = bbsDAO.getBBSList(page, limit, type);  	  
		        request.setAttribute("bbsList", bbsList);
	        } else {
	        	listCount =  mediadao.getListCount(type);
	        	bbsList = mediadao.mediaGetList(type);
	        	request.setAttribute("medialist", bbsList);
	        }
	        int maxPage = (int)((double)listCount / limit + 0.95);	// 페이징 max 페이지
	        int pageBlock = 5;
	        int startPage = ((int)((double)page / pageBlock + 0.9) - 1) * pageBlock + 1;
	        int endPage = startPage + pageBlock - 1;
	        if(endPage > maxPage) endPage = maxPage;
	        
	        if(request.getParameter("page") != null) {
	            page = Integer.parseInt(request.getParameter("page"));
	        }
	        request.setAttribute("maxPage", maxPage);
	        request.setAttribute("startPage", startPage);
	        request.setAttribute("endPage", endPage);
	        request.setAttribute("listCount", listCount);
	        request.setAttribute("page", page);
	        request.setAttribute("type", type);
      
	        forward.setRedirect(false);
	        
	        if(type==51){
	        	forward.setPath("/notice.jsp");
	        } else if(type==52) {
	        	forward.setPath("/onlineenquiries.jsp");
	        } else if ( type== 53) {
	        	forward.setPath("/orderrequest.jsp");
	        }
	        else if(type==54) {
	        	forward.setPath("/data.jsp");
	        } else if(type==21 || type==22) {
				forward.setPath("/catalog.jsp");
			} 
	        return forward;
	}

}
