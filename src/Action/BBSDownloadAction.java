package Action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BBSDAO;

public class BBSDownloadAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ActionForward forward = new ActionForward();
		BBSDAO bbsdao = new BBSDAO();

		String filename = (String) request.getParameter("filename");
		int page = Integer.parseInt(request.getParameter("page"));
		int type = Integer.parseInt(request.getParameter("type"));
		int id	 = Integer.parseInt(request.getParameter("id"));

		String saveFolder = "upload/type0";

		String realFolder = request.getRealPath(saveFolder);
		String filePath = realFolder + "\\" + filename;
		System.out.println(filePath);
		try {
			String strFilename = java.net.URLDecoder.decode(filename);
			String strFilenameOutput = new String(strFilename.getBytes("euc-kr"), "8859_1");
			File file = new File(filePath);
			byte b[] = new byte[(int) file.length()];
			response.setHeader("Content-Disposition", "attachment;filename=" + strFilenameOutput);
			response.setHeader("Content-Length", String.valueOf(file.length()));
			if (file.isFile()) {
				BufferedInputStream fin = new BufferedInputStream(new FileInputStream(file));
				BufferedOutputStream outs = new BufferedOutputStream(response.getOutputStream());
				int read = 0;
				while ((read = fin.read(b)) != -1) {
					outs.write(b, 0, read);
				}
				outs.close();
				fin.close();
			}

			forward.setRedirect(true);
			forward.setPath("./detail.bs?type="+type+"&page="+page+"&id="+id);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
