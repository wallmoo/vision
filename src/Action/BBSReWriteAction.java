package Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BBSDAO;
import bean.BBSBean;

public class BBSReWriteAction implements Action {
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		BBSDAO bbsdao = new BBSDAO();
		BBSBean bbsbean = new BBSBean();

		ActionForward forward = new ActionForward();

		int type = Integer.parseInt(request.getParameter("type"));
		int num = Integer.parseInt(request.getParameter("num"));
		
		String writer = request.getParameter("writer");
		String password = request.getParameter("password");
		String contents = request.getParameter("contents");
		String title = request.getParameter("title");
		System.out.println("!!!" + type + "/" + "title" + title + "/contents" + contents);
		
		String filtertitle = title.replaceAll(" ", "&nbsp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;")
				.replaceAll("\n", "<br>");

		String addPath = "";
		
		int result = bbsdao.reWriteBBS(type, num, filtertitle,contents,writer,password);
		System.out.println(result);
		

		addPath = "./list.bs?type=";
		addPath += type;
		forward.setRedirect(true);
		forward.setPath(addPath);

		return forward;

	}
}
