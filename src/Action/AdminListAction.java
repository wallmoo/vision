package Action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;
import dao.BBSDAO;
import dao.MediaDAO;
import dao.OrderDAO;

public class AdminListAction implements Action{
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
	        BBSDAO bbsdao = new BBSDAO();
	        OrderDAO orderdao = new OrderDAO();
	        MediaDAO mediadao = new MediaDAO();
	        List bbsList = new ArrayList();
	        ActionForward forward = new ActionForward();
	        
	        int page = 1; 
	        int limit = 10; 
	        
	        int type = Integer.parseInt(request.getParameter("type"));
	        int listCount = bbsdao.getListCount(type); 	
	        int maxPage = (int)((double)listCount / limit + 0.95);	
	        
	        int pageBlock = 5;
	        int startPage = ((int)((double)page / pageBlock + 0.9) - 1) * pageBlock + 1;
	        int endPage = startPage + pageBlock - 1;
	        if(endPage > maxPage) endPage = maxPage;
	        
	        if(request.getParameter("page") != null) {
	            page = Integer.parseInt(request.getParameter("page"));
	        }
	        
	        String path = "";
	        switch (type) {
	        case 51:
	        	path = "notice_admin.jsp";
	        	bbsList = bbsdao.getBBSList(page, limit, type);
	        	break;
	        case 52:
	        	path = "notice_online.jsp";
	        	bbsList = bbsdao.getBBSList(page, limit, type);
	        	break;
	        case 54:
	        	path = "notice_data.jsp";
	        	bbsList = bbsdao.getBBSList(page, limit, type);
	        	break;
	        case 53:
	        	path = "notice_order.jsp";
	        	bbsList = orderdao.orderGetList(page, limit);
	        	break;
	        case 21:
	        	path ="admin_catalog.jsp";
	        	bbsList = mediadao.mediaGetList(type);
	        	break;
	        case 22:
	        	path ="admin_catalog.jsp";
	        	bbsList = mediadao.mediaGetList(type);
	        	break;
	        }
	  
	        request.setAttribute("bbsList", bbsList);
	        request.setAttribute("maxPage", maxPage);
	        request.setAttribute("startPage", startPage);
	        request.setAttribute("endPage", endPage);
	        request.setAttribute("listCount", listCount);
	        request.setAttribute("page", page);
	        request.setAttribute("type", type);
	        forward.setRedirect(false);
	        System.out.println(path+"::ADMIN PATH");
	      
	        forward.setPath(path);
	       
	        return forward;
	}
}
