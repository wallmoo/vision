package Action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.BBSBean;
import dao.AdminDAO;
import dao.BBSDAO;
import dao.ComentDAO;
import dao.OrderDAO;

public class AdminChangeAction implements Action{
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		BBSDAO bbsdao = new BBSDAO();
		BBSBean bbsbean = new BBSBean();
		OrderDAO orderdao = new OrderDAO();
		ComentDAO comentDAO = new ComentDAO();
		AdminDAO admindao = new AdminDAO();
		List bbsList = new ArrayList();
		List comentlist = new ArrayList();
		ActionForward forward = new ActionForward();
		
		int num = 0;
		int type = 0;
		int status = 0;
		String pass = "";
		String path = "";
		String memo = "";
		String mode = "";
		String content = "";
		
		if(request.getParameter("num")!=null) {
			num = Integer.parseInt(request.getParameter("num"));
		}
		if(request.getParameter("type")!=null) {
			type = Integer.parseInt(request.getParameter("type"));
		}
		if(request.getParameter("pass")!=null) {
			pass = request.getParameter("pass");
		}
		if(request.getParameter("memo")!=null) {
			memo = request.getParameter("memo");
		}
		if(request.getParameter("status")!=null) {
			status = Integer.parseInt(request.getParameter("status"));
		}
		if(request.getParameter("mode")!=null) {
			mode  = request.getParameter("mode");
		}
		System.out.println(mode+"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		if(mode.equals("delete")) {
			int bool = bbsdao.statusBBS(num, type, mode, content);
			if(bool==1) {
				out.println("<script>");
				out.println("alert('정상적으로 삭제되었습니다.')");
				out.println("location.href='./adminList.ad?type="+type+"'");
				out.println("</script>");
			} else {
				out.println("<script>");
				out.println("alert('삭제가 실패되었습니다.')");
				out.println("location.href='./adminList.ad?type="+type+"'");
				out.println("</script>");
			}
		} else if (mode.equals("mediadelete")) {
			int bool = bbsdao.statusBBS(num, type, mode, content);
			if(bool==1) {
				out.println("<script>");
				out.println("alert('정상적으로 삭제되었습니다.')");
				out.println("location.href='./adminList.md?type="+type+"'");
				out.println("</script>");
			} else {
				out.println("<script>");
				out.println("alert('삭제가 실패되었습니다.')");
				out.println("location.href='./adminList.md?type="+type+"'");
				out.println("</script>");
			}
		} else if (mode.equals("changePw")) {
			boolean bool = admindao.adminPassChange(pass);
			if(bool) {
				out.println("<script>");
				out.println("alert('비밀번호가 변경되었습니다.')");
				out.println("location.href='./admin_index.jsp'");
				out.println("</script>");
			} else {
				out.println("<script>");
				out.println("alert('비빌번호가 변경되지 않았습니다.')");
				out.println("location.href='./admin_index.jsp'");
				out.println("</script>");
			} 
		}
		else {
			boolean bool = orderdao.orderStatusChange(num, status);
			boolean bool2 = orderdao.orderMemoChange(num, memo);
			if(bool && bool2) {
				out.println("<script>");
				out.println("alert('내용이 변경되었습니다.')");
				out.println("location.href='./adminList.ad?type=53'");
				out.println("</script>");
			} else {
				out.println("<script>");
				out.println("alert('내용이 변경되지 않았습니다.')");
				out.println("location.href='./adminList.ad?type=53'");
				out.println("</script>");
			} 
		}
		out.close();
		return null;
	}
}
