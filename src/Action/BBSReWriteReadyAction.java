package Action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.BBSDAO;
import bean.BBSBean;

public class BBSReWriteReadyAction implements Action {
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		BBSDAO bbsdao = new BBSDAO();
		BBSBean bbsbean = new BBSBean();
		ActionForward forward = new ActionForward();

		int type = Integer.parseInt(request.getParameter("type"));
		int num = Integer.parseInt(request.getParameter("num"));
		request.setAttribute("type", type);
		request.setAttribute("num", num);
		bbsbean = bbsdao.getBBSDetail(num, type);
		request.setAttribute("bbslist", bbsbean);
		
		String addPath = "/noticeModify.jsp";
		forward.setRedirect(false);
		forward.setPath(addPath);

		return forward;

	}
}
