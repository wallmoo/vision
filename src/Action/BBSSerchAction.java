package Action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.BBSBean;
import dao.BBSDAO;

public class BBSSerchAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		BBSBean bbsbean = new BBSBean();
		BBSDAO bbsdao = new BBSDAO();
		List bbsList = new ArrayList();
		ActionForward forward = new ActionForward();
		
		int page = 1; 
        int limit = 10; 
        
        if(request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }

		int type = Integer.parseInt(request.getParameter("type"));
		String srch = request.getParameter("srch");
        int listCount = bbsdao.getSearchListCount(type, srch);
        int maxPage = (int)((double)listCount / limit + 0.95);
        int pageBlock = 5;
        int startPage = ((int)((double)page / pageBlock + 0.9) - 1) * pageBlock + 1;
        int endPage = startPage + pageBlock - 1;
        if(endPage > maxPage) endPage = maxPage;

		bbsList = bbsdao.getBBSSearchList(srch, type,page, limit);
		if (bbsList != null) {
			request.setAttribute("bbsList", bbsList);
			request.setAttribute("maxPage", maxPage);
			request.setAttribute("startPage", startPage);
			request.setAttribute("endPage", endPage);
			request.setAttribute("listCount", listCount);
			request.setAttribute("page", page);
			request.setAttribute("type", type);
			request.setAttribute("srch", srch);
			forward.setRedirect(false);
			
		}
		 
        
        forward.setPath("/board_serch.jsp");
        
		return forward;
	}

}
