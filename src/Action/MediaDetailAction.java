package Action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.MediaBean;
import dao.BBSDAO;
import dao.MediaDAO;


public class MediaDetailAction implements Action{
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		MediaBean mediabean = new MediaBean();
		BBSDAO bbsdao = new BBSDAO();
		MediaDAO medaidao = new MediaDAO();
		List bbsList = new ArrayList();
		List comentlist = new ArrayList();
		ActionForward forward = new ActionForward();
		
		int num = Integer.parseInt(request.getParameter("num"));
		int type = Integer.parseInt(request.getParameter("type"));
		int page = Integer.parseInt(request.getParameter("page"));
		
		System.out.println(num+type+page);
		int listCount = medaidao.getListCount(type);
		String path = "";
		medaidao.updateMediacount(num, type);
		bbsList = medaidao.getBBSDetail(num, type);
		if(bbsList != null) {
			request.setAttribute("medialist", bbsList);
			request.setAttribute("page", page);
			request.setAttribute("type", type);
			request.setAttribute("listcount", listCount);
			path = "/videoView.jsp";
		}
		forward.setRedirect(false);
		forward.setPath(path);	
		return forward; 
	}
}
