package Action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.AdminBean;
import dao.AdminDAO;
import dao.BBSDAO;

public class AdminLoginAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ActionForward forward = new ActionForward();
		AdminBean adminbean = new AdminBean();
		AdminDAO admindao = new AdminDAO();
		BBSDAO bbsdao = new BBSDAO();
		
		HttpSession session = request.getSession();
		
		String id 	= request.getParameter("id");
		String pass = request.getParameter("pass");
		
		boolean bool =admindao.adminCheck(id,pass);
		response.setContentType("text/html; charset=UTF-8");
		if(bool) {
			session.setAttribute("admin", "관리자");
			session.setAttribute("date", bbsdao.getDate());
			forward.setRedirect(false);
			forward.setPath("/adminList.ad?type=51");
			
		} else {
			PrintWriter out = response.getWriter();
			out.println("<script>");
			out.println("alert('아이디 또는 비밀번호를 확인해주세요.')");
			out.println("location.href='/admin_index.jsp';"); 
			out.println("</script>");
			out.close();
			return null;
		}
		
		return forward;
	}

}
