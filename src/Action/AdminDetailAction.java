package Action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.BBSBean;
import dao.BBSDAO;
import dao.ComentDAO;
import dao.OrderDAO;

public class AdminDetailAction implements Action{
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		BBSDAO bbsdao = new BBSDAO();
		BBSBean bbsbean = new BBSBean();
		OrderDAO orderdao = new OrderDAO();
		ComentDAO comentDAO = new ComentDAO();
		List bbsList = new ArrayList();
		List comentlist = new ArrayList();
		ActionForward forward = new ActionForward();
		String path = "";
		int num = Integer.parseInt(request.getParameter("num"));
		int type = Integer.parseInt(request.getParameter("type"));
		int page = Integer.parseInt(request.getParameter("page"));
		
		int listCount = bbsdao.getListCount(type); 
		
		if(request.getParameter("mode")!=null) {
			String mode = request.getParameter("mode");
			if(mode.equals("rewrite")) {

				bbsbean = bbsdao.getBBSDetail(num, type);
				if(bbsList != null) {
					request.setAttribute("bbsList", bbsbean);
					request.setAttribute("page", page);
					request.setAttribute("type", type);
					request.setAttribute("listcount", listCount);
					path = "notice_Resignup.jsp";
				}
			}
		} else {
			int status = 0;
			String password = null;
			bbsdao.updateBBScount(num, type);
			
			if(type==53) {
				int id = num;
				bbsList = orderdao.orderGetDetail(id);
				request.setAttribute("orderList", bbsList);
				request.setAttribute("page", page);
				request.setAttribute("type", type);
				request.setAttribute("listcount", listCount);
				path = "order_detail.jsp";
			} else if (type==51 || type==54 ) {
				bbsbean = bbsdao.getBBSDetail(num, type);
				if(bbsList != null) {
					request.setAttribute("bbsList", bbsbean);
					request.setAttribute("page", page);
					request.setAttribute("type", type);
					request.setAttribute("listcount", listCount);
					path = "notice_detail.jsp";
				}
			} else if (type==52) {
				bbsbean = bbsdao.getBBSDetail(num, type);
				comentlist = comentDAO.getComentList(type, num);
				int comentCount = comentDAO.getListCount(type, num);
				if(bbsList != null) {
					request.setAttribute("bbsList", bbsbean);
					request.setAttribute("page", page);
					request.setAttribute("type", type);
					request.setAttribute("listcount", listCount);
					request.setAttribute("comentlist", comentlist);
					request.setAttribute("comentCount", comentCount);
					path = "notice_detail.jsp";
				}
			}
		}
		
		forward.setRedirect(false);
		forward.setPath(path);
		return forward; 
	}
}
