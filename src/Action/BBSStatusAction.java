package Action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.BBSBean;
import dao.BBSDAO;

public class BBSStatusAction implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		BBSBean bean = new BBSBean();
		BBSDAO bbsdao = new BBSDAO();
		ActionForward forward = new ActionForward();
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		int type = Integer.parseInt(request.getParameter("type"));
		int num = Integer.parseInt(request.getParameter("num"));
		String title = request.getParameter("title");
		String status = "";
		String content = "";
	
		if(request.getParameter("status")!=null) {
			status = request.getParameter("status");
		}
		
		if(request.getParameter("content")!=null) {
			content = request.getParameter("content");
		}
		int result = bbsdao.statusBBS(num, type, status, content);
		System.out.println("result :::: "+result);
		if(result ==-1) {
			out.println("<script>");
			out.println("alert('재시도 해주세요.')");
			out.println("location.href='./list.bs?type="+type+"'");
			out.println("</script>");
		} else if (result > 0) {
			out.println("<script>");
			out.println("alert('정상적으로 처리되었습니다.')");
			out.println("location.href='./list.bs?type="+type+"'");
			out.println("</script>"); 
		} else {
			out.println("<script>");
			out.println("alert('재시도 해주세요.')");
			out.println("location.href='./list.bs?type="+type+"'");
			out.println("</script>");
		}
		out.close(); 
		return null;
	}

}
