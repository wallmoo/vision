package Action;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import bean.BBSFileBean;
import bean.MediaBean;
import dao.MediaDAO;

public class MediaWriteAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ActionForward forward = new ActionForward();
		MediaBean mediabean = new MediaBean();
		MediaDAO mediadao = new MediaDAO();
		List<BBSFileBean> fileList = new ArrayList<BBSFileBean>();
		
		boolean result = false;
		int num = 0;
		int type =0;
		String path = request.getSession().getServletContext().getRealPath("/upload/type" + type).replaceAll("\\\\","/");
			
		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		int fileSize = 10 * 1024 * 1024;
		MultipartRequest multi = null;
		try {
			multi = new MultipartRequest(request, path, fileSize, "UTF-8", new DefaultFileRenamePolicy());
			type = Integer.parseInt(multi.getParameter("type"));
			String writer = multi.getParameter("writer");
			String title = multi.getParameter("title").replaceAll(" ", "&nbsp;").replaceAll("<", "&lt;")
					.replaceAll(">", "&gt;").replaceAll("\n", "<br>");
			num = mediadao.getNext(type);
			if(type==21) {
				mediabean.setMedia_num(num);
				mediabean.setMedia_type(type);
				mediabean.setMedia_title(title);
				mediabean.setMedia_writer(writer);
				mediabean.setMedia_filename(multi.getFilesystemName((String) multi.getFileNames().nextElement()));
					
				String formName = "", fileName = "";
				File file = null;
				Enumeration forms = multi.getFileNames(); 
					
				while (forms.hasMoreElements()) {
					formName = (String) forms.nextElement();

					fileName = multi.getFilesystemName(formName);
						
					if (fileName != null) 
					{
							
						BBSFileBean bean = new BBSFileBean();
						bean.setFilepath(path + "/" + fileName);
						bean.setFilename(multi.getOriginalFileName(formName));
						bean.setType(type);
						fileList.add(bean);
					}
				}
				result = mediadao.mediaInsert(mediabean, fileList);
			} else if(type==22) {
				String arr[] = multi.getParameter("url").split("/");

				mediabean.setMedia_num(num);
				mediabean.setMedia_type(type);
				mediabean.setMedia_title(multi.getParameter("title"));
				mediabean.setMedia_writer(writer);
				mediabean.setMedia_filename(multi.getParameter(arr[3]));
				
				result = mediadao.mediaTMPInsert(mediabean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		forward.setRedirect(true);
		forward.setPath("./adminList.md?type="+type);
		return forward;
	}

}
