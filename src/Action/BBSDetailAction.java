package Action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.omg.CORBA.INTERNAL;

import bean.BBSBean;
import dao.BBSDAO;
import dao.ComentDAO;

public class BBSDetailAction implements Action{
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		BBSDAO bbsdao = new BBSDAO();
		BBSBean bbsbean = new BBSBean();
		ComentDAO comentDAO = new ComentDAO();
		
		List bbsList = new ArrayList();
		List comentlist = new ArrayList();
		
		ActionForward forward = new ActionForward();
		
		int num = Integer.parseInt(request.getParameter("num"));
		int type = Integer.parseInt(request.getParameter("type"));
		int page = Integer.parseInt(request.getParameter("page"));
		int status = 0;
		String password = null;
		
		int listCount = bbsdao.getListCount(type);
		String path = "";
		
		bbsdao.updateBBScount(num, type);
		bbsbean = bbsdao.getBBSDetail(num, type);
		
		if(bbsbean != null) {
			request.setAttribute("bbsList", bbsbean);
			request.setAttribute("page", page);
			request.setAttribute("type", type);
			request.setAttribute("listcount", listCount);
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out = response.getWriter();
			if(type==51 || type==54) {
				path = "/board_content.jsp";
			} else if(type==52) {
				if(request.getParameter("su")!=null) {
					if(Integer.parseInt(request.getParameter("su"))==1) {
						String userpassword = request.getParameter("uw");
						password = bbsdao.getSecretPassword(num,type);
						if(userpassword.equals(password)) {
							comentlist = comentDAO.getComentList(type, num);
							int listcount = comentDAO.getListCount(type, num);
							
							request.setAttribute("comentlist", comentlist);
							request.setAttribute("listcount", listcount); 	
							path ="/online_content.jsp";
						} else {
							
							out.println("<script>");
							out.println("alert('패스워드가 일치하지않습니다.')");
							out.println("location.href='list.bs?type="+type+"'");
							out.println("</script>");
							out.close();
							return null;
						}
					}	else {
						out.println("<script>");
						out.println("alert('패스워드가 일치하지않습니다.')");
						out.println("location.href='list.bs?type="+type+"'");
						out.println("</script>");
						out.close();
						return null;
					}
				}	
			}
		}
		
		forward.setRedirect(false);
		forward.setPath(path);
		return forward; 
	}
}
