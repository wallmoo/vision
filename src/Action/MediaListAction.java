package Action;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import bean.BBSFileBean;
import bean.MediaBean;
import dao.MediaDAO;

public class MediaListAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ActionForward forward = new ActionForward();
		MediaBean mediabean = new MediaBean();
		MediaDAO mediadao = new MediaDAO();
		List medialist = new ArrayList();
		int type = Integer.parseInt(request.getParameter("type"));
		medialist = mediadao.mediaGetList(type);
		int page = 1; // ������������ �⺻�� 
        int limit = 9; // ����¡ 10��
        
        int listCount = mediadao.getListCount(type); 	
        int maxPage = (int)((double)listCount / limit + 0.95);
        
        int pageBlock = 5;
        int startPage = ((int)((double)page / pageBlock + 0.9) - 1) * pageBlock + 1;
        int endPage = startPage + pageBlock - 1;
        if(endPage > maxPage) endPage = maxPage;
        
        if(request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }
       
		request.setAttribute("maxPage", maxPage);
	    request.setAttribute("startPage", startPage);
	    request.setAttribute("endPage", endPage);
	    request.setAttribute("listCount", listCount);
	    request.setAttribute("page", page);
		String path = "";
		if(type==21 || type==22) {
			request.setAttribute("medialist", medialist);
			request.setAttribute("type", type);
			path = "./admin_catalog.jsp";
		} 
		/*
		 * else if ( type==22) {
			request.setAttribute("medialist", medialist);
			request.setAttribute("type", type);
			path = "/admin/video.jsp";
		}
		 * 
		 * */
		forward.setRedirect(false);
		forward.setPath(path);
		return forward;
	}	

}
