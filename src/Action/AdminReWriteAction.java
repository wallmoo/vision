package Action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AdminDAO;
import dao.BBSDAO;
import bean.BBSBean;

public class AdminReWriteAction implements Action {
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		BBSDAO bbsdao = new BBSDAO();
		BBSBean bbsbean = new BBSBean();
		AdminDAO admindao = new AdminDAO();
		ActionForward forward = new ActionForward();

		int type = Integer.parseInt(request.getParameter("type"));
		int num = Integer.parseInt(request.getParameter("num"));
		
	
		String contents = request.getParameter("contents");
		String title = request.getParameter("title");
		System.out.println("!!!" + type + "/" + "title" + title + "/contents" + contents);
		
		String filtertitle = title.replaceAll(" ", "&nbsp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;")
				.replaceAll("\n", "<br>");

		String addPath = "";
		
		int result = admindao.reWriteBBS(type, num, filtertitle,contents);
		System.out.println("관리자 수정 ::::"+result);
		
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		if(result > 0 ) {
			out.println("<script>");
			out.println("alert('내용이 변경되었습니다.')");
			out.println("location.href='./adminList.ad?type="+type+"'");
			out.println("</script>");
		} else {
			out.println("<script>");
			out.println("alert('내용이 변경되지 않았습니다.')");
			out.println("location.href='./adminList.ad?type="+type+"'");
			out.println("</script>");
		} 
		out.close();
		return null;

	}
}
