package Action;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.OrderBean;
import dao.OrderDAO;

public class BBSOrderAction implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ActionForward forward = new ActionForward();
		OrderBean orderbean = new OrderBean();
		OrderDAO orderdao = new OrderDAO();
		
		orderbean.setUsername(request.getParameter("username"));
		orderbean.setUsercompanyname(	request.getParameter("companyname"));
		orderbean.setUserphone(request.getParameter("userphone"));
		orderbean.setUseremail(request.getParameter("useremail"));
		orderbean.setTitle(request.getParameter("title"));
		orderbean.setContents(request.getParameter("contents"));
	
		boolean bool = orderdao.orderInsert(orderbean);
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		if (bool) {
			out.println("<script>");
			out.println("alert('주문의뢰가 신청되었습니다.')");
			out.println("location.href='orderrequest.jsp'");
			out.println("</script>");
			out.close();
		} else {
			out.println("<script>");
			out.println("alert('주문의뢰가 신청되지않았습니다.')"); 
			out.println("location.href='orderrequest.jsp'");
			out.println("</script>");
			out.close();
		}
		forward.setRedirect(true);
		return null;
	}	
}
