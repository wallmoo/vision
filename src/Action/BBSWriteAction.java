package Action;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import dao.BBSDAO;
import bean.BBSBean;
import bean.BBSFileBean;

public class BBSWriteAction implements Action {
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("write");
		BBSDAO bbsdao = new BBSDAO();
		BBSBean bbsbean = new BBSBean();
		List<BBSFileBean> fileList = new ArrayList<BBSFileBean>();
		ActionForward forward = new ActionForward();
		String id = null;
		String pass= null;
		int type = 0;
		if(request.getParameter("type")!=null) {
			type = Integer.parseInt(request.getParameter("type"));
		}
		int secretContent = 0;
		if(type!=52) {
			id = request.getParameter("writer");
			pass =  request.getParameter("password");
		}
		String path = request.getSession().getServletContext().getRealPath("/upload/type" + type).replaceAll("\\\\","/");
		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		int fileSize = 10 * 1024 * 1024;
		MultipartRequest multi = null;
		try {
			multi = new MultipartRequest(request, path, fileSize, "UTF-8", new DefaultFileRenamePolicy());
			if(type==52) {
				id = multi.getParameter("writer");
				pass =  multi.getParameter("password");
				secretContent = 1;
			}
			if(multi.getParameter("type")!=null) {
				id = multi.getParameter("writer");
				type = Integer.parseInt(multi.getParameter("type"));
				
			}
			String title = multi.getParameter("title").replaceAll(" ", "&nbsp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;")
					.replaceAll("\n", "<br>");
		
			String contents = multi.getParameter("contents");
			System.out.println("--------------------------------------");
			System.out.println("type::"+type);
			System.out.println("id::"+id);
			System.out.println("pass::"+pass);
			System.out.println("title::"+title);
			System.out.println("contents::"+contents);
			System.out.println("secretContent::"+secretContent);
			System.out.println("path::"+path);
			System.out.println("--------------------------------------");
			bbsbean.setBbs_type(type);
			bbsbean.setBbs_content(contents);
			bbsbean.setBbs_title(title);
			bbsbean.setBbs_writer(id);
			bbsbean.setBbs_password(pass);
			bbsbean.setBbs_secretStatus(secretContent);
			bbsbean.setBbs_filename(multi.getFilesystemName((String) multi.getFileNames().nextElement()));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String formName = "", fileName = "";
		File file = null;
		Enumeration forms = multi.getFileNames(); 
		
		while (forms.hasMoreElements()) {
			formName = (String) forms.nextElement();

			fileName = multi.getFilesystemName(formName);
			
			if (fileName != null) 
			{
				
				BBSFileBean bean = new BBSFileBean();
				bean.setFilepath(path + "/" + fileName);
				bean.setFilename(multi.getOriginalFileName(formName));
				bean.setType(type);
				fileList.add(bean);
			}
		}
		String addPath = "";
		boolean result = false;

		result = bbsdao.bbsInsert(bbsbean, fileList);
		if(id.equals("관리자")) {
			addPath = "./adminList.ad?type=";
		}else {
			addPath = "./list.bs?type=";
		}
		addPath += type;

		forward.setRedirect(true);
		forward.setPath(addPath);

		return forward;

	}
}
