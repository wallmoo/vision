/* main.js */

$(document).ready(function(){
	/*gnb */
		$(".gnb > ul > li").bind('mouseenter focusin', function() { 
			$(this).addClass('on').siblings().removeClass('on'); 
		}); 

		$(".gnb > ul > li").bind("mouseleave focusout", function() {
			$(this).removeClass("on");
		});
	/*모바일 검색*/ 
	var toggle_srch = true;
	$(".msearch").click(function(){
		
		if(toggle_srch){
			$(this).next().next().next().slideToggle();			
		}
		
	});
	/* 모바일 - 전체메뉴 */

	/* 전체메뉴 열기 */
	$(".header_wrap > img:nth-of-type(1)").click(function(){
		$('.allmenu').animate({"left":"0"});
	});

	/*화살표*/
	$('.allmenu > ul > li > a').click(function(e){
		e.preventDefault();
		$(this).toggleClass('submenu-open').next('.allmenu > ul > li > ul').slideToggle(300)
		.parent().siblings().children().removeClass('submenu-open').next('.allmenu > ul > li > ul').slideUp(300);
	});

	/* 전체메뉴 닫기 */
	$(".allmenu > h3 > img").click(function(){
		
		$('.allmenu').animate({"left":"-150%"});
	});
});


function subMove(type) {
	switch (type) {
	case 1:
		location.href = "about.jsp";
		break;
	case 2:
		location.href = "history.jsp";
		break;
	case 3:
		location.href = "certificate.jsp";
		break;
	case 4:
		location.href = "location.jsp";
		break;
	case 5:
		location.href = "vhls.jsp";
		break;
	case 6:
		location.href = "vbsl_s1.jsp";
		break;
	case 7:
		location.href = "vrll.jsp";
		break;
	case 8:
		location.href = "vsql_d.jsp";
		break;
	case 9:
		location.href = "vspl.jsp";
		break;
	case 10:
		location.href = "sepcial.jsp";
		break;
	case 11:
		location.href = "vbml.jsp";
		break;
	case 12:
		location.href = "visiontech.jsp";
		break;
	case 13:
		location.href = "kvision.jsp";
		break;
	case 14:
		location.href = "light.jsp";
		break;
	case 15: //돔조명
		location.href = "vdml.jsp";
		break;
	case 16: //등축
		location.href = "vpsl.jsp";
		break;
	case 51:
		location.href = "../list.bs?type="+type;
		break;
	case 52:
		location.href = "../list.bs?type="+type;
		break;
	case 53:
		location.href = "../list.bs?type="+type;
		break;
	case 21:
		location.href = "../list.md?type="+type;
		break;
	case 22:
		location.href = "../list.md?type="+type;
		break;
	case 54:
		location.href = "../list.bs?type="+type;
		break;
	}
}
