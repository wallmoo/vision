<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VISION TECHNOLOGY</title>

		<jsp:include page="header.jsp" />
<style>
.popup
{
	position: absolute;
	left: 0;
	top: 50px;
	margin: 0 0 0 0px;
	z-index: 1000;
	display: none;
	font-size:0.75em; font-family:Nanum Gothic, "나눔고딕", dotum, "돋움", verdana, Arial, sans-serif;
}

.popup .footer
{
	background: black;
	height: 24px;
}

.popup .footer span
{
	margin: 5px;
}

.popup .footer span.left
{
	float:left;
}

.popup .footer span.right
{
	float:right;
}

.popup .footer span a
{
	text-decoration:none;
	color: white;
}

</style>	
<script type="text/javascript">
	$(document).ready(function () {
		$('.popup').each(function (idx) {
			var id = $(this).attr('id');
			
			switch (idx) {
			    case 0://기본 실행  680 982
				    $(this).css({ 'left': '30%', 'top': '10px', 'width': '680px' });
					break;
			    case 1:
			        var offLeft = $('#eventDiv1')[0].offsetLeft;
			        if (offLeft < 0) {
			            offLeft = 0;
			        }

			        $(this).css({ 'left': offLeft + 'px', 'top': '174px', 'width': '480px' });
					break;
				case 2:
					$(this).css({ 'left': '700px', 'top': '50px', 'width': '500px' });
					break;
			}

			if (getCookie(id) == "" /*&& idx != 1*/ && idx != 2) {
				//팝업 주석화
				$('#'+id).show();
			}
			if (getCookie(id) == "done"){
				$('#' + id).hide();
			}
		});
	});

	function setCookie(popupname, value, expiredays) {
		var todayDate = new Date();
		todayDate.setDate(todayDate.getDate() + expiredays);
		document.cookie = popupname + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
	}

	function getCookie(popupname) {
		var popupnameOfCookie = popupname + "=";
		var x = 0;
		while (x <= document.cookie.length) {
			var y = (x + popupnameOfCookie.length);
			if (document.cookie.substring(x, y) == popupnameOfCookie) {
				if ((endOfCookie = document.cookie.indexOf(";", y)) == -1)
					endOfCookie = document.cookie.length;

				return unescape(document.cookie.substring(y, endOfCookie));
			}

			x = document.cookie.indexOf(" ", x) + 1;
			if (x == 0)
				break;
		}

		return "";
	}

	function closeOneDay(obj) {
		var id = $(obj).parent().parent().parent().attr('id');
		setCookie(id, "done", 1);
		closeSelf(obj);
	}

	function closeSelf(obj) {
		$(obj).parent().parent().parent().hide();
	}
</script>	
<div id="eventDiv1" style="width: 680px; left: 53%; top: 143px; display: block;" class="popup">
	<div><div>
		<a href="#none" onclick="closeSelf(this);" title="비젼테크 기술평가 우수기업 인증서"><img src="img/popup.gif" border="0" usemap="#popup2" alt="비젼테크 기술평가 우수기업 인증서"></a>
	</div></div>
	<div class="footer" style="width:680px;">
		<span class="left"><a href="#none" onclick="closeOneDay(this);">오늘 하루 이창을 열지 않습니다.</a></span>
		<span class="right"><a href="#none" onclick="closeSelf(this);">창닫기</a></span>
	</div>			
</div>		
		
		<!--메인 베너-->
		<section class='index_section'>
		<h2 class='blind'>메인 베너</h2>
		<ul>
			<li>
				<!--<img src='img/main_img1.jpg' alt=''>-->
			</li>
			<!--<li><img src='img/main_img2.jpg' alt=''></li>-->
		</ul>
		</section>	
		<!--container-->
		<div class='container'>
			<div class='con1'>
				<ul class="pc">
					<li><a href='javascript:subMove(14);'><img src='img/product.jpg' alt='제품소개'/></a></li>
					<li><a href='javascript:subMove(52);'><img src='img/inquiry.jpg' alt='궁금한점 안내'/></a></li>
					<li><a href='javascript:subMove(51);'><img src='img/news,notice.jpg' alt='새소식'/></a></li>
					<li><a href='javascript:subMove(4);'><img src='img/information.jpg' alt='회사 정보'/></a></li>
				</ul>
				<ul class="mobile">
					<li><img src='img/mobile_img1.jpg' alt='메인 이미지'/></li>
					<li><a href='javascript:subMove(14);'><img src='img/mobile_img2.jpg' alt='제품소개'/></a></li>
					<li><a href='javascript:subMove(52);'><img src='img/mobile_img3.jpg' alt='궁금한점 안내'/></a></li>
					<li><a href='javascript:subMove(51);'><img src='img/mobile_img4.jpg' alt='새소식'/></a></li>
					<li><a href='javascript:subMove(4);'><img src='img/mobile_img5.jpg' alt='회사 정보'/></a></li>
				</ul>
			</div>
		</div>
			<jsp:include page="footer.jsp" />
</body>
</html>