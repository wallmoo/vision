<%@page import="bean.MediaBean"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
	// 전달된 객체 가져오기 
	List medialist = (List) request.getAttribute("medialist");
	MediaBean mediaBean = null;
	int type = (Integer) request.getAttribute("type");
	int nowPage = (Integer) request.getAttribute("page");
	int listcount = (Integer) request.getAttribute("listcount");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Video</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0 " />
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<link type="text/css" rel="stylesheet" href="css/main.css" />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<!-- js Library -->
</head>
<body>
	<div>
		<jsp:include page="header.jsp" />
		<!--container-->
		<div class='container'>
		<div class='board_content'>
		<%
			if(type==21) {
		%>
			<h2> <span>Catalog</span> </h2>
		<%		
			} else {
		%>
			<h2> <span>Video</span> </h2>
		<%		
			}
		%>
		
			<div>
					<%
						for(int i = 0 ; i< medialist.size(); i ++) {
							mediaBean = (MediaBean) medialist.get(i);
					%>
						<table>
					<caption class='blind'>목록</caption>
					<thead>
						<tr>
							<th colspan='1'><%=mediaBean.getMedia_num() %></th>
							<th colspan='5'><%=mediaBean.getMedia_title() %></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>작성자</td>
							<td><%=mediaBean.getMedia_writer() %></td>
							<td>등록일</td>
							<td><%=mediaBean.getMedia_date().substring(0,11) %></td>
							<td>조회수</td>
							<td><%=mediaBean.getMedia_count()%></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div align="center">
			<%
				if(type==21) {
			%>
				<p>
				<img src="upload/type0/<%=mediaBean.getMedia_filename()%>" alt="" />
					
				</p>	
			<%		
				} else {
			%>
				<p>
				<iframe style="width: 854px;height: 480px;" src="https://www.youtube.com/embed/<%=mediaBean.getMedia_filename()%>" allowfullscreen></iframe>
					
				</p>
			<%		
				}
			%>
			</div>
			<div>
				<%
					if (mediaBean.getMedia_num() - 1 == 0) {
				%>
				<p>
					<a href="" onclick="alert('게시물이 없습니다.')">이전글</a>
				</p>
				<%
					} else {
				%>
				<p>
					<a
						href="./detail.md?num=<%=mediaBean.getMedia_num() - 1%>&page=<%=nowPage%>&type=<%=mediaBean.getMedia_type()%>">이전글</a>
				</p>
				<%
					}
					if (mediaBean.getMedia_num() + 1 > listcount) {
				%>
				<p>
					<a href="" onclick="alert('게시물이 없습니다.')">다음글</a>
				</p>
				<%
					} else {
				%>
				<p>
					<a
						href="./detail.md?num=<%=mediaBean.getMedia_num() + 1%>&page=<%=nowPage%>&type=<%=mediaBean.getMedia_type()%>">다음글</a>
				</p>
				<%
					}
				%>
				<p>
					<a href="./list.bs?type=<%=type%>&page=<%=nowPage%>">목록</a>
				</p>
			</div>
			<%		
				}
			%>
		</div>
		</div>
		<jsp:include page="footer.jsp" />
</body>
</html>