<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VRHL series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vrhl'>
		<div class='vrhl_con1'>
			<h2  class='pc'> <span>VRHL series</span> </h2>
			<h2 class='mobile'> <span>링조명</span> </h2>
			<ul class='mobile'>
				<li ><a href='vrll.jsp'>VRLL</a></li>
				<li><a href='vrml.jsp'>VRML</a></li>
				<li class='name'><a href='vrhl.jsp'>VRHL</a></li>
				<li><a href='vrdl.jsp'>VRDL</a></li>
				<li><a href='vrfl.jsp'>VRFL</a></li>
			</ul>
			<section>
				<h3><span>고입사각 링 조명</span> RING HIGH ANGLE LIGHT</h3>
				<p><span>VRHL series</span>는 수평을 기준으로5~30°의 입사각을 갖는 제품입니다. 산업 전반에 걸쳐 가장 많이 사용되며 표준 타입의 링조명입니다. 
				</p>
				<img src='../img/light/vrll_img1.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>크기가 작은 부품 검사,고무링 검사,PCB검사</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VRHLserie는 수평을 기준으로5~30°의 입사각을 갖는 제품입니다. 가장 광범위로 이용되는 표준형 링 조명입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					설치가 용이하며 조명과 검사 대상체의 높이조절에 따라 부터 검사 면적이 넓은 부품 검사부터 크기가 작은 제품까지 검사가 용이한 제품입니다.<br/>
					수평을 기준으로 5~30°의 입사각을 가지고 있어 집광률이 다른 자사 링조명에 비해 높습니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>입사각 링 조명(입사각 35°~55°)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>외경(Ø)</th>
								<th>내경(Ø)</th>
								<th>높이(㎜)</th>
								<th>입사각(°)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VRHL50/20(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>5W</td>
								<td>0.4A/0.2A</td>
								<td>50</td>
								<td>20</td>
								<td>17</td>
								<td>5</td>
								<td class='download'><a href='../download/VRHL50 20J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRHL50 20J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRHL70/35(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>7W</td>
								<td>0.6A/0.3A</td>
								<td>70</td>
								<td>35</td>
								<td>20</td>
								<td>15</td>
								<td class='download'><a href='../download/VRHL70 35J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRHL70 35J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRHL70/27(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>8W</td>
								<td>0.6A/0.3A</td>
								<td>70</td>
								<td>27</td>
								<td>19</td>
								<td>30</td>
								<td class='download'><a href='../download/VRHL70 27J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRHL70 27J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRHL81/38</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>8W</td>
								<td>0.7A/0.4A</td>
								<td>81</td>
								<td>38</td>
								<td>22</td>
								<td>20</td>
								<td class='download'><a href='../download/VRHL81 38.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRHL81 38-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRHL92/50(J)</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>9W</td>
								<td>0.8A/0.4A</td>
								<td>92</td>
								<td>50</td>
								<td>18.5</td>
								<td>15</td>
								<td class='download'><a href='../download/VRHL92 50J.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRHL92 50J-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vrhl_img2.png' alt=''>
			</section>
			
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>