<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VISION-TECH series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_controller'>
		<div class='visiontech_con1'>
			<h2> <span>정전압</span> </h2>
			<section>
				<h3><span>정전압</span>CONSTANT VOLTAGE</h3>
				<p><span>정전압 제어 형식</span>의 조명 컨트롤러 제품입니다.<br/>조명 연결 채널 수(1~8CH)와 전력량(10W~1000W/12V,24V) 등 고객님께서 원하시는 대로 제품 설계, 생산 가능한 제품이니 담당자와 상담부탁드립니다.<br/>
					또한 RS-232 통신을 통해 자동 제어 가능한 제품입니다. 컨트롤러 크기 또한 표를 보시고 선택 가능 하시니 표 참고 부탁드립니다.
				</p>
			</section>
			<section>
				<div><img src='../img/controller/visiontech_1.png' alt=''><img src='../img/controller/visiontech_2.png' alt=''></div>
				<div><img src='../img/controller/visiontech_3.png' alt=''><img src='../img/controller/visiontech_4.png' alt=''></div>
				<h3><span>◆</span>1 Channel</h3>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>전류 전압</th>
								<th>소형 A</th>
								<th>소형 B</th>
								<th>중형 A</th>
								<th>중형 B</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/1CH(전류,전압)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/1CH(소형A)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/1CH(소형B)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/1CH(중형 A)(132)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../download/1CH(전류,전압).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/1CH(소형A).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/1CH(소형B).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/1CH(중형 A)(132).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/1CH(중형B)앞면.dwg' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
				<h3><span>◆</span>2 Channel</h3>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>소형 A</th>
								<th>소형 B</th>
								<th>소형 C</th>
								<th>소형 D</th>
								<th>소형 F</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/2CH(소형A)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/2CH(소형B)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/2CH(소형C)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/2CH(소형D)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/2CH(소형F)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../download/2CH(소형A).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/2CH(소형B).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/2CH(소형C).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/2CH(소형D).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/2CH(소형F).dwg' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
					<h3><span>◆</span>3 Channel</h3>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>소형 A</th>
								<th>소형 B</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/3CH(소형A)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/3CH(소형 B)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../download/3CH(소형A).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/3CH(소형 B).dwg' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
					<h3><span>◆</span>4 Channel</h3>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>소형 A</th>
								<th>소형 B</th>
								<th>소형 C</th>
								<th>소형 D</th>
								<th>소형 F</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/4CH(소형A)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/4CH(소형B)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/4CH(소형C)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/4CH(소형D)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/4CH(소형F)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../download/4CH(소형A).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/4CH(소형B).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/4CH(소형C).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/4CH(소형D).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/4CH(소형F).dwg' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
					<h3><span>◆</span>6 Channel</h3>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>소형 A</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>LAYOUT</td>
								<td><a href='../download/6CH(소형A)-Model.pdf' target='_black'><img src='../img/light/download.png'></a></td>
								
							</tr>
							<tr>
								<td>DWG</td>
								<td><a href='../download/6CH(소형A).dwg' target='_black'><img src='../img/light/download.png'></a></td>
								
							</tr>
						</tbody>
					</table>
			</section>
			<section>
				<div><img src='../img/controller/visiontech_5.png' alt=''><img src='../img/controller/visiontech_6.png' alt=''></div>
				<div><img src='../img/controller/visiontech_7.png' alt=''><img src='../img/controller/visiontech_8.png' alt=''></div>
				<h3><span>◆</span>RSP series</h3>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>RSP-100</th>
								<th>RSP-200</th>
								<th>RSP-320</th>
								<th>RSP-500</th>
								<th>RSP-1000</th>
								<th>RSP-1500</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>SPECIFICATION</td>
								<td><a href='../download/' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/' target='_black'><img src='../img/light/download.png'></a></td>
								<td><a href='../download/' target='_black'><img src='../img/light/download.png'></a></td>
							</tr>
						</tbody>
					</table>
			</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>