<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>About</title>
		<jsp:include page="viewerheader.jsp" />
		<!--container-->
	<div class='container about'>
		<div class='about_con1'>
			<h2> <span>About</span> </h2>
			<img src='../img/about_img1.jpg' alt=''>
			<p class='pc'>"안녕하십니까? 비젼테크 홈페이지에 방문해 주셔서 대단히 감사드립니다."</p>
			<p>
				조명은 인류역사의 탄생과 더불어 인간에게 없어서는 안될 생활필수품이 되었습니다.  비전테크는 이 조명분야에서 수십년간 쌓아온 경험과 기술을 바탕으로 2004년 설립되었습니다. 
				조명은 이제 사무실이나 가정의 광원으로만이 아니라 전 사업 분야에 널리 쓰이고 있습니다. 폐사는 건설 분야, 전자 분야, 자동차 분야, 의료 분야, 보안 분야, 광학 분야, 게임기 분야, 
				광고분야 등  다양한 광원을 공급하고 있으며 항상 고객 여러분의 요구에 대응하여 다양한 솔루션을 제공해 오고 있습니다. 경험과 기술을 바탕으로 고품질의 제품공급과 한 차원 높은 
				서비스를 통해 고객감동의 가치를 창출하는 기업, 진취적 기상으로 세계를 향해 도전하는 기업으로서 그 역할과 사명을 다할 것을 다짐합니다.
			</p>
			<div class='about_dl'> 
				<dl>
					<dt>비젼을 갖자</dt>
					<dd>비젼테크 임직원은 항상 긍정적이고 진취적인 젊은사고를 가지고</br/>
						고객의 꿈을 실현 시켜 드리기 위한 올바른 방향을 제시해 드릴것을 약속 드립니다.</dd>
				</dl>
				<dl>
					<dt>정도를 걷자</dt>
					<dd>비젼테크 임직원은 신뢰를 생명으로 여기고 있습니다.<br/>
						옳지않는 길은 절대로 가지 않을 것이며 언제나 정도를 걸으며 고객 곁에 남겠습니다.</dd>
				</dl>
				<dl>
					<dt>최선을 다하자</dt>
					<dd>비젼테크 임직원은 고객을 최우선으로 생각합니다. 작은 주문과 의견도 보석같이 여기며 최선을 다해 대처하겠습니다.<br/>
						말이 아닌 행동이 앞선 모범 기업이 되겠습니다.</dd>
				</dl>
				<p>비젼테크 대표이사 <span>이 석 길</span></p>
			</div>
		</div>
		<jsp:include page="viewerfooter.jsp" />
	</body>
</html>