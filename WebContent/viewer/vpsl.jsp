<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VPSL series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vpsl'>
		<div class='vpsl_con1'>
			<h2 class='pc'> <span>VPSL series</span> </h2>
			<h2 class='mobile'> <span>스폿조명</span> </h2>
			<ul class='mobile'>
				<li><a href='vspl.jsp'>VSPL</a></li>
				<li class='name'><a href='vpsl.jsp'>VPSL</a></li>
			</ul>
			<section>
				<h3><span>파워스폿 조명</span>POWER SPOT LIGHT</h3>
				<p><span>VPSL series</span>는 크기가 작은 조명으로써 좁고 협소한 공간에서 효과 적인 조명입니다.<br/>고휘도 LED를 채용하여 고밀도의 빛을 한포인트에 조사합니다.
				</p>
				<img src='../img/light/vpsl_img1.png' alt='vspl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>반도체,PCB 마킹검사 , 금속면 인쇄 ,라벨 검사</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					조명,취급이 간단한 헤드사이즈입니다.
					좁고 협소한 공간에서도 확산광을 이용한 균일 조사를 실현 할 수 있습니다.<br/>
					고휘도 LED를 채용하여 고밀도의 광을 발생시킵니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					협소한 공간에서 효과를 내는 조명입니다.설치가 자유로워 여러가지 형태로 응용이 가능한 제품입니다.<br/>
					공간 절약 설치, 원포인트 스팟 조명
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>파워스폿 조명(POWER SPOT TYPE)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>길이(㎜)</th>
								<th>외경(Ø)</th>
								<th>내경(Ø)</th>
								<th>발광장(Ø)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VPSL173/27</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>3W</td>
								<td>3㎃/1.5㎃</td>
								<td>173</td>
								<td>27</td>
								<td>12</td>
								<td>10</td>
								<td class='download'><a href='../download/VSPL 10 173.5.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSPL 10 173.5-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VPSL175/27</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>6W</td>
								<td>5㎃/2.5㎃</td>
								<td>175</td>
								<td>27</td>
								<td>12</td>
								<td>10</td>
								<td class='download'><a href='../download/VSPL 10 175.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VSPL 10 175-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
					
				<img src='../img/light/vpsl_img2.png' alt=''>
			</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>