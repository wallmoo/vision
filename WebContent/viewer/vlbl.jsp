<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VLBL series </title>

<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vlbl'>
		<div class='vlbl_con1'>
			<h2 class='pc'> <span>VLBL series</span> </h2>
			<h2 class='mobile'> <span>라인스캔조명</span> </h2>
			<ul class='mobile'>
				<li><a href='vhls.jsp'>VHLS</a></li>
				<li><a href='vlsl.jsp'>VLSL</a></li>
				<li class='name'><a href='vlbl.jsp'>VLBL</a></li>
			</ul>
			<section>
				<h3><span>바 라인조명</span> LAMP BAR LIGHT</h3>
				<p><span>VLBL series</span>는 자유도가 높은 바조명으로써 다양한 설치를 통해 다양한 형태의 검사대상체 적용할 수 있으며, 활용도 높은 제품입니다.<br/>
					넓은 발광장으로 넓고 균일한 확산각을 확보하였습니다.
				</p>
				<img src='../img/light/vlbl_img1.png' alt='vlbl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>라인 스캔용 조명, 필름, 제품 외관, 바코드 등 다양한 제품 검사, 설치의 자유도가 높지 않은 협소한 공간 등에 쓰입니다.</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					MOVLSL는 라인스캔용 다목적 LED BAR 조명입니다.빛의 확산도와 균일성을 위해 광확산판을 채용하였습니다.<br/>
					넓은 발광원(18,28㎜)을 가지고 있습니다.발열을 잡기위해 특수제작한 방열판과 방열펜을 채용하였습니다.						
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					LED PCB를 고밀도로 배치하여 균일도를 높힌 제품입니다.제품의 폭,길이와 높이 등을 각10㎜,100㎜,축소 또는 확대 제작 가능합니다.<br/>
					조도 또한 30만lux,60만 lux에 국한되어있지않고 적게는 3000~6000lux많게는 300만lux까지 고객님께서 원하시는 조도,휘도로 제작가능합니다.
						
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>라인 바 조명(20)</p>
					<table>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th>발광장(㎜)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VLBL100N</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>6W X N</td>
								<td>0.5A/0.3A</td>
								<td>100 X N</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL100N(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL100N(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL300</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>18W</td>
								<td>1.5A/0.7A</td>
								<td>300</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL300(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL300(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL500</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>30W</td>
								<td>2.4A/1.2A</td>
								<td>500</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL500(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL500(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>48W</td>
								<td>4A/2A</td>
								<td>800</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL800(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL800(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL1000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>60W</td>
								<td>5A/2.5A</td>
								<td>1000</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL1000(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL1000(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL1200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>72W</td>
								<td>6A/3A</td>
								<td>1200</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL1200(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL1200(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL1400</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>84W</td>
								<td>7A/3.5A/td>
								<td>1400</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL1400(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL1400(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL1600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>96W</td>
								<td>8A/4A</td>
								<td>1600</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL1600(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL1600(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL1800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>108W</td>
								<td>9A/4.5A</td>
								<td>1800</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL1800(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL1800(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL2000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>120W</td>
								<td>10A/5A</td>
								<td>2000</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL2000(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL2000(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL2400</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>144W</td>
								<td>12A/6A</td>
								<td>2400</td>
								<td>30</td>
								<td>40</td>
								<td>20</td>
								<td class='download'><a href='../download/VLBL2400(20).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL2400(20)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
					<p>라인 바 조명(40)</p>
						<table>
							<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th>발광장(㎜)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VLBL100N</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>10W X N</td>
								<td>0.8A/0.4A</td>
								<td>100 X N</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL100N(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL100N(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL300</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>40W</td>
								<td>3A/1.5A</td>
								<td>300</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL300(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL300(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL500</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>60W</td>
								<td>5A/2.5A</td>
								<td>500</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL500(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL500(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>80W</td>
								<td>6.5A/3A</td>
								<td>800</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL800(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL800(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL1000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>100W</td>
								<td>8A/4A</td>
								<td>1000</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL1000(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL1000(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL1200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>120W</td>
								<td>10A/5A</td>
								<td>1200</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL1200(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL1200(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL1400</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>140W</td>
								<td>12A/6A</td>
								<td>1400</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL1400(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL1400(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL1600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>160W</td>
								<td>13A/6.5A</td>
								<td>1600</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL1600(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL1600(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL1800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>180W</td>
								<td>5A/7.5A</td>
								<td>1800</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL1800(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL1800(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL2000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>200W</td>
								<td>17A/8.5A</td>
								<td>2000</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL2000(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL2000(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VLBL2400</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>240W</td>
								<td>20A/10A</td>
								<td>2400</td>
								<td>50</td>
								<td>40</td>
								<td>40</td>
								<td class='download'><a href='../download/VLBL2400(40).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VLBL2400(40)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
						</table>
				<img src='../img/light/vlbl_img2.png' alt=''>
			</section>
			<section>
				<h3><span>◆</span> 라인 조명 예시</h3>
				<dl>
					<dt><img src='../img/light/vhis_img4.jpg' alt='라인조명 예시1'></dt>
					<dd>라인조명 예시1</dd>
				</dl>
				<dl>
					<dt><img src='../img/light/vhis_img5.jpg' alt='라인조명 예시2'></dt>
					<dd>라인조명 예시2</dd>
				</dl>
			</section>
			
			
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>