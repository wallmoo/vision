<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VHLS series </title>
	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vhls'>
		<div class='vhls_con1'>
			<h2 class='pc'> <span>VHLS series</span> </h2>
			<h2 class='mobile'> <span>라인스캔조명</span> </h2>
			<ul class='mobile'>
				<li class='name'><a href='vhls.jsp'>VHLS</a></li>
				<li><a href='vlsl.jsp'>VLSL</a></li>
				<li><a href='vlbl.jsp'>VLBL</a></li>
			</ul>
			<section>
				<h3><span>고휘도 라인조명</span>HIGH POWER LINE SCAN LIGHT</h3>
				<p><span>VHLS series</span>는 자사 제품 중 가장 높은 휘도를 자랑하는 제품입니다.<br/>다양한 길이와 넓이 조정으로 다양한 제품검사 활용도가 높은 제품입니다. 발광장을 집광형과 확산형을 선택하시어 제품에 맞추어 특화된 검사 조명으로 활용 가능합니다.
				</p>
				<dl>
					<dt><img src='../img/light/vhis_img1.png' alt='vhis 2KK'></dt>
					<dd>VHLS 2KK</dd>
				</dl>
				<dl>
					<dt><img src='../img/light/vhis_img6.png' alt='vhis 4KK'></dt>
					<dd>VHLS 4KK</dd>
				</dl>
				<dl>
					<dt><img src='../img/light/vhis_img7.png' alt='vhis 8KK'></dt>
					<dd>VHLS 8KK</dd>
				</dl>                                
				<dl>
					<dt><img src='../img/light/vhis_img8.png' alt='vhis 집광형1-1'></dt>
					<dd>VHLS 집광형1-1</dd>
				</dl>
                
				<dl>
					<dt><img src='../img/light/vhis_img9.png' alt='vhis 집광형1-2(확대)'></dt>
					<dd>VHLS 집광형1-2(확대)</dd>
				</dl>
				<dl>
					<dt><img src='../img/light/vhis_img10.png' alt='vhis 집광형2'></dt>
					<dd>VHLS 집광형2</dd>
				</dl>                                
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>라인 스캔용 조명 , 스크래치 검사 ( 유리, PCB외관, 필름 등 ) 등에 쓰입니다.</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>VHLS는 라인스캔 전용 조명입니다. 빛의 확산도와 균일성을 위해 광확산판을 채용하였습니다. 고휘도의 LED PCB를 채용하여 높은 휘도를 자랑합니다.<br/>
					발열 억제를 위해 특수제작한 방열판을 채용하였습니다. 집광형과 확산형 TYPE으로 검사제품에 맞추어 선택하실 수 있습니다
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>고휘도 LED PCB를 고밀도로 배치하여 높은 휘도를 자랑합니다.길이100㎜기준으로 150㎜~2550㎜까지 주문생산이 가능한 제품입니다.<br/>제품의 폭,길이와 높이 등을 각10㎜,100㎜,10㎜로
					축소 또는 확대 제작 가능합니다.<br/>조도 또한 20,40,80,100만lux에 국한 되어있지않고 고객님께서 원하시는 조도,휘도로 제작가능합니다.확산형과 집광형을 선택하셔서 주문생산 가능합니다. 
					확산형(광확산판 채용), 집광형 (폴렌즈 채용)
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>고휘도 라인 조명(2KK)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th>발광장(㎜)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VHLS100N</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>7W X N</td>
								<td>0.6A/0.2A</td>
								<td>100 X N</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS100N(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS100N(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS150</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>10W</td>
								<td>0.9A/0.4A</td>
								<td>150</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS150(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS150(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS280</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>20W</td>
								<td>1.6A/0.8A</td>
								<td>280</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS280(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS280(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS400</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>28W</td>
								<td>2.4A/1.2A</td>
								<td>400</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS400(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS400(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS600</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>42W</td>
								<td>3.5A/1.7A</td>
								<td>600</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS600(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS600(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>56W</td>
								<td>4.7A/2.4A</td>
								<td>800</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS800(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS800(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS812</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>58W</td>
								<td>5A/2.5A</td>
								<td>812</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS812(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS812(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS1000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>70W</td>
								<td>5.8A/2.9A</td>
								<td>1000</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS1000(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS1000(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS1100</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>77W</td>
								<td>6.4A/3.2A</td>
								<td>1100</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS1100(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS1100(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS1200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>84W</td>
								<td>7A/3.5A</td>
								<td>1200</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS1200(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS1200(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS1500</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>105W</td>
								<td>8.8A/4.4A</td>
								<td>1500</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS1500(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS1500(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS1800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>126W</td>
								<td>10A/5A</td>
								<td>1800</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS1800(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS1800(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS2000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>140W</td>
								<td>12A/6A</td>
								<td>2000</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS2000(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS2000(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS2200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>156W</td>
								<td>13A/6.5A</td>
								<td>2200</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS2200(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS2200(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS2400</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>168W</td>
								<td>14A/7A</td>
								<td>2400</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS2400(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS2400(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS2550</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>178W</td>
								<td>15A/7.5A</td>
								<td>2550</td>
								<td>75</td>
								<td>25</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS2550(2KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS2550(2KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
					<p>고휘도 라인 조명(4KK)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th>발광장(㎜)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VHLS100N</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>14W X N</td>
								<td>1.2A/0.4A</td>
								<td>100 X N</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS100N(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS100N(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS280</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>39W</td>
								<td>3.2A/1.6A</td>
								<td>280</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS280(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS280(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS300</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>42W</td>
								<td>3.5A/1.7A</td>
								<td>300</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS300(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS300(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS450</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>56W</td>
								<td>4.2A/2.4A</td>
								<td>400</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS450(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS450(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS500</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>70W</td>
								<td>5.8A/2.9A</td>
								<td>500</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS500(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS500(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS700</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>98W</td>
								<td>8A/4A</td>
								<td>700</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS700(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS700(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS812</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>113W</td>
								<td>9.6A/4.8A</td>
								<td>812</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS812(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS812(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS900</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>126W</td>
								<td>10A/5A</td>
								<td>900</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS900(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS900(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS1000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>140W</td>
								<td>12A/6A</td>
								<td>1000</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS1000(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS1000(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS1200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>154W</td>
								<td>13A/6.5A</td>
								<td>1200</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS1200(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS1200(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS1500</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>210W</td>
								<td>18A/9A</td>
								<td>1500</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS1500(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS1500(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS1800</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>252W</td>
								<td>20A/10A</td>
								<td>1800</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS1800(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS1800(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS1900</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>266W</td>
								<td>22A/11A</td>
								<td>1900</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS1900(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS1900(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS2000</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>280W</td>
								<td>24A/12A</td>
								<td>2000</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS2000(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS2000(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VHLS2200</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>308W</td>
								<td>26A/13A</td>
								<td>2200</td>
								<td>103</td>
								<td>29</td>
								<td>9</td>
								<td class='download'><a href='../download/VHLS2200(4KK).dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VHLS2200(4KK)-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vhis_img3.jpg' alt=''>
			</section>
			<section>
				<h3><span>◆</span> 라인 조명 예시</h3>
				<dl>
					<dt><img src='../img/light/vhis_img4.jpg' alt='라인조명 예시1'></dt>
					<dd>라인조명 예시1</dd>
				</dl>
				<dl>
					<dt><img src='../img/light/vhis_img5.jpg' alt='라인조명 예시2'></dt>
					<dd>라인조명 예시2</dd>
				</dl>
			</section>
			
			
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>