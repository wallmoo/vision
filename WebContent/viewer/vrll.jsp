<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VRLL series </title>

<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vrll'>
		<div class='vrll_con1'>
			<h2  class='pc'> <span>VRLL series</span> </h2>
			<h2 class='mobile'> <span>링조명</span> </h2>
			<ul class='mobile'>
				<li class='name'><a href='vrll.jsp'>VRLL</a></li>
				<li><a href='vrml.jsp'>VRML</a></li>
				<li><a href='vrhl.jsp'>VRHL</a></li>
				<li><a href='vrdl.jsp'>VRDL</a></li>
				<li><a href='vrfl.jsp'>VRFL</a></li>
			</ul>
			<section>
				<h3><span>저입사각 링 조명</span> RING LOW ANGLE LIGHT</h3>
				<p><span>VRLL series</span>는 수평을 기준으로60~85°의 입사각을 갖는 제품입니다.
				</p>
				<img src='../img/light/vrll_img1.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>금속,유리등 표면조도가 높은 제품의 스크래치 및 크랙 검사 , 바코드 및 라벨 검사 , 고무링 검사</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VRLLseries는 수평을 기준으로60~85°의 입사각을 갖는 제품입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					다각도에서 검사대상체 표면의 돌출부에 빛을 조사함으로써 금속 표면 스크래치 검사에 특화된 조명입니다.<br/>
					고입사각으로 다각도에서 빛을 조사함으로써 표면 조도가 높은 검사체에 검사가 적합합니다. 미세한 스크레치 검사에 용이한 제품입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>입사각 링 조명(입사각 60°~85°)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>외경(Ø)</th>
								<th>내경(Ø)</th>
								<th>높이(㎜)</th>
								<th>입사각(°)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VRLL95/28</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>10W</td>
								<td>0.8A/0.4A</td>
								<td>95</td>
								<td>28</td>
								<td>21</td>
								<td>60</td>
								<td class='download'><a href='../download/VRLL90 28.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRLL90 28-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRLL92/58</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>12W</td>
								<td>1A/0.5A</td>
								<td>92</td>
								<td>58</td>
								<td>22</td>
								<td>70</td>
								<td class='download'><a href='../download/VRLL92 58.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRLL92 58-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vrll_img2.png' alt=''>
			</section>
			
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>