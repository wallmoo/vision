<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VRML series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vrml'>
		<div class='vrml_con1'>
			<h2  class='pc'> <span>VRML series</span> </h2>
			<h2 class='mobile'> <span>링조명</span> </h2>
			<ul class='mobile'>
				<li ><a href='vrll.jsp'>VRLL</a></li>
				<li class='name'><a href='vrml.jsp'>VRML</a></li>
				<li ><a href='vrhl.jsp'>VRHL</a></li>
				<li ><a href='vrdl.jsp'>VRDL</a></li>
				<li ><a href='vrfl.jsp'>VRFL</a></li>
			</ul>
			<section>
				<h3><span>중입사각 링 조명</span> RING MIDDLE ANGLE LIGHT</h3>
				<p><span>VRML series</span>는 수평을 기준으로35~50°의 입사각을 갖는 제품입니다.
				</p>
				<img src='../img/light/vrll_img1.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>이물질 및 스크래치 검사 , PT병 검사, 표면조도가 높은 제품에 바코드 및 프린팅 검사, 금속표면 크랙검사</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VRMLseries는 수평을 기준으로35~50°의 입사각을 갖는 제품입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					다각도에서 검사대상체 표면의 돌출부에 빛을 조사함으로써 스크래치 검사에 특화된 조명입니다.<br/>
					중입사각으로 다각도에서 빛을 조사함으로써 표면 조도가 높은 검사체에 검사가 적합합니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>입사각 링 조명(입사각 35°~55°)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>외경(Ø)</th>
								<th>내경(Ø)</th>
								<th>높이(㎜)</th>
								<th>입사각(°)</th>
								<th colspan='2'>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>VRML90/68</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>5W</td>
								<td>0.3A/0.2A</td>
								<td>90</td>
								<td>68</td>
								<td>22</td>
								<td>45</td>
								<td class='download'><a href='../download/VRML90 68.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRML90 68-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
							<tr>
								<td>VRML92/58</td>
								<td><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td>12V/24V</td>
								<td>8W</td>
								<td>0.8A/0.4A</td>
								<td>92</td>
								<td>58</td>
								<td>22</td>
								<td>45</td>
								<td class='download'><a href='../download/VRML92 58.dwg' target='_black'><img src='../img/light/download.png'></a><a hef=''>DWG</a></td>
								<td class='download'><a href='../download/VRML92 58-Model.pdf' target='_black'><img src='../img/light/download.png'></a><a hef=''>PDF</a></td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vrml_img2.png' alt=''>
			</section>
			
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>