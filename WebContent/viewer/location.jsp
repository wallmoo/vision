<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Location</title>

		<jsp:include page="viewerheader.jsp" />
		<!--container-->
	<div class='container location'>
		<div class='loca_con1'>
			<h2> <span>Location</span> </h2>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6337.796878716631!2d126.88644509951179!3d37.41587534070978!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357b613f5e101bdd%3A0x41d4dc67aca39be0!2z6rSR66qF7Jet7J6Q7J207YOA7JuM!5e0!3m2!1sko!2skr!4v1579505092209!5m2!1sko!2skr" width="1050" height="500" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		</div>
		<div class='loca_con2'>
			<ul>
				<li>
					<ul>
						<li>ADDRESS</li>
						<li><span class='pc'>경기도 광명시 새빛공원로 67, 광명역 자이타워 A동 1821호</span>
							<span class='mobile'>경기도 광명시 새빛공원로 67<br/>광명역 자이타워 A동 1821호</span>
						</li>
						<li>E-MAIL</li>
						<li>luckylsk@naver.com</li>
					</ul>
				</li>
				<li>
					<ul>
						<li>TEL</li>
						<li>02) 869-3001</li>
						<li>FAX</li>
						<li>02) 6008-6405</li>
					</ul>
				</li>
			</ul>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>