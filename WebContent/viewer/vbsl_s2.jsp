<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> VBSL_S2 series </title>

	<jsp:include page="viewerheader.jsp" />
	<!--container-->
	<div class='container_light vbsl_s2'>
		<div class='vbsl_s1_con1'>
			<h2> <span>VBSL_S2 series</span> </h2>
			<section>
				<h3><span>백라이트 조명</span>LED BACK LIGHT</h3>
				<p><span>VBSL series</span>는 타사 백라이트 제품에 비해 높은 휘도율을 자랑하는 제품입니다.
				</p>
				<img src='../img/light/vbsl_s1_img1.png' alt='vlsl'>
			</section>
			<section>
				<h3><span>◆</span> 제품용도</h3>
				<p>PCB 외관 검사 , 고속촬영 및 실루엣 검사, 대형 제품 검사용</p>
			</section>
			<section>
				<h3><span>◆</span> 설명</h3>
				<p>
					VBL serise는 타사에 비해 높은 휘도율을 자랑하는 제품입니다.<br/>
					발열을 억제하기 위해 특수 제작한 방열판을 채용하였고
					펜을 추가적으로 부착할 수 있는 제품입니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 특성</h3>
				<p>
					LED PCB를 균일하고 고밀도로 배열하여 균일하고 높은 휘도율을 자랑합니다.<br/>
					광확산판을 채용하여 확산각이 넓고 균일합니다.<br/>
					발열을 잡기 위해 자체 설계한 방열판(VBSL,VBML,VBLL)과 
					추가적으로 펜(VBLL)을 부착하였습니다.
				</p>
			</section>
			<section>
				<h3><span>◆</span> 제품사양</h3>
					<p>평판형 링 조명(입사각 0°)</p>
					<table>
						<caption class="blind">제품사양</caption>
						<thead>
							<tr>
								<th>MODEL</th>
								<th>COLOR</th>
								<th>VOTAGE</th>
								<th>WATTAGE</th>
								<th>CURRET</th>
								<th>가로(㎜)</th>
								<th>세로(㎜)</th>
								<th>높이(㎜)</th>
								<th>휘도(lm)</th>
								<th>download</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td rowspan='2'>VBSL127/79J</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>6W</td>
								<td rowspan='1'>0.5A/0.25A</td>
								<td rowspan='2'>127</td>
								<td rowspan='2'>79</td>
								<td rowspan='2'>25</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>12W</td>
								<td rowspan='1'>1A/0.5A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL135/135</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>7.5W</td>
								<td rowspan='1'>0.6A/0.3A</td>
								<td rowspan='2'>135</td>
								<td rowspan='2'>135</td>
								<td rowspan='2'>25</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>15W</td>
								<td rowspan='1'>1.2A/0.6A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL167/167J</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>9W</td>
								<td rowspan='1'>0.7A/0.35A</td>
								<td rowspan='2'>167</td>
								<td rowspan='2'>167</td>
								<td rowspan='2'>32</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>18W</td>
								<td rowspan='1'>1.4A/0.7A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL169/213JL</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>2W</td>
								<td rowspan='1'> 0.2A/0.1A</td>
								<td rowspan='2'>169</td>
								<td rowspan='2'>213</td>
								<td rowspan='2'>12</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>4W</td>
								<td rowspan='1'>0.4A/0.2A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL193/128J</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>9W</td>
								<td rowspan='1'>0.7A/0.35A</td>
								<td rowspan='2'>193</td>
								<td rowspan='2'>128</td>
								<td rowspan='2'>32</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>18W</td>
								<td rowspan='1'>1.4A/0.7A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL195/195</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>12W</td>
								<td rowspan='1'>1A/0.5A</td>
								<td rowspan='2'>195</td>
								<td rowspan='2'>195</td>
								<td rowspan='2'>25</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>24W</td>
								<td rowspan='1'>2A/1A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL208/25J</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>7W</td>
								<td rowspan='1'>0.6A/0.3A</td>
								<td rowspan='2'>208</td>
								<td rowspan='2'>25</td>
								<td rowspan='2'>22</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>14W</td>
								<td rowspan='1'>1.2A/0.6A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL225/225</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>14W</td>
								<td rowspan='1'>1.2A/0.6A</td>
								<td rowspan='2'>225</td>
								<td rowspan='2'>225</td>
								<td rowspan='2'>30</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>28W</td>
								<td rowspan='1'>2.4A/12A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL255/255</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>15W</td>
								<td rowspan='1'>1.3A/0.65A</td>
								<td rowspan='2'>255</td>
								<td rowspan='2'>255</td>
								<td rowspan='2'>30</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>30W</td>
								<td rowspan='1'>2.6A/1.3A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL285/285</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>16W</td>
								<td rowspan='1'>1.4A/0.7A</td>
								<td rowspan='2'>285</td>
								<td rowspan='2'>285</td>
								<td rowspan='2'>30</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>32W</td>
								<td rowspan='1'>2.8A/1.4A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL343/323J</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>18W</td>
								<td rowspan='1'>1.5A/0.75A</td>
								<td rowspan='2'>343</td>
								<td rowspan='2'>323</td>
								<td rowspan='2'>30</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>36W</td>
								<td rowspan='1'>3A/1.5A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL334/334J</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>20W</td>
								<td rowspan='1'>1.6A/0.8A</td>
								<td rowspan='2'>334</td>
								<td rowspan='2'>334</td>
								<td rowspan='2'>30</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>40W</td>
								<td rowspan='1'>3.2A/1.6A</td>
								<td rowspan='1'>140</td>
							</tr>
							<tr>
								<td rowspan='2'>VBSL362/348JL</td>
								<td rowspan='2'><span>w</span><span>r</span><span>g</span><span>b</span></td>
								<td rowspan='2'>12V/24V</td>
								<td rowspan='1'>4W</td>
								<td rowspan='1'>0.3A/0.15A</td>
								<td rowspan='2'>362</td>
								<td rowspan='2'>348</td>
								<td rowspan='2'>16</td>
								<td rowspan='1'>70</td>
								<td></td>	
							</tr>
							<tr>
								<td rowspan='1'>8W</td>
								<td rowspan='1'>0.6A/0.3A</td>
								<td rowspan='1'>140</td>
							</tr>
						</tbody>
					</table>
				<img src='../img/light/vbsl_s1_img2.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 백라이트 조명의 적용 사례</h3>
					<img src='../img/light/vbsl_s1_img3.png' alt=''>
			</section>
			<section>
					<h3><span>◆</span> 백라이트 조명 예시</h3>
					<img src='../img/light/vbsl_s1_img4.png' alt=''>
			</section>
		</div>
		<jsp:include page="viewerfooter.jsp" />
</body>
</html>