<%@page import="bean.BBSBean"%>
<%@page import="java.util.List"%>
<%@page import="java.io.PrintWriter" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>	
<%
	request.setCharacterEncoding("UTF-8");
	// 전달된 객체 가져오기 
	BBSBean bbsBean =(BBSBean) request.getAttribute("bbsList");
	
	// 기본 자료형을 setAttribute() 로 전달했을 경우
	// getAttribute() 리턴형이 Object 이므로 기본자료형으로 직접 형변환 불가
	// 따라서 Wrapper 클래스 타입으로 형변환 후 기본자료형에 저장 필수!
	int maxPage = (Integer) request.getAttribute("maxPage");
	int startPage = (Integer) request.getAttribute("startPage");
	int endPage = (Integer) request.getAttribute("endPage");
	int listCount = (Integer) request.getAttribute("listCount");
	int nowPage = (Integer) request.getAttribute("page");
	int type = (Integer) request.getAttribute("type");
	String date = (String) request.getAttribute("date");
%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">

<title>VISIONTECH</title>
<link rel="stylesheet" href="adminD/css/do_layout.css">
<!-- radio,check -->
<link href="adminD/css/_all.css" rel="stylesheet">
<script src="adminD/js/jquery.js"></script>
<script src="adminD/js/icheck.js"></script>
<!--달력-->
<link rel="stylesheet" type="text/css"
	href="adminD/css/jquery.datetimepicker.css" />
<!-- 메뉴 -->
<link rel="stylesheet" href="adminD/dist/css/superfish.css"
	media="screen">
<!-- <script src="dist/js/jquery.js"></script> -->
<script src="//cdn.ckeditor.com/4.7.2/standard/ckeditor.js"></script>
<script src="adminD/dist/js/superclick.js"></script>
<script>

		(function($){ //create closure so we can safely use $ as alias for jQuery

			$(document).ready(function(){

				// initialise plugin
				var example = $('#example').superclick({
					//add options here if required
				});

				// buttons to demonstrate Superclick's public methods
				$('.destroy').on('click', function(){
					example.superclick('destroy');
					return false;
				});

				$('.init').on('click', function(){
					example.superclick();
					return false;
				});

				$('.open').on('click', function(){
					example.children('li:first').superclick('show');
					return false;
				});

				$('.close').on('click', function(){
					example.children('li:first').superclick('hide');
					return false;
				});

				$('.closeall').on('click', function(){
					example.superclick('reset');
					return false;
				});
			});

		})(jQuery);


	</script>
</head>
<body>
	<div class="do_wrap">
		<jsp:include page="adminheader.jsp"></jsp:include>
			<div class="right_box">
				<div class="manage_box h1000">
					<h2>관리자관리</h2>
					<div class="manage_box_inner">
								<form action="./rewrite.ad" id="afrm" method="post">
								<table cellspacing="0" cellpadding="0" class="">
								<input type="hidden" value="<%=session.getAttribute("admin")%>" name="writer"/>
								<input type="hidden" value="<%=bbsBean.getBbs_type()%>" name="type"/>
								<input type="hidden" value="<%=bbsBean.getBbs_num()%>" name="num"/>
									<tr class="col_1">
										<td class="left_table">제목</td>
										<td class="right_bottom"><input class="w100" name="title" id="title" value="<%=bbsBean.getBbs_title()%>"></td>
									</tr>
											
							<script>
							
								function resubmitAdmin() {
									var content = CKEDITOR.instances.contents.getData();
									var title = $("#title").val();
									var select = $("#select").val();
									if(title==null || title=="" || title == undefined || content==null || content=="" || content == undefined) {
										alert("모두 작성해주세요.");
									} else {
										$("#afrm").submit();	
									}
								}
							</script>
							
							
							<tr class="p20">
								<td colspan="2" class="h450">
									<textarea name="contents" id="contents"><%=bbsBean.getBbs_content()%></textarea>
							        <script>
							        CKEDITOR.config.language ='ko';
							            CKEDITOR.replace( 'contents',{
							            	width:'980',
							            	height:'320',
							            	enterMode:'2',
											shiftEnterMode:'3',
											filebrowserUploadUrl:'./upload.jsp?'
										        +'realUrl=http://dostory.co.kr:8080/vision/'
										        +'&realDir=/home/vision/textupload/'
							            });
							        </script>
								</td>
							</tr>
						</table>
						</form>
					</div>
					<div class="f_right">
						<button>
							<a href="javascript:resubmitAdmin();" class="b_o">저장</a>
						</button>
						<button>
							<a href="javascript:history.back();" class="b_g">취소</a>
						</button>
					</div>
				</div>
				<jsp:include page="adminfooter.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<script>
			$(document).ready(function(){
              $('.login input').iCheck({
                checkboxClass: 'icheckbox_square-top',
                // radioClass: 'iradio_square-green',
                increaseArea: '20%'
              });
            });           
    </script>

	<!-- 달력 -->
	<!-- <script src="js/jqueryC.js"></script> -->
	<script src="adminD/build/jquery.datetimepicker.full.js"></script>
	<script>/*
window.onerror = function(errorMsg) {
	$('#console').html($('#console').html()+'<br>'+errorMsg)
}*/

$.datetimepicker.setLocale('en');

$('#datetimepicker_format').datetimepicker({value:'2015/04/15 05:03', format: $("#datetimepicker_format_value").val()});
$("#datetimepicker_format_change").on("click", function(e){
	$("#datetimepicker_format").data('xdsoft_datetimepicker').setOptions({format: $("#datetimepicker_format_value").val()});
});
$("#datetimepicker_format_locale").on("change", function(e){
	$.datetimepicker.setLocale($(e.currentTarget).val());
});

$('#datetimepicker').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:	'1986/01/05'
});
$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});

$('.some_class').datetimepicker();

$('#default_datetimepicker').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:false
});

$('#datetimepicker10').datetimepicker({
	step:5,
	inline:true
});
$('#datetimepicker_mask').datetimepicker({
	mask:'9999/19/39 29:59'
});
$('#datetimepicker_mask02').datetimepicker({
	mask:'9999/19/39 29:59'
});
$('#datetimepicker_mask03').datetimepicker({
	mask:'9999/19/39 29:59'
});
$('#datetimepicker_mask04').datetimepicker({
	mask:'9999/19/39 29:59'
});

$('#datetimepicker1').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:5
});
$('#datetimepicker2').datetimepicker({
	yearOffset:222,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	minDate:'-1970/01/02', // yesterday is minimum date
	maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#datetimepicker3').datetimepicker({
	inline:true
});
$('#datetimepicker4').datetimepicker();
$('#open').click(function(){
	$('#datetimepicker4').datetimepicker('show');
});
$('#close').click(function(){
	$('#datetimepicker4').datetimepicker('hide');
});
$('#reset').click(function(){
	$('#datetimepicker4').datetimepicker('reset');
});
$('#datetimepicker5').datetimepicker({
	datepicker:false,
	allowTimes:['12:00','13:00','15:00','17:00','17:05','17:20','19:00','20:00'],
	step:5
});
$('#datetimepicker6').datetimepicker();
$('#destroy').click(function(){
	if( $('#datetimepicker6').data('xdsoft_datetimepicker') ){
		$('#datetimepicker6').datetimepicker('destroy');
		this.value = 'create';
	}else{
		$('#datetimepicker6').datetimepicker();
		this.value = 'destroy';
	}
});
var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('#datetimepicker7').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});
$('#datetimepicker8').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date')
			.toggleClass('xdsoft_disabled');
	},
	minDate:'-1970/01/2',
	maxDate:'+1970/01/2',
	timepicker:false
});
$('#datetimepicker9').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date.xdsoft_weekend')
			.addClass('xdsoft_disabled');
	},
	weekends:['01.01.2014','02.01.2014','03.01.2014','04.01.2014','05.01.2014','06.01.2014'],
	timepicker:false
});
var dateToDisable = new Date();
	dateToDisable.setDate(dateToDisable.getDate() + 2);
$('#datetimepicker11').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [false, ""]
		}

		return [true, ""];
	}
});
$('#datetimepicker12').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [true, "custom-date-style"];
		}

		return [true, ""];
	}
});
$('#datetimepicker_dark').datetimepicker({theme:'dark'})


</script>
</body>