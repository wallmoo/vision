<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
    request.setCharacterEncoding("UTF-8");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contact Online</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0 " />
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<link type="text/css" rel="stylesheet" href="css/main.css" />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/order.js"></script>
<!-- js Library -->
</head>
<body>
	<div>
		<jsp:include page="header.jsp" />
		<!--container-->
		<div class='container Request'>
			<div class='online_con1'>
				<h2>
					<span>Order Request</span>
				</h2>
				<form action="./order.bs" method="post" id="orderfrm">
					<fieldset>
						<legend class='blind'>주문의뢰서</legend>
						<p>
							<span>*</span>표시 필수 입력사항
						</p>
						<ul>
							<li><label for="username">이름<span>*</span></label></li>
							<li><input type="text" id="username" name="username" /></li>
						</ul>
						<ul>
							<li><label for="companyname">회사명<span>*</span></label></li>
							<li><input type="text" id="companyname" name="companyname" /></li>
						</ul>
						<ul>
							<li><label for="userphone">연락처<span>*</span></label></li>
							<li><input type="text" id="userphone" name="userphone" /></li>
						</ul>
						<ul>
							<li><label for="useremail">이메일<span>*</span></label></li>
							<li><input type="text" id="useremail" name="useremail" /></li>
						</ul>
						<ul>
							<li><label for="title">제목<span>*</span></label></li>
							<li><input type="text" id="title" name="title" /></li>
						</ul>
						<ul>
							<li><label for="contents">내용<span>*</span></label></li>
							<li><textarea id='contents' name="contents"></textarea></li>
						</ul>
					</fieldset>
				</form>
				<p>
					<a href='javascript:submitFunction();'>전송</a>
				</p>
			</div>
			</div>
			
			<jsp:include page="footer.jsp" />
</body>
</html>